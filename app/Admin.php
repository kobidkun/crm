<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Admin extends Authenticatable
{
    use HasApiTokens, Notifiable;
    protected $fillable = [
        'fname',
        'lname',
        'mobile',
        'email',
        'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
