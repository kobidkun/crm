<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;


class CasherUser extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $guard = 'cashier';


    protected $fillable = [
        'name',
        'mobile',
        'email',
        'user_type',
        'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
