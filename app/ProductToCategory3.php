<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductToCategory3 extends Model
{


    protected $hidden = array(
        'id',
        'description',
        'product_id',
        'updated_at',
        'created_at',
        'catagory_id',
    );

    public function products()
    {
        return $this->hasMany('App\Product','id','product_id');
    }


}
