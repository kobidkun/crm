<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{

    protected $hidden = array('id',
        'product_id',
        'updated_at',
        'created_at',
    );


    public function products()
    {
        return $this->belongsTo('App\Product');
    }
}
