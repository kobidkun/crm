<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $guard = 'customer';
    protected $fillable = [
        'fname',
     //   'lname',
      //  'mobile',
     //   'ref',
     //   'email',
     //   'password',
    ];

   // protected $hidden = [
 //       'password', 'remember_token',
  //  ];

    public function customer_cart_items()
    {
        return $this->hasMany('App\Customer\CustomerCartItems',
            'customer_id','id');
    }

    public function wallet()
    {
        return $this->hasMany('App\Customer\CustomerWallet',
            'customer_id','id');
    }

    public function customer_to_customers()
    {
        return $this->hasOne('App\CustomerToCustomer',
            'customer_id','id');
    }

    public function customer()
    {
        return $this->hasOne('App\Customer',
            'sponser_id','ic_number');
    }

    protected $hidden = ['password','remember_token','created_at','updated_at'];
}
