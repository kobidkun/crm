<?php

namespace App\Model\Initial\Order;

use Illuminate\Database\Eloquent\Model;

class InitialCreateOrderToProduct extends Model
{
    protected $fillable = [
        'id',
        'quantity',
        'parent_product_id',
        'initial_create_orders_id',
        'customer_id',
        'product_id',

    ];
}
