<?php

namespace App\Model\Initial\OrderBV;

use Illuminate\Database\Eloquent\Model;

class InitialCreateOrderToProductBV extends Model
{
    protected $fillable = [
        'id',
        'quantity',
        'parent_product_id',
        'initial_create_orders_id',
        'customer_id',
        'product_id',
    ];
}
