<?php

namespace App\Model\BVInvoice;

use Illuminate\Database\Eloquent\Model;

class ReturnInvoiceBV extends Model
{
    public function return_invoiceto_products()
    {
        return $this->hasMany('App\Model\BVInvoice\ReturnInvoicetoProductBV', 'create_orders_id', 'id');
    }


    public function invoice_to_stock_out_bv()
    {
        return $this->hasMany('App\StockOut', 'id', 'invoice_id');
    }
}
