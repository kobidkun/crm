<?php

namespace App\Model\BVInvoice;

use Illuminate\Database\Eloquent\Model;

class CreateInvoiceBV extends Model
{
    public function invoice_to_products()
    {
        return $this->hasMany('App\Model\BVInvoice\InvoiceToProductBV', 'create_orders_id', 'id');
    }


    public function invoice_to_stock_out()
    {
        return $this->hasMany('App\StockOut', 'id', 'invoice_id');
    }

    public function create_b_v_order_to_invoices()
    {
        return $this->hasOne('App\Model\Order\BVOrder\CreateBVOrderToInvoice', 'create_invoice_b_v_id', 'id');
    }
}
