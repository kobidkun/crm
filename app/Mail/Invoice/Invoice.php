<?php

namespace App\Mail\Invoice;

use App\model\Invoice\CreateInvoice;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Invoice extends Mailable
{
    use Queueable, SerializesModels;

    protected $createorder;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(CreateInvoice $createorder)
    {
        $this->createorder = $createorder;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.invoice.invoice') ->with([
            'createorder' => $this->createorder
        ]);
    }
}
