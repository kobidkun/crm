<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category3 extends Model
{


    protected $hidden = array(
        'description',
        'product_id',
        'updated_at',
        'created_at',
        'catagory_id',
    );


    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function category()
    {
        return $this->belongsTo('App\Catagory');
    }

    public function category2()
    {
        return $this->belongsTo('App\Catagory2');
    }
}
