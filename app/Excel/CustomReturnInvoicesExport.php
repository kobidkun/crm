<?php
/**
 * Created by PhpStorm.
 * User: kobid
 * Date: 13/04/18
 * Time: 5:44 AM
 */




namespace App\Excel;

use App\model\Invoice\ReturnInvoice;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;


class CustomReturnInvoicesExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function headings(): array
    {
        return [
            'Sno',
            'Invoice Number',
            'Invoice Date',
            'Return Month ',
            'Customer Name',
            'Invoice GSTIN',
            'Place of Supply',
            'Invoice Return Quarter',
            'Billing Address Street',
            'Billing Address Locality',
            'Billing Address City',
            'Billing Address State',
            'Billing Address Country',
            'Billing Address Pin',
            'Shipping Address Street',
            'Shipping Address Locality',
            'Shipping Address City',
            'Shipping Address State',
            'Shipping Address Country',
            'Shipping Address Pincode',
            'Taxable Amount',
            'Discount Amount',
            'Cgst',
            'Sgst',
            'Igst',
            'Tax',
            'Taxable Value',
            'Taxable Amount',
            'Disount Amount',
            'Total'


        ];
    }

    public function forYear($from, $to)
    {
        $this->from = $from;
        $this->to = $to;

        return $this;
    }

    public function query()
    {

        $dateFrom = $this->from->format('Y-m-d')." 00:00:00";
        $dateTo = $this->to->format('Y-m-d')." 23:59:59";
        $a = ReturnInvoice::select(
            'id',
            'invoice_number',
            'invoice_date',
            'invoice_return_month',
            'invoice_customer_name',
            'invoice_customer_gstin',
            'invoice_place_of_supply',
            'invoice_return_quater',
            'invoice_billing_address_street',
            'invoice_billing_address_locality',
            'invoice_billing_address_city',
            'invoice_billing_address_state',
            'invoice_billing_address_country',
            'invoice_billing_address_pin',
            'invoice_shipping_address_street',
            'invoice_shipping_address_locality',
            'invoice_shipping_address_city',
            'invoice_shipping_address_state',
            'invoice_shipping_address_country',
            'invoice_shipping_address_pin',
            'invoice_total_taxable_amount',
            'invoice_total_taxable_discount_amount',
            'invoice_total_tax_cgst',
            'invoice_total_tax_sgst',
            'invoice_total_tax_igst',
            'invoice_total_tax',
            'invoice_total_tax_value',
            'invoice_total_taxable_amt',
            'invoice_total_discount_amt',
            'invoice_total'


        )->whereBetween('created_at', [$dateFrom, $dateTo]);
       return $a;

      //  return CreateInvoice::query()->whereYear('created_at', $this->year);
    }
}