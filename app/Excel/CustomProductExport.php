<?php
/**
 * Created by PhpStorm.
 * User: kobid
 * Date: 13/04/18
 * Time: 5:44 AM
 */




namespace App\Excel;



use App\model\Invoice\CreateInvoice;
use App\model\Invoice\InvoiceToProduct;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;


class CustomProductExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function headings(): array
    {
        return [
            'Sno',
            'Invoice Number',
            'Name',
            'Unit ',
            'HSN Code',
            'Type',
            'Quantity',
            'Rate',
            'Discount Amount',
            'Discount Percentage',
            'Total Discount Percentage',
            'Taxable Rate',
            'Taxable Amount',
            'Tax Value',
            'CGST',
            'SGST',
            'IGST',
            'CGST %',
            'SGST %',
            'IGST %',
            /*'Taxable Amount',
            'Taxable Amount',
            'Taxable Amount',
            'Discount Amount',*/
            'Time & Date',
            'Color',
            'Size',
            'Total'


        ];
    }

    public function forYear($from, $to)
    {
        $this->from = $from;
        $this->to = $to;

        return $this;
    }

    public function query()
    {

        $dateFrom = $this->from->format('Y-m-d')." 00:00:00";
        $dateTo = $this->to->format('Y-m-d')." 23:59:59";
        $a = InvoiceToProduct::select(
            'id',
            'invoice_id',
            'name',
            'unit',
            'hsn',
            'type',
            'quantity',
            'rate',
            'discount_amount',
            'discount_percentage',
            'product_total_disc',
            'taxable_rate',
            'taxable_amount',
            'tax_value',
            'cgst',
            'sgst',
            'igst',
            'sgst_percentage',
            'cgst_percentage',
            'igst_percentage',
            'product_total_disc',
            'taxable_amount',
            'created_at',
            'color',
            'size',
            'total'


        )->whereBetween('created_at', [$dateFrom, $dateTo]);
       return $a;

      //  return CreateInvoice::query()->whereYear('created_at', $this->year);
    }
}