<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVatienttoImages extends Model
{

    protected $fillable = [
        'name',
        'cdn_url',
        'path',
        'local_path',
        'extinsion',
        'images_url',
        'product_vatient_id',
        'product_id',
    ];

    protected $hidden = array(
        'updated_at',
        'created_at',
    );


    public function products_variant()
    {
        return $this->belongsTo('App\ProductVatient');
    }
}
