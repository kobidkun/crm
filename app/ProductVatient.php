<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVatient extends Model
{
    public function stock()
    {
        return $this->hasMany('App\ProductVatienttoStock','product_vatient_id','id');
    }

    public function Productsale()
    {
        return $this->hasMany('App\model\Invoice\InvoiceToProduct','product_id','id');
    }

    public function stockout()
    {
        return $this->hasMany('App\ProductVatienttoStock','product_vatient_id','id');
    }

    public function Images()
    {
        return $this->hasMany('App\ProductVatienttoImages',
            'product_vatient_id','id');
    }

    public function productvarient_to_sale()
    {
        return $this->hasMany('App\model\Invoice\InvoiceToProduct',
            'product_id','id');
    }
}
