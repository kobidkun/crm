<?php

namespace App\Http\Controllers\cashier;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\Invoice\CreateInvoice;
use App\model\Invoice\InvoiceToProduct;

use Illuminate\Support\Facades\DB;

class BaseConreoller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:cashier');
    }

    public function dashboard(Request $request){


        $monthlysalestext = CreateInvoice::sum('invoice_total');
        $monthlytax = CreateInvoice::sum('invoice_total_tax');
        $monthlyqtytext = InvoiceToProduct::sum('quantity');





        $monthlysales = DB::table('create_invoices')
            ->select(
                DB::raw('sum(invoice_total) as total'),
                DB::raw('date(created_at) as dates')


            )

            ->groupBy('dates')
            ->orderBy('dates','asc')
            ->get();


        $dailysalesqty = DB::table('invoice_to_products')
            ->select(
                DB::raw('sum(quantity) as total'),
                DB::raw('date(created_at) as dates')
            //   DB::raw('created_at as kkk')


            )

            ->groupBy('dates')
            ->orderBy('dates','asc')
            ->get();


        return view('cashier.dashboard')->with([
            'monthlysales' => $monthlysales,
            'monthlysalestext' => $monthlysalestext,
            'monthlyqtytext' => $monthlyqtytext,
            'monthlytax' => $monthlytax,
            'dailysalesqty' => $dailysalesqty,
        ]);
    }


}
