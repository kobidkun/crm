<?php

namespace App\Http\Controllers\export;

use App\model\Invoice\InvoiceToProduct;
use App\ProductVatient;
use App\ProductVatienttoStock;
use App\StockOut;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Illuminate\Support\Collection;
class ExportJson extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }

    public function ExportProduct( ){
        return view('export.json.saleproduct');
    }



    public function ProductExportTojson(Request $request){

        $from = $request->from;
        $To= $request->to;

        $dateFrom = \Carbon\Carbon::parse($from)->format('Y-m-d')." 00:00:00";
        $dateTo = \Carbon\Carbon::parse($To)->format('Y-m-d')." 23:59:59";



        $a = InvoiceToProduct::whereBetween('created_at', [$dateFrom, $dateTo])->get();


        $fileName = $dateFrom.' '.$dateTo. time() . '_invoice.json';
        File::put(public_path('/exported/custom/invoice/json/'.$fileName),$a);

        return response()->download(public_path('/exported/custom/invoice/json/'.$fileName));
    }






    public function ExportCustomer( ){
        return view('export.json.salecustomer');
    }



    public function CustomerExportTojson(Request $request){

        $from = $request->from;
        $To= $request->to;
        $customer = $request->customer_id;

        $dateFrom = \Carbon\Carbon::parse($from)->format('Y-m-d')." 00:00:00";
        $dateTo = \Carbon\Carbon::parse($To)->format('Y-m-d')." 23:59:59";



        $a = InvoiceToProduct::where('customer_id', $customer)->whereBetween('created_at', [$dateFrom, $dateTo])->get();


        $fileName = $dateFrom.' '.$dateTo. time() . '_invoice.json';
        File::put(public_path('/exported/custom/invoice/json/'.$fileName),$a);

        return response()->download(public_path('/exported/custom/invoice/json/'.$fileName));
    }

    public function ProductToStock(){
        $product = ProductVatient::findorfail('1');

        $sale = InvoiceToProduct::where('product_id', '1')->sum('quantity');

    }

    public function StockinoutView(){

        return view('export.json.productstock');

    }

    public function Stockinout(Request $request){

        /*$from = $request->from;
        $To= $request->to;

        $product_id = $request->id;

        $dateFrom = \Carbon\Carbon::parse($from)->format('Y-m-d')." 00:00:00";
        $dateTo = \Carbon\Carbon::parse($To)->format('Y-m-d')." 23:59:59";



        $getcurrentStock = ProductVatienttoStock::whereBetween('created_at', [$dateFrom, $dateTo])
            ->where('product_vatient_id', $product_id)->sum('amount');



        $currstocksale = StockOut::whereBetween('created_at', [$dateFrom, $dateTo])
            ->where('product_variant_id' , $product_id)->sum('stock_out_quantity');*/

        $users = ProductVatient::all();

        $collect = collect($users);

        $lengths = $collect->map(function ($name, $key) {
            return [
                'id' => $name->id,
                'name' => $name->name,
                'color' => $name->color,
                'size' => $name->size,
                'stockout' => $name->Productsale()->sum('quantity'),
                'stockin' => $name->stock()->sum('amount')
            ];
        });

        $lengths->toArray();






        $fileName =  time() . '_product_status_invoice.json';
        File::put(public_path('/exported/custom/invoice/json/'.$fileName),$lengths);

        return response()->download(public_path('/exported/custom/invoice/json/'.$fileName));














    }
}
