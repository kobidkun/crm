<?php

namespace App\Http\Controllers\admin\product\catagory;

use App\Category2;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category3;
use Yajra\DataTables\DataTables;

class Turtiary extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


    public function CreateCatagory(){
        $sc = Category2::with('category')->get();
       // dd($sc);
      //  return response()->json($sc);

        return view('admin.page.catagory.tertiary.create',['scts' => $sc]);
    }

    public function DeleteCategoryTurtiary($id){
        $a = Category3::findorfail($id);
        $a->delete();
        return back();
    }




    public function CreateCatagorySave(Request $request){
        $s = new Category3();
        $s->name = $request->name;
        $s->slug = $request->slug;
        $s->description = $request->description;
        $s->catagory_id = $request->catagory_id;

        $s->product_id = $request->product_id;
        $s->save();
        //dd($s);
        return back();
    }

    public function AllCatagory(){
        return view('admin.page.catagory.tertiary.all');
    }

    public function getCatagoryDatatables()
    {
        $cats = Category3::select(['id','name','slug','description','slug']);

        return DataTables::of($cats)
            ->addColumn('details', function ($cat) {
                return '<a href="#edit-'.$cat->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
            ->make(true);
    }

    public function getCategoriesList(Request $request, $cat){
        $f = Category3::where('catagory_id', $cat)->get();
        return response()->json([
            'cat3' => $f
                ->makeVisible('id')
        ]);
    }
}
