<?php

namespace App\Http\Controllers\admin\product\catagory;

use App\Catagory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category2;
use Yajra\DataTables\DataTables;
class Secondary extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


    public function CreateCatagory(){
        $primarycat = Catagory::all();
        return view('admin.page.catagory.secondary.create',['primarycategories' => $primarycat]);
    }

    public function CreateCatagorySave(Request $request){
        $s = new Category2();
        $s->name = $request->name;
        $s->slug = $request->slug;
        $s->description = $request->description;
        $s->product_id = $request->product_id;
        $s->catagory_id = $request->catagory_id;
        $s->save();
        return back();
    }

    public function DeleteCategorySecondary($id){
        $a = Category2::findorfail($id);
        $a->delete();
        return back();
    }

    public function AllCatagory(){
        return view('admin.page.catagory.secondary.all');
    }

    public function getCatagoryDatatables()
    {
        $cats = Category2::select(['id','name','slug','description','slug']);

        return DataTables::of($cats)
            ->addColumn('details', function ($cat) {
                return '<a href="#edit-'.$cat->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
            ->make(true);
    }

    public function getCategoriesList(Request $request, $cat){
        $f = Category2::where('catagory_id', $cat)->get();
        return response()->json([
            'cat2' => $f
                ->makeVisible('id')
        ]);
    }
}
