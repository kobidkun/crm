<?php

namespace App\Http\Controllers\admin\product;

use App\Product\ProductAtribute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
class ManageAttribute extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


    public function CreateAttribute(Request $request){
        return view('admin.page.attribute.create');
    }

    public function CreateAttributeStore(Request $request){
        $a = new ProductAtribute();
        $a->name = $request->name;
        $a->type = $request->type;
        $a->default_value = $request->default_value;
        $a->is_filterable = $request->is_filterable;
        $a->is_searchable = $request->is_searchable;
        $a->is_swatch = $request->is_swatch;
        $a->description = $request->description;
        $a->save();

        return redirect(route('admin.attribute.show.all'));
        //
    }


    public function AllAttribute(Request $request){
        return view('admin.page.attribute.all');
    }

    public function AttributeDetails(Request $request, $id){
        return view('admin.page.attribute.details');
    }

    public function getAttribuleDatatables()
    {
        $cats = ProductAtribute::select(['id','name','default_value',
            'is_filterable','is_searchable','is_swatch']);

        return DataTables::of($cats)
            ->addColumn('details', function ($cat) {
                return '<a href="#edit-'.$cat->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
            ->make(true);
    }

}
