<?php

namespace App\Http\Controllers\admin\stock;

use App\Product;
use App\Stock;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
class ManageStock extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


   public function CreateStockView(){






   }

   public function StockSave(Request $request){





       $a = new Stock();
       $a->stock_number = $request->stock_number;
       $a->description = $request->description;
       $a->product_id = $request->product_id;
       $a->save();




       $b = Product::findorfail($request->product_id);

       $getStock = $b->stock;

       $b->stock = ($getStock+$request->stock_number);
       $b->save();



       return back();

   }

   public function DeleteStock(Request $request, $id){

       $fof = Stock::findorfail($id);

       $findpro = Product::findorfail($fof->product_id);
       $previousinstock = $findpro->stock;
       $findpro->stock =  $previousinstock - $fof->stock_number;
       $findpro->save();

       $fof->delete();
       return back();


   }



    public function getBasicData(Request $request, $id)
    {

        $productStock = Stock::where('product_id', $id)
        ->get();
        $users = Stock::where('product_id', $id)->select(['id','stock_number','description','created_at']);



        return Datatables::of($users)
            ->editColumn('created_at', function($user) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->toDayDateTimeString();
            })
            ->addColumn('action', function ($user) {
                return '<a href="/admin/product/get/delete/'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>';
            })
            ->rawColumns(['action'])
            ->make();
    }




}
