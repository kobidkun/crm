<?php

namespace App\Http\Controllers\admin\FrontEnd\Slider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class WebsiteSlider extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


    public function View(){
        $a = \App\Model\FrontEnd\Slider\WebsiteSlider::all();
        return view('admin.page.frontend.websiteslider')->with(['images' => $a]);
    }
    public function Upload(Request $request){

        $imageName1 = time().'.'.$request->image->getClientOriginalExtension();
        $imageName2 = str_replace(' ', '_', $imageName1);
        $imageName = str_replace(' ', '_', $imageName1);
        $image = $request->file('image');
        $t = Storage::disk('s3')->put('slider/website/'.$imageName, file_get_contents($image), 'slider/website/');
        $imageName = Storage::disk('s3')->url('slider/website/'.$imageName);
        // $imageName3 = Storage::disk('s3')->url('product_image/primary/'.$imageName);

        //  $replacesextraspaces1 = str_replace(' ', '_', $finalimageurl1);

        $s = new \App\Model\FrontEnd\Slider\WebsiteSlider();
        $s->url = $imageName;
        $s->title = $imageName2;

        $s->cdn_url = 'https://cdn.emp.tecions.xyz/slider/website/'.$imageName2;

        $s->name = $imageName2;
        $s->save();
        return $imageName2;
    }


    public function Delete($id){
        $img = \App\Model\FrontEnd\Slider\WebsiteSlider::findorfail($id);

        Storage::disk('s3')->delete('slider/website/'.$img->name);
        $img->delete();
        return redirect()->back();
    }
}
