<?php

namespace App\Http\Controllers\admin;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ManageAdmin extends Controller
{


    public function __construct()
    {
        $this->middleware(['auth:admin']);


    }

    public function View(){

        return view('admin.password.changepassword');



    }
   public function changePassword(Request $request){

       $pass1 = $request->password;
       $pass2 = $request->password2;


       if ($pass1 === $pass2){

       $id = Auth::id();

       $a = Admin::findorfail($id);

       $a->password = bcrypt($pass1);

       $a->save();

           return 'Your Password Changed successfully. Please Click back button and logout and login again';



       } else {
           return 'Your Password Does not Match. Please Click back button and try again';
       }


   }
}
