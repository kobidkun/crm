<?php

namespace App\Http\Controllers\publiccontroller;

use App\Model\BVInvoice\CreateInvoiceBV;
use App\Model\BVInvoice\ReturnInvoiceBV;
use App\model\Invoice\CreateInvoice;
use App\Model\Invoice\ReturnInvoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
class PublicWebConreoller extends Controller
{
    public function exportPdfInvoice($slug){
        $invoice = CreateInvoice::where('invoice_secure_id', $slug)->first();

        $invoicepdf = PDF::loadView('admin.page.Invoice.generatepdffrominvoice',['invoice' => $invoice])->setPaper('a4');

        return $invoicepdf->download('invoice'.$invoice->invoice_number.'-'.$invoice->invoice_customer_name.'.pdf');


    }

    public function exportPdfInvoicebv($slug){
        $invoice = CreateInvoiceBV::where('invoice_secure_id', $slug)->first();

        $invoicepdf = PDF::loadView('admin.page.bvInvoice.generatepdffrominvoice',['invoice' => $invoice])->setPaper('a4');

        return $invoicepdf->download('invoice'.$invoice->invoice_number.'-'.$invoice->invoice_customer_name.'.pdf');


    }

    public function exportPdfReturnInvoice($slug){
        $invoice = ReturnInvoice::where('invoice_secure_id', $slug)->first();

        $invoicepdf = PDF::loadView('admin.page.returnInvoice.generatepdffrominvoice',['invoice' => $invoice])->setPaper('a4');

        return $invoicepdf->download('invoice'.$invoice->invoice_number.'-'.$invoice->invoice_customer_name.'.pdf');


    }

    public function exportPdfReturnInvoiceBV($slug){
        $invoice = ReturnInvoiceBV::where('invoice_secure_id', $slug)->first();

        $invoicepdf = PDF::loadView('admin.page.returnInvoicebv.generatepdffrominvoice',['invoice' => $invoice])->setPaper('a4');

        return $invoicepdf->download('invoice'.$invoice->invoice_number.'-'.$invoice->invoice_customer_name.'.pdf');


    }
}
