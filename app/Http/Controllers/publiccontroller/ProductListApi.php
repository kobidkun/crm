<?php

namespace App\Http\Controllers\publiccontroller;

use App\Catagory;
use App\Http\Controllers\admin\FrontEnd\Slider\WebsiteSlider;
use App\Model\FrontEnd\Slider\MobileSloder;
use App\ProductsToCategory;

use App\ProductVatient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;

use Spatie\QueryBuilder\QueryBuilder;


class ProductListApi extends Controller
{
    public function ProductsApiList()
    {

       $products = Product::with(
           'product_primary_images',
                    'products_to_categories',
           'products_to_categories2',
           'products_to_categories3'
       )->paginate(12);






        return response()->json([

            'product' => $products,

        ]);








    }


    public function GetProductById($slug){
        $product = Product::with(
            'product_images',
            'product_primary_images',
            'product_ratingss',
            'products_to_categories2',
            'products_to_categories3',
            'products_to_categories',
            'product_to_varients'



        )->where('slug', $slug)->firstOrFail();
        return response()->json([
           'product' => $product
            ->makeVisible('l_desc')
        ]);
    }


    public function GetProductVariantById($slug){
        $product = ProductVatient::with(
            'Images'

        )->where('slug', $slug)->firstOrFail();
        return response()->json([
           'product' => $product
        ]);
    }


    public function GetProductByCount($count){
        $products = Product::take($count)->inRandomOrder()->get();


        $allproducts = [];

        foreach ($products as $key => $product) {
            $allproducts[] = [
                'id' => $product->id,
                'slug' => $product->slug,

                'name' => $product->name,
                //'img' => $product->product_primary_images->name,
                'price' => $product->base_price,
                'disc_price' => $product->disc_price,
                'img' => $product->product_primary_images
            ];
        }


        return response()->json($allproducts);

    }

    public function GetProductByCategory($calslud){

        $cat = ProductsToCategory::with('products','product_primary_images','product_to_atributes')
            ->where('slug', $calslud)->paginate(12);
       // $products = $cat->products;



        /*$allcat = [];
        foreach ($cat as $key => $cat) {
            $allcat[] = [
                'name' => $cat->name,
                'product' => $this->getcatcountandproduct($cat)
            ];

        }*/


        return response()->json($cat);

    }

    public function getcatcountandproduct($cat){

        $products = $cat->products;

        $allproducts  = [];


        foreach ($products as $key => $product) {
            $allproducts[] = [
                'id' => $product->id,
                'slug' => $product->slug,

                'name' => $product->name,
                //'img' => $product->product_primary_images->name,
                'price' => $product->base_price,
                'disc_price' => $product->disc_price,
                'img' => $product->product_primary_images
            ];
        }

        return $allproducts;
    }








    public function ProductsApiListbkp(){

      $products =  $p =  Product::with(
            'product_primary_images'

        )->get();


        return response()->json([
           'product' => $p
        ],200);


    }


    public function MobileSlider(){
        $a = MobileSloder::all();

        return response()->json($a,200);
    }

    public function WebsiteSlider(){
        $a = \App\Model\FrontEnd\Slider\WebsiteSlider::all();

        return response()->json($a,200);
    }

    public function ProductbyCategory(){

        // ProductToAtribute


        $users = QueryBuilder::for(Product\ProductToAtribute::class)
            ->allowedFilters('value')
            ->allowedIncludes('productstwo','productstwo.images','productstwo.image')
            ->paginate(12);




        $mul = $users;






        return response()->json([

            'product' => $mul,

        ]);
    }



}
