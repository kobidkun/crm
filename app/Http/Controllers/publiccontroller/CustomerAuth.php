<?php

namespace App\Http\Controllers\publiccontroller;

use App\Customer;
use App\Mail\Customer\Activate;
use App\Mail\Customer\Register;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class CustomerAuth extends Controller
{
    public function CreateCustomerSave(\App\Http\Requests\Customer\CustomerAuth $request){
        $s = new Customer\TempCustomer();
        $s->fname = $request->fname;
        $s->lname = $request->lname;
        $s->dob = $request->dob;
        $s->sponser_id = $request->sponser_id;
        $s->email = $request->email;
        $s->mobile = $request->mobile;
        $s->save();

        $a = $s;






        $this->Sendsms($a);
      //  $this->SendEmail($a);




        return response()->json($a,200);
    }

    function Sendsms($a){


        //Your authentication key
        $authKey = "143565AIE7jf1Mb5abd98a0";

        //Multiple mobiles numbers separated by comma
        $mobileNumber = $a->mobile;

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "EMPMKT";

        //Your message to send, Add URL encoding here.
        $message = urlencode("Thanks for becoming a part of Emporium Marketing Family.To activate your account please call Customer Care");

        //Define route
        $route = "4";
        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

        //API URL
        $url="https://control.msg91.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output = curl_exec($ch);



        //endsms




//$customerDetails->mobile



    }

    function SendEmail($a){

        Mail::to($a->email)->send(new Activate($a));
    }

}
