<?php

namespace App\Http\Controllers\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Customer\CustomerFiles as CF;
class CustomerFiles extends Controller
{
    public function savefiles(Request $request){

        $path = $request->file('image')->store('customer/files');

      //  $path->move(public_path().'/'.$path);


        $a = new CF();

        $a->name = $request->name;
        $a->customer_id = $request->customer_id;
        $a->description = $request->description;
        $a->id_number = $request->id_number;
        $a->location = $path;

        $a->path = 'customer/files/';

        $a->save();
        return back();


    }
}
