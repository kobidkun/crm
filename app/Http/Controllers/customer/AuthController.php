<?php

namespace App\Http\Controllers\customer;

use App\Customer;
use App\Http\Requests\CustomerLogin;
use App\Http\Requests\CustomerRegister;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
class AuthController extends Controller
{
    public function loginPage(){

        return view('customer.page.customer.login');

    }


    public function CustomerRegisterShow(Request $request){

    }

    public function CustomerRegisterSave(CustomerRegister $request ){

        $s = new Customer();
        $s->fname = $request->fname;
        $s->lname = $request->lname;
        $s->email = $request->email;
        $s->ref = $request->ref;
        $s->mobile = $request->mobile;
        $s->password = bcrypt($request->password);
        $s->save();
        return response()->json('successful',200);

    }

    public function CustomerLoginShow(Request $request){

    }
    public function __construct()
    {
        $this->middleware('guest:customer', ['except' => ['logout']]);
    }

    public function CustomerLogin(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:customers,email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput($request->only('email', 'remember'));
        }


        // Attempt to log the user in
        if (Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->intended(route('customer.dashboard'));
        }
        // if unsuccessful, then redirect back to the login with the form data
        return redirect() ->back()
            ->withErrors($validator, 'password')
            ->withInput($request->only('email', 'remember'));

    }


}
