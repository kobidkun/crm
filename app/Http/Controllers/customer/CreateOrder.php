<?php

namespace App\Http\Controllers\Customer;

use App\Customer;
use App\Model\Initial\Order\InitialCreateOrder;
use App\Model\Initial\Order\InitialCreateOrderToProduct;
use App\ProductVatient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Model\Order\OrderToProduct;
use PDF;
use App\Mail\customer\CreateOrder as EmailToCreteOrder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class CreateOrder extends Controller
{


    public function createOrderRecipt(Request $request)
    {

        Mail::to('chrisn@scotch.io')->send(new EmailToCreteOrder);


        $pdf = PDF::loadView('service.email.neworder')->setPaper('a4');

        $pdf->save('storage/invoice/invoice001.pdf');


        //  return $pdf->download('invoice.pdf');

        // return $savepdf;


    }


    public function __construct()
    {
        $this->middleware('auth:customer-api');
    }


    public function CreateOrderSave(Request $request)
    {

        $getcustomer = $request->user();


        $createorder = new \App\Model\Order\CreateOrder();
        $createsecureorderid = ('OD' . $this->random_str('alphanum', 10));
        //invoice request

        $createorder->customer_id = $getcustomer->id;
        $createorder->customer_gstin = $request->customer_gstin;
        $createorder->order_secure_id = $createsecureorderid;
        $createorder->order_total_taxable_amount = $request->order_total_taxable_amount;
        $createorder->order_total_taxable_discount_amount = $request->order_total_taxable_discount_amount;
        $createorder->order_total_tax_cgst = $request->order_total_tax_cgst;
        $createorder->order_total_tax_sgst = $request->order_total_tax_sgst;
        $createorder->order_total_tax_igst = $request->order_total_tax_igst;

        $createorder->order_total_tax = $request->order_total_tax;
        $createorder->order_total = $request->order_total;
        $createorder->payment_type = $request->payment_type;
        $createorder->bookedfrom = $request->bookedfrom;
        $createorder->save();


        $products = $request->input('product');

        foreach ($products as $key => $value) {
            $createorder->order_to_products()->create([
                'order_id' => $createorder->id,
                'product_id' => $value['product_id'],
                'parent_product_id' => $value['parent_product_id'],
                'customer_id' => $getcustomer->id,
                'color' => $value['color'],
                'size' => $value['size'],
                'product_name' => $value['product_name'],
                'quantity' => $value['quantity'],
                'hsn' => $value['hsn'],
                'type' => $value['type'],
                'taxable_rate' => $value['taxable_rate'],
                'taxable_value' => $value['taxable_value'],
                'taxable_discount_amount' => $value['taxable_discount_amount'],
                'taxable_tax_cgst' => $value['taxable_tax_cgst'],
                'taxable_tax_cgst_percentage' => $value['taxable_tax_cgst_percentage'],
                'taxable_tax_sgst' => $value['taxable_tax_sgst'],
                'taxable_tax_sgst_percentage' => $value['taxable_tax_sgst_percentage'],
                'taxable_tax_igst' => $value['taxable_tax_igst'],
                'taxable_tax_igst_percentage' => $value['taxable_tax_igst_percentage'],
                'taxable_tax_cess' => $value['taxable_tax_cess'],
                'taxable_tax_cess_percentage' => $value['taxable_tax_cess_percentage'],
                'taxable_tax_total' => $value['taxable_tax_total'],
                'total' => $value['total'],


            ]);
        }


        /*    $invoicepdf = PDF::loadView('service.email.neworder',[
                   'products' => $products,
                   'getcustomer' => $getcustomer,
                   'createorder' => $createorder,

               ])->setPaper('a4');

               $invoicepdf->save('public/invoice/'.$createsecureorderid.'.pdf');*/


        //     $order = $createorder;
        //   $customer_name = $getcustomer->fname;
        //  Mail::to($getcustomer->email)->send(new EmailToCreteOrder($order,$createsecureorderid,$customer_name));

        /*sms*/

        /*Send SMS using PHP*/

        //Your authentication key
        $authKey = "143565AIE7jf1Mb5abd98a0";

        //Multiple mobiles numbers separated by comma
        $mobileNumber = $getcustomer->mobile;

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "EMPMKT";

        //Your message to send, Add URL encoding here.
        $message = urlencode("Thanks for Shopping with Emporium Marketing your order id is " . $createorder->id . " Have a Pleasent Day");

        //Define route
        $route = "4";
        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

        //API URL
        $url = "https://control.msg91.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output = curl_exec($ch);



        //endsms


        return response()->json([
            'customer_id' => $getcustomer->id,
            'order' => $createorder->id,
            'status' => 'success',
            'order_id' => $createsecureorderid,
            'products' => $products
        ]);


    }


    function random_str($type = 'alphanum', $length = 10)
    {
        switch ($type) {
            case 'basic'    :
                return mt_rand();
                break;
            case 'alpha'    :
            case 'alphanum' :
            case 'num'      :
            case 'nozero'   :
                $seedings = array();
                $seedings['alpha'] = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['alphanum'] = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['num'] = '0123456789';
                $seedings['nozero'] = '123456789';

                $pool = $seedings[$type];

                $str = '';
                for ($i = 0; $i < $length; $i++) {
                    $str .= substr($pool, mt_rand(0, strlen($pool) - 1), 1);
                }
                return $str;
                break;
            case 'unique'   :
            case 'md5'      :
                return md5(uniqid(mt_rand()));
                break;
        }
    }


    public function CreateOrderSave2(Request $request)
    {

        $getcustomer = $request->user();


        $createinitialorder = new InitialCreateOrder();

        $createinitialorder->payment_type = $request->payment_type;
        $createinitialorder->bookedfrom = $request->bookedfrom;
        $createinitialorder->customer_id = $getcustomer->id;

        $createinitialorder->save();


        $products = $request->input('product');

        foreach ($products as $key => $value) {


            $createinitialorder->initial_create_order_to_products()->create([

                'product_id' => $value['product_id'],
                'quantity' => $value['quantity'],
                'parent_product_id' => $value['parent_product_id'],
                'initial_create_orders_id' => $createinitialorder->id,
                'customer_id' => $getcustomer->id,


            ]);
        }


        $a = $this->CreateOrderSave3($createinitialorder);


        return response()->json([
            'products' => $createinitialorder,
            'product' => $a,
        ]);


    }


    public function CreateOrderSave3($createinitialorder)
    {

        $getcustomer = Customer::findorfail($createinitialorder->customer_id);


        $createorder = new \App\Model\Order\CreateOrder();
        $createsecureorderid = ('OD' . $this->random_str('alphanum', 10));


        $productssums = InitialCreateOrder::with('initial_create_order_to_products')
            ->where('id', $createinitialorder->id)->first();


        $products = InitialCreateOrderToProduct::where('initial_create_orders_id', $productssums->id)->get();


        $order_total_taxable_amount = $products->sum(function ($row) {

            $productid = $row->product_id;

            $quantity = $row->quantity;

            $getProduct = ProductVatient::findorfail($productid);

            return round($getProduct->taxable_rate * $quantity,2);

        });


        $order_total_tax_cgst = $products->sum(function ($product) {

            $productid = $product->product_id;

            $quantity = $product->quantity;

            $getProduct = ProductVatient::findorfail($productid);

            return round(($getProduct->cgst_percentage / 100) * ($getProduct->taxable_rate * $quantity),2);
        });

        $order_total_tax_sgst = $products->sum(function ($product) {

            $productid = $product->product_id;

            $quantity = $product->quantity;

            $getProduct = ProductVatient::findorfail($productid);

            return round(($getProduct->sgst_percentage / 100) * ($getProduct->taxable_rate * $quantity),2);
        });


        $order_total_tax = $products->sum(function ($product) {

            $productid = $product->product_id;

            $quantity = $product->quantity;

            $getProduct = ProductVatient::findorfail($productid);

            return round(($getProduct->sgst_percentage / 100) * 2 * ($getProduct->taxable_rate * $quantity),2);
        });


        $order_total = $products->sum(function ($product) {

            $productid = $product->product_id;

            $quantity = $product->quantity;

            $getProduct = ProductVatient::findorfail($productid);

            return round(($getProduct->listing_price) * $quantity,2);
        });


        $createorder->customer_id = $getcustomer->id;
        $createorder->customer_gstin = $getcustomer->gstin;
        $createorder->order_secure_id = $createsecureorderid;
        $createorder->order_total_taxable_amount = $order_total_taxable_amount;
        $createorder->order_total_taxable_discount_amount = '0';
        $createorder->order_total_tax_cgst = $order_total_tax_cgst;
        $createorder->order_total_tax_sgst = $order_total_tax_sgst;
        $createorder->order_total_tax_igst = '0';

        $createorder->order_total_tax = $order_total_tax;
        $createorder->order_total = $order_total;
        $createorder->payment_type = $productssums->payment_type;
        $createorder->bookedfrom = $productssums->bookedfrom;
        $createorder->save();


        foreach ($products as $key => $value) {

            $productid = $value->product_id;

            $quantity = $value['quantity'];

            $getProduct = ProductVatient::findorfail($productid);

            $taxablecgst = round(($getProduct->cgst_percentage / 100) * ($getProduct->taxable_rate * $quantity),2);
            $taxablesgst = round(($getProduct->sgst_percentage / 100) * ($getProduct->taxable_rate * $quantity),2);

            $createorder->order_to_products()->create([
                'order_id' => $createorder->id,
                'product_id' => $productid,
                'parent_product_id' => $getProduct->product_id,
                'customer_id' => $getcustomer->id,
                'color' => $getProduct->color,
                'size' => $getProduct->size,
                'product_name' => $getProduct->name,
                'quantity' => $quantity,
                'hsn' => $getProduct->hsn,
                'type' => $getProduct->type,
                'taxable_rate' => $getProduct->taxable_rate,
                'taxable_value' => round($getProduct->taxable_rate * $quantity,2),
                'taxable_discount_amount' => '0',
                'taxable_tax_cgst' => $taxablecgst,
                'taxable_tax_cgst_percentage' => $getProduct->cgst_percentage,
                'taxable_tax_sgst' => $taxablesgst,
                'taxable_tax_sgst_percentage' => $getProduct->sgst_percentage ,
                'taxable_tax_igst' =>$taxablecgst + $taxablesgst,
                'taxable_tax_igst_percentage' => round($getProduct->cgst_percentage + $getProduct->sgst_percentage,2),
                'taxable_tax_cess' => '0',
                'taxable_tax_cess_percentage' => '0',
                'taxable_tax_total' => $taxablecgst + $taxablesgst,
                'total' => round(($getProduct->taxable_rate * $quantity) + ($taxablecgst + $taxablesgst),2),


            ]);
        }


        DB::table('customer_cart_items')->where('customer_id', '=', $getcustomer->id)->delete();



        return ([

            'orders' => $createorder,
            // 'products' => $prod
        ]);


    }


}
