<?php

namespace App\Http\Controllers\Customer;

use App\Customer\CustomerCartItems;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:customer-api');
    }


    public function StoreCart(Request $request){

        $getcustomer = $request->user();

        // dd($getcustomer);

        $reqProd = $request->product_id;

        if (CustomerCartItems::where('customer_id',$getcustomer->id)
            ->where('product_id',$request->product_id )->exists()
        ) {


            $a = CustomerCartItems::where('product_id', $reqProd)->first();


            $reqqty = $request->quantity;
            $initqty = $a->quantity;


            $a->quantity = ($initqty+$reqqty);
            $a->color = $request->color;
            $a->size = $request->size;

            $a->save();

            $getqty = $a->quantity;









            return response()->json([

             'message' =>  'Product added in your Cart',
             'state' =>  'success'

            ],200);

        } elseif (CustomerCartItems::where('customer_id',$getcustomer->id)
            ->where('product_id',$request->product_id )->doesntExist()){

            $a = new CustomerCartItems();
            $a->product_id = $request->product_id;
            $a->customer_id = $getcustomer->id;
            $a->quantity = $request->quantity;
            $a->color = $request->color;
            $a->size = $request->size;
            $a->save();
            return response()->json([

                 'message' =>  'Product added to cart',
             'state' =>  'success'

                ,200

            ]);
        } else{

            return response()->json('error adding product to cart',200);

        }


    }


    public function GetCartItem(Request $request){
        $getcustomer = $request->user();
        $getid = $getcustomer->id;

        $f = CustomerCartItems::with('product','images')
        ->where('customer_id',$getid)->get();
        $cartcount = $f->count();


        return response()->json([
            'cart' => $f,
            'customer' => $getid,
            'cart_count' =>$cartcount
        ]);

    }


    public function DeleteCartItem(Request $request, $id){

        $f = CustomerCartItems::findorfail($id);

        $f->delete();


        return response()->json('Product Deleted From Cart',200);


    }




}
