<?php

namespace App\Http\Controllers\Customer;

use App\Customer\CustomerCartItems;
use App\Master\Types\BVTypes;
use App\model\master\CreateBvPurchaseType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerData extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:customer-api');
    }


   public function Profile(Request $request){

       $getcustomer = $request->user();

       return response()->json($getcustomer, 200);

   }

   public function GetOrder(Request $request){
       $getcustomer = $request->user();

       $customer_id = $getcustomer->id;




        $a = \App\Model\Order\CreateOrder::with([
            'order_to_products'
        ])->where('customer_id', $customer_id)->latest()->get();

        return response()->json($a,200);

   }

   public function GetBVOrder(Request $request){

       $getcustomer = $request->user();

       $customer_id = $getcustomer->id;




       $a = \App\Model\Order\BVOrder\CreateBVOrder::with([
           'order_to_products'
       ])->where('customer_id', $customer_id)->latest()->get();

       return response()->json($a,200);

   }

   public function GetInvoice(Request $request){

       $getcustomer = $request->user();

       $customer_id = $getcustomer->id;




       $a = \App\Model\Invoice\CreateInvoice::with([
           'invoice_to_products'
       ])->where('customer_id', $customer_id)->latest()->get();

       return response()->json($a,200);

   }

   public function GetBVInvoice(Request $request){

       $getcustomer = $request->user();

       $customer_id = $getcustomer->id;




       $a = \App\Model\BVInvoice\CreateInvoiceBV::with([
           'invoice_to_products'
       ])->where('customer_id', $customer_id)->latest()->get();

       return response()->json($a,200);

   }

   public function GetReturnInvoice(Request $request){

       $getcustomer = $request->user();

       $customer_id = $getcustomer->id;




       $a = \App\Model\Invoice\ReturnInvoice::with([
           'invoice_to_products'
       ])->where('customer_id', $customer_id)->latest()->get();

       return response()->json($a,200);


   }

   public function GetReturnBVInvoice(Request $request){

       $getcustomer = $request->user();

       $customer_id = $getcustomer->id;

       $a = \App\Model\BVInvoice\ReturnInvoiceBV::with([
           'return_invoiceto_products'
       ])->where('customer_id', $customer_id)->latest()->get();

       return response()->json($a,200);

   }


    public function getForApiforBvPurchaseTypes(){

        $a = BVTypes::all();

        return response()->json($a,200);
    }




}
