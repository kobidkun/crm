<?php

namespace App\Http\Controllers\storeuser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Auth;



class AuthConreoller extends Controller
{


    public function loginview(){

        return view('storeuser.auth.login');

    }

    public function StoreLogin(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput($request->only('email', 'remember'));
        }


        // Attempt to log the user in
        if (Auth::guard('storeuser')->attempt(['email' => $request->email, 'password' => $request->password],
            $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->intended(route('storeuser.dashboard.view'));
        }
        // if unsuccessful, then redirect back to the login with the form data
        return redirect() ->back()
            ->withErrors($validator, 'password')
            ->withInput($request->only('email', 'remember'));

    }

    public function logout()
    {
        Auth::guard('storeuser')->logout();
        return redirect(route('storeuser.login'));
    }
}
