<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPrimaryImage extends Model
{
    protected $fillable = [
        'name', 'images_url'
    ];
    protected $hidden = array('id',
        'product_id',
        'updated_at',
        'path',
        'local_path',
        'extinsion',
        'images_url',
        'created_at'
    );


    public function products()
    {
        return $this->belongsTo('App\Product');
    }
}
