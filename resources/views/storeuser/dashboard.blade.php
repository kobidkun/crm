@extends('admin.index');

@section('content')

    <div class="m-content">

        <div class="row">


                <div class="col-md-12">
                    <!--begin::Portlet-->


                <div id="container"></div>


                </div>






        </div>



        <div class="row" style="padding-top: 30px">
            <div class="col-md-4" >
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Orders Last 30 Days (updated Daily)
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="m-widget4__chart m-portlet-fit--sides m--margin-top-10 m--margin-top-20" >
                                <canvas id="m_chart_sales_sales_daily"></canvas>
                            </div>
                        </div>

                        <!--end::Section-->
                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>
            <div class="col-md-4">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Orders Value Last 30 Days
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">

                            <div class="m-widget1">
                                <div class="m-widget1__item">
                                    <div class="row m-row--no-padding align-items-center">
                                        <div class="col">
                                            <h5 class="m-widget1__title">
                                                Order Placed Value
                                            </h5>

                                        </div>
                                        <div class="col m--align-right">
														<span class="m-widget1__number m--font-brand">
														<i class="la la-inr"></i>	{{$monthlysalestext}}
														</span>
                                        </div>
                                    </div>


                                    <div class="row m-row--no-padding align-items-center">
                                        <div class="col">
                                            <h5 class="m-widget1__title">
                                                Order Tax Value
                                            </h5>

                                        </div>
                                        <div class="col m--align-right">
														<span class="m-widget1__number m--font-brand">
														<i class="la la-inr"></i>	{{$monthlytax}}
														</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-widget1__item">
                                    <div class="row m-row--no-padding align-items-center">
                                        <div class="col">
                                            <h3 class="m-widget1__title">
                                                Order Quantity
                                            </h3>

                                        </div>
                                        <div class="col m--align-right">
														<span class="m-widget1__number m--font-danger">
															+ {{$monthlyqtytext}}
														</span>Units
                                        </div>
                                    </div>
                                </div>


                            </div>




                        </div>

                        <!--end::Section-->
                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>
            <div class="col-md-4">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Daily Sales Last 30 Days
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">

                            <div class="m-widget14__chart" >
                                <canvas  id="m_chart_daily_sales"></canvas>
                            </div>




                        </div>

                        <!--end::Section-->
                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>
        </div>




    </div>



    <div class="m-content">


    </div>

 @endsection

@section('admin_footer_script')
    <style>
        .highcharts-credits{
            display: none;!important;
        }
    </style>
    <script>//== Class definition
        var Dashboard = function() {

            //== Sparkline Chart helper function


            //== Daily Sales chart.
            //** Based on Chartjs plugin - http://www.chartjs.org/


            //== Profit Share Chart.
            //** Based on Chartist plugin - https://gionkunz.github.io/chartist-js/index.html


            //== Sales Stats.
            //** Based on Chartjs plugin - http://www.chartjs.org/
            var salesStats = function() {
                if ($('#m_chart_sales_sales_daily').length == 0) {
                    return;
                }

                var config = {
                    type: 'line',
                    data: {
                        labels: [

                           @foreach($monthlysales as $monthlysale)

                        ' {{$monthlysale->dates}}' ,

                            @endforeach
                        ],
                        datasets: [{
                            label: "Total Sales in INR ",
                            borderColor: mUtil.getColor('brand'),
                            borderWidth: 2,
                            pointBackgroundColor: mUtil.getColor('brand'),

                            backgroundColor: mUtil.getColor('accent'),

                            pointHoverBackgroundColor: mUtil.getColor('danger'),
                            pointHoverBorderColor: Chart.helpers.color(mUtil.getColor('danger')).alpha(0.2).rgbString(),
                            data: [
                                @foreach($monthlysales as $monthlysale)

                           {{$monthlysale->total}},

                                @endforeach
                            ]
                        }]
                    },
                    options: {
                        title: {
                            display: true,
                        },
                        tooltips: {
                            intersect: false,
                            mode: 'nearest',
                            xPadding: 10,
                            yPadding: 10,
                            caretPadding: 10
                        },
                        legend: {
                            display: false,
                            labels: {
                                usePointStyle: false
                            }
                        },
                        responsive: true,
                        maintainAspectRatio: false,
                        hover: {
                            mode: 'index'
                        },
                        scales: {
                            xAxes: [{
                                display: false,
                                gridLines: false,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Month'
                                }
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: false,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Value'
                                }
                            }]
                        },

                        elements: {
                            point: {
                                radius: 3,
                                borderWidth: 0,

                                hoverRadius: 8,
                                hoverBorderWidth: 2
                            }
                        }
                    }
                };

                var chart = new Chart($('#m_chart_sales_sales_daily'), config);
            }

            //== Daily Sales chart.
            //** Based on Chartjs plugin - http://www.chartjs.org/
            var dailySales = function() {
                var chartContainer = $('#m_chart_daily_sales');

                if (chartContainer.length == 0) {
                    return;
                }

                var chartData = {
                    labels: [
                        @foreach($dailysalesqty as $monthlysale)

                       '  {{     $monthlysale->dates }} ' ,
                  //

                        @endforeach

                    ],
                    datasets: [{
                        //label: 'Dataset 1',
                        backgroundColor: mUtil.getColor('success'),
                        label: "Sold Quantity ",
                        data: [
                            @foreach($dailysalesqty as $monthlysale)

                            {{$monthlysale->total}} ,

                            @endforeach
                        ]
                    }]
                };

                var chart = new Chart(chartContainer, {
                    type: 'bar',
                    data: chartData,
                    options: {
                        title: {
                            display: true,
                            text:'Quantity '

                        },
                        tooltips: {
                            intersect: false,
                            mode: 'nearest',
                            xPadding: 10,
                            yPadding: 10,
                            caretPadding: 10
                        },
                        legend: {
                            display: false
                        },
                        responsive: true,
                        maintainAspectRatio: false,
                        barRadius: 4,
                        scales: {
                            xAxes: [{
                                display: false,
                                gridLines: false,
                                stacked: true
                            }],
                            yAxes: [{
                                display: false,
                                stacked: true,
                                gridLines: false
                            }]
                        },
                        layout: {
                            padding: {
                                left: 0,
                                right: 0,
                                top: 0,
                                bottom: 0
                            }
                        }
                    }
                });
            }

            //== Profit Share Chart.
            //** Based on Chartist plugin - https://gionkunz.github.io/chartist-js/index.html


            //== Sales Stats.



            return {
                //== Init demos
                init: function() {
                    // init charts

                    salesStats();

                   dailySales();
                }
            };
        }();

        //== Class initialization on page load
        jQuery(document).ready(function() {
            Dashboard.init();
        });
    </script>

    <script src="{{asset('production/js/highcharts-with-moment.min.js')}}"></script>
    <script>


        function requestData() {
            $.ajax({
                url: '{{route('realtime.graph')}}',
                success: function(point) {
                    var series = chart.series[0],
                        shift = series.data.length > 20; // shift if the series is
                                                         // longer than 20

                    // add the point
                    chart.series[0].addPoint(point, true, shift);

                    // call it again after one second
                    setTimeout(requestData, 1000);
                },
                cache: false
            });
        }

        document.addEventListener('DOMContentLoaded', (function() {


                Highcharts.theme = {
                    colors: ['#3f74ff', '#95C471', '#35729E', '#251735'],

                    colorAxis: {
                        maxColor: '#3f74ff',
                        minColor: '#F3E796'
                    },

                    plotOptions: {
                        map: {
                            nullColor: '#fcfefe'
                        }
                    },

                    navigator: {
                        maskFill: 'rgba(170, 205, 170, 0.5)',
                        series: {
                            color: '#95C471',
                            lineColor: '#35729E'
                        }
                    }
                };

// Apply the theme
                Highcharts.setOptions(Highcharts.theme);

                Highcharts.setOptions({
                    time: {
                        timezone: 'Asia/Calcutta'
                    }
                });


                chart = Highcharts.chart('container', {
                    chart: {
                        type: 'area',
                        events: {
                            load: requestData
                        }
                    },
                    title: {
                        text: 'Live Order Status'
                    },
                    xAxis: {
                        type: 'datetime',
                        tickPixelInterval: 150,
                        maxZoom: 20 * 1000
                    },
                    yAxis: {
                        minPadding: 0.2,
                        maxPadding: 0.2,
                        title: {
                            text: 'No of Order Placed Today',
                            margin: 80
                        }
                    },
                    series: [{
                        name: 'No of Order Placed Today',
                        data: []
                    }]
                });
            })
        );









    </script>




    @endsection