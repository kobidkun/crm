@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">




            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Change Password
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">







                            <form class="m-form m-form--fit m-form--label-align-right"
                                  role="form" method="post"
                                  action="{{ route('admin.password.change.store') }}"
                            >
                                {{ csrf_field() }}
                                <div class="m-portlet__body">

                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            New Password
                                        </label>
                                        <div class="input-group m-input-group">

                                            <input type="password"
                                                   class="form-control m-input--air category-name"
                                                   placeholder="password"
                                                   name="password"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>

                                    </div>


                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Type Again
                                        </label>
                                        <div class="input-group m-input-group">

                                            <input type="password"
                                                   class="form-control m-input--air category-name"
                                                   placeholder="password"
                                                   name="password2"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>

                                    </div>









                                </div>






                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--end::Section-->

                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>









        </div>
    </div>

 @endsection



@section('admin_footer_script')


    @endsection