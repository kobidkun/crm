@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Create new Attribute
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">


                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif




                            <form class="m-form m-form--fit m-form--label-align-right"
                                  role="form" method="post"
                                  action="{{ route('admin.attribute.create.store') }}"
                            >
                                {{ csrf_field() }}
                                <div class="m-portlet__body">

                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Attribute Name
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="flaticon-list"></i>
														</span>
                                            </div>
                                            <input type="text"
                                                   class="form-control m-input--air category-name"
                                                   placeholder="Attribute Name"
                                                   name="name"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>
                                        <span class="m-form__help">
													Do not Create Same Attribute Twice
												</span>
                                    </div>
                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Attribute Default Value
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="flaticon-more-v6"></i>
														</span>
                                            </div>
                                            <input type="text"
                                                   class="form-control m-input--air"
                                                   placeholder="Attribute Default Value"
                                                   name="default_value"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>
                                        <span class="m-form__help">
													Default Value
												</span>
                                    </div>


                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Is this Filterable
                                        </label>
                                        <div class="input-group m-input-group">
                                            <select class="form-control m-bootstrap-select m_selectpicker"
                                                    name="is_filterable"
                                            >
                                                <option value="yes">Yes</option>
                                                <option value="no">No</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Is this Searchable
                                        </label>
                                        <div class="input-group m-input-group">
                                            <select class="form-control m-bootstrap-select m_selectpicker"
                                                    name="is_searchable"
                                            >
                                                <option value="yes">Yes</option>
                                                <option value="no">No</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Is this Filterable
                                        </label>
                                        <div class="input-group m-input-group">
                                            <select class="form-control m-bootstrap-select m_selectpicker"
                                                    name="is_filterable"
                                            >
                                                <option value="yes">Yes</option>
                                                <option value="no">No</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Is this Color Swatch
                                        </label>
                                        <div class="input-group m-input-group">
                                            <select class="form-control m-bootstrap-select m_selectpicker"
                                                    name="is_swatch"
                                            >
                                                <option value="no">No</option>
                                                <option value="yes">Yes</option>

                                            </select>

                                        </div>
                                    </div>



                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Attribute  Description
                                        </label>
                                        <div class="input-group m-input-group">

                                            <textarea name="description" id="mytextarea">Attribute Description</textarea>
                                        </div>
                                        <span class="m-form__help">
													Description
												</span>
                                    </div>




                                </div>






                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--end::Section-->

                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>
        </div>
    </div>

 @endsection



@section('admin_footer_script')
    <script src='{{asset('/production/js/admin/tinymce/tinymce.min.js')}}'></script>
    <script>
        tinymce.init({
            selector: '#mytextarea'
        });
    </script>
    <script>
        //== Class definition

        var BootstrapSelect = function () {

            //== Private functions
            var demos = function () {
                // minimum setup
                $('.m_selectpicker').selectpicker();
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        jQuery(document).ready(function() {
            BootstrapSelect.init();
        });



        $(".js-example-placeholder-single").select2({
            placeholder: "Select a yes or no",
            allowClear: true
        });
    </script>


    @endsection