@extends('admin.index');

@section('content')
    <link rel="stylesheet" href="{{asset('/js/vis/dist/vis.min.css') }}" />
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">


                            <div class="m-portlet m-portlet--mobile">
                                <div class="m-portlet__head">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <h3 class="m-portlet__head-text">
                                                Tree View
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="m-portlet__head-tools">

                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <!--begin: Search Form -->


                                    <div class="row">

                                    <div class="col-4">

                                        <h5>Primary Member</h5>

                                        <button class="btn btn-primary primary-member">

                                        </button>



                                    </div>
                                    <div  class="col-8">

                                        <h5>Down line Member</h5>
                                        <div id="downline"></div>





                                    </div>

                                    </div>







                        </div>
                        <!--end::Section-->

                    </div>


            </div>
        </div>
    </div>

 @endsection


@section('admin_footer_script')

    <style>
        .treeviewbutton{
            margin-top: 5px;
        }
    </style>

    <script>






     var downline = [];

     $.ajax({
         /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
         url:"{{route('admin.tree.view.by.id.admin')}}",
       //  async: true,
         dataType: 'json',

         success: function(obj){
            // var json = $.parseJSON(obj);

             $(".primary-member").replaceWith('<i style="font-size: 33px; color: #e12500!important; text-align: center; padding-left: 75px" class="fa fa-user"></i> <br> ' +
                 '<button  class="btn btn-danger primary-member">'+obj.customer.fname+'<br> Sponcer Id: '+obj.customer.sponser_id+'<br> IC NO: '+obj.customer.ic_number+'</button> ')



             $.each(obj.downline,function(k,v){
                 $("#downline").append('<i style="font-size: 33px; color: #5de178!important; text-align: center; padding-left: 125px" class="fa fa-user"></i> <br>' +
                     '<a href="/dashboard/admin/tree-view/view/'+v.id+'"  class="btn btn-success treeviewbutton">'+v.fname+ ' '+v.lname +
                     ' <br> IC No:  '+v.ic_number+
                     ' <br> Sponcer ID:  '+v.sponser_id+'</a> ' +
                     '<br>')
                 console.log(v.fname);
             });
         },
         error: function(error){
             alert(error);
         }
     })








    </script>









    @endsection