@extends('admin.index');

@section('content')
    <style>
        .select2-options {


        }
    </style>
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">



                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Create Return Invoice
                                </h3>
                            </div>
                        </div>
                    </div>






                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                          role="form" method="post"

                    >
                        {{ csrf_field() }}
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <div class="col-lg-3">

                                        <label for="exampleInputEmail1">
                                          Return  Invoice Number
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                           #
                                                         </span>
                                            </div>
                                            <input type="text"
                                                   class="form-control m-input--air invoice_number"
                                                   placeholder="Invoice Number"
                                                   name="invoice_number"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">


                                    </div>
                                </div>
                                <div class="col-lg-3">

                                        <label for="exampleInputEmail1">
                                            Invoice Date
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                          <i class="la la-calendar"></i>
                                                         </span>
                                            </div>
                                            <input type="text"
                                                   class="form-control m-input--air invoice_date"
                                                   placeholder="Invoice Date"
                                                   name="invoice_date"
                                                   value="{{ date('d-m-Y') }}"
                                                   id="m_datepicker_1" readonly
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">


                                    </div>
                                </div>
                                <div class="col-lg-3">

                                        <label for="exampleInputEmail1">
                                            Return Month
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                          <i class="la la-calendar"></i>
                                                         </span>
                                            </div>
                                            <input type="text"
                                                   id="m_datepicker_return_month"
                                                   class="form-control m-input--air invoice_return_month"
                                                   placeholder="Return Month"
                                                   value="{{ date('m-Y') }}"
                                                   name="invoice_return_month"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">


                                    </div>
                                </div>
                                <div class="col-lg-3">

                                        <label for="exampleInputEmail1">
                                            Return Quater
                                        </label>
                                        <div class="input-group m-input-group">

                                            <select name="invoice_return_quater" class="form-control invoice_return_quater" id="invoice_return_quater">
                                                <option value="APR-JUN 2018" >APR-JUN 2018</option>
                                                <option value="JAN-MAR 2018">JAN-MAR 2018</option>
                                                <option value="JUL-SEPT 2018">JUL-SEPT 2018</option>
                                                <option value="OCT-DEC 2018">OCT-DEC 2018</option>
                                                <option value="JAN-MAR 2019">JAN-MAR 2019</option>
                                                <option value="APR-JUN 2019">APR-JUN 2019</option>
                                                <option value="JUL-SEPT 2019">JUL-SEPT 2019</option>
                                                <option value="OCT-DEC 2019">OCT-DEC 2019</option>


                                            </select>


                                    </div>
                                </div>







                            </div>


                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <div class="col-lg-12">
                                        <label for="exampleInputEmail1">
                                            Customer Name
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                             <i class="la la-user"></i>
                                                         </span>
                                            </div>
                                            <input type="text"
                                                   id="customername"
                                                   name="customer_name"

                                                   class="form-control m-input--air inv_customer_name"
                                                   autocomplete="off"
                                            >

                                            <input type="hidden"
                                                   id="inv_customer_id"
                                                   name="inv_customer_id"

                                                   class="inv_customer_id"
                                                   autocomplete="off"
                                            >

                                            <input type="hidden"
                                                   id="user_commission"
                                                   name="user_commission"

                                                   class="form-control m-input--air user_commission"
                                                   autocomplete="off"
                                            >
                                    </div>

                                        <br>
                                        <div class="m-stack m-stack--ver m-stack--general" style="height: 100px">
                                            <div class="m-stack__item">
                                                <label for="exampleInputEmail1">
                                                    Customer GSTIN:
                                                </label>
                                                <input type="text"
                                                       class="form-control m-input--air customer-gstin"
                                                       placeholder="Customer GSTIN"
                                                       name="customer_gstin"
                                                       autocomplete="off"
                                                       aria-describedby="basic-addon1 inv-gstin">
                                            </div>
                                            <div class="m-stack__item" style="padding-left: 10px">
                                                <label for="exampleInputEmail1">
                                                    Place of Supply:
                                                </label>
                                                <select name="customer_place_of_supply" class="form-control inv-placeofsupply" id="">
                                                    <option selected>Select State</option>
                                                    <option value="35">Andaman and Nicobar</option>
                                                    <option value="37">Andhra Pradesh</option>
                                                    <option value="12">Arunachal Pradesh</option>
                                                    <option value="18">Assam</option>
                                                    <option value="10">Bihar</option>
                                                    <!-- <option value="Chandigarh">Chandigarh</option> -->
                                                    <option value="04">Chhattisgarh</option>
                                                    <option value="26">Dadra and Nagar Haveli</option>
                                                    <option value="25">Daman and Diu</option>
                                                    <option value="07">Delhi</option>
                                                    <!-- <option value="Foreign">Foreign</option> -->
                                                    <option value="30">Goa</option>
                                                    <option value="24">Gujarat</option>
                                                    <option value="06">Haryana</option>
                                                    <option value="02">Himachal Pradesh</option>
                                                    <option value="01">Jammu and Kashmir</option>
                                                    <option value="20">Jharkhand</option>
                                                    <option value="29">Karnataka</option>
                                                    <option value="32">Kerala</option>
                                                    <option value="31">Lakshadweep</option>
                                                    <option value="23">Madhya Pradesh</option>
                                                    <option value="27">Maharastra</option>
                                                    <option value="14">Manipur</option>
                                                    <option value="17">Meghalaya</option>
                                                    <option value="15">Mizoram</option>
                                                    <option value="13">Nagaland</option>
                                                    <option value="21">Orissa</option>
                                                    <option value="34">Puducherry</option>
                                                    <option value="03">Punjab</option>
                                                    <option value="08">Rajasthan</option>
                                                    <option value="11">Sikkim</option>
                                                    <option value="33">Tamil Nadu</option>
                                                    <option value="36">Telangana</option>
                                                    <option value="16">Tripura</option>
                                                    <option value="09">Uttar Pradesh</option>
                                                    <option value="05">Uttarakhand</option>
                                                    <option value="19">West Bengal</option>
                                                </select>
                                            </div>

                                        </div><div class="m-stack m-stack--ver m-stack--general" style="height: 100px">
                                            <div class="m-stack__item">
                                                <label for="exampleInputEmail1">
                                                    Remark:
                                                </label>
                                                <input type="text"
                                                       class="form-control m-input--air remark"
                                                       placeholder="Remark"
                                                       name="remark"
                                                       autocomplete="off"
                                                       aria-describedby="basic-addon1 inv-gstin">
                                            </div>


                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-4">
                                    <div >
                                        <label for="exampleInputEmail1">
                                            Billing Address
                                        </label>


                                        <div class="m-stack m-stack--ver m-stack--general" style="height: 50px">
                                            <div class="m-stack__item">
                                                <input type="text"
                                                       name="customer_billing_street"

                                                       placeholder="Street Address"

                                                       class="form-control m-input--air inv-street"
                                                       autocomplete="off"
                                                >
                                            </div>
                                            <div class="m-stack__item" style="padding-left: 10px">
                                                <input type="text"


                                                       name="customer_billing_locality"
                                                       placeholder="Locality"
                                                       class="form-control m-input--air inv-locality"
                                                       autocomplete="off"
                                                >
                                            </div>

                                        </div>
                                        <div class="m-stack m-stack--ver m-stack--general" style="height: 50px">
                                            <div class="m-stack__item">
                                                <input type="text"


                                                       placeholder="City / Vill"
                                                       class="form-control m-input--air inv-city"
                                                       autocomplete="off"
                                                >
                                            </div>
                                            <div class="m-stack__item" style="padding-left: 10px">
                                                <input type="text"


                                                       name="customer_billing_state"
                                                       placeholder="State"
                                                       class="form-control m-input--air inv-state"
                                                       autocomplete="off"
                                                >
                                            </div>

                                        </div>
                                        <div class="m-stack m-stack--ver m-stack--general" style="height: 50px">
                                            <div class="m-stack__item">
                                                <input type="text"

                                                       name="customer_billing_pincode"

                                                       placeholder="Pincode"
                                                       class="form-control m-input--air inv-pincode"
                                                       autocomplete="off"
                                                >
                                            </div>
                                            <div class="m-stack__item" style="padding-left: 10px">
                                                <input type="text"


                                                       name="customer_billing_country"
                                                       placeholder="Country"
                                                       class="form-control m-input--air inv-country"
                                                       autocomplete="off"
                                                >
                                            </div>

                                        </div>


                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div>

                                        <label for="exampleTextarea">
                                            Shipping Address
                                        </label>
                                        <div class="m-stack m-stack--ver m-stack--general" style="height: 50px">
                                            <div class="m-stack__item">
                                                <input type="text"

                                                       name="customer_shipping_street"
                                                       placeholder="Street Address"

                                                       class="form-control m-input--air inv-shipping-street"
                                                       autocomplete="off"
                                                >
                                            </div>
                                            <div class="m-stack__item" style="padding-left: 10px">
                                                <input type="text"


                                                       name="customer_shipping_locality"
                                                       placeholder="Locality"
                                                       class="form-control m-input--air inv-shipping-locality"
                                                       autocomplete="off"
                                                >
                                            </div>

                                        </div>
                                        <div class="m-stack m-stack--ver m-stack--general" style="height: 50px">
                                            <div class="m-stack__item">
                                                <input type="text"


                                                       name="customer_shipping_city"
                                                       placeholder="City / Vill"
                                                       class="form-control m-input--air inv-shipping-city"
                                                       autocomplete="off"
                                                >
                                            </div>
                                            <div class="m-stack__item" style="padding-left: 10px">
                                                <input type="text"


                                                       name="customer_shipping_state"
                                                       placeholder="State"
                                                       class="form-control m-input--air inv-shipping-state"
                                                       autocomplete="off"
                                                >
                                            </div>

                                        </div>
                                        <div class="m-stack m-stack--ver m-stack--general" style="height: 50px">
                                            <div class="m-stack__item">
                                                <input type="text"

                                                       name="customer_shipping_pincode"

                                                       placeholder="Pincode"
                                                       class="form-control m-input--air inv-shipping-pincode"
                                                       autocomplete="off"
                                                >
                                            </div>
                                            <div class="m-stack__item" style="padding-left: 10px">
                                                <input type="text"


                                                       name="customer_shipping_country"
                                                       placeholder="Country"
                                                       class="form-control m-input--air inv-shipping-country"
                                                       autocomplete="off"
                                                >
                                            </div>

                                        </div>
                                    </div>
                                </div>


                            </div>




                            <div class="form-group m-form__group row">
                                <div class="producthead">

                                                <div class="m-stack m-stack--ver m-stack--general  m-stack--demo">


                                                    <div class="m-stack__item m-stack__item--center" style="width: 200px;">
                                                        Product Name
                                                        <input type="text"
                                                               disabled
                                                               id="product-name"
                                                               class="form-control m-input product product-name"
                                                               name="product_name[]"
                                                               placeholder="Product Name">
                                                    </div>



                                                    <div class="m-stack__item">
                                                        Item Type

                                                        <select name="product_type[]" class="form-control m-input product-type">
                                                            <option>
                                                                Goods
                                                            </option>
                                                            <option>
                                                                Service
                                                            </option>
                                                        </select>
                                                    </div>




                                                    <div class="m-stack__item ">
                                                        HSN / SAC
                                                        <input type="text" readonly
                                                               name="product_hsn[]"
                                                               class="form-control m-input product-hsn"
                                                               placeholder="HSN / SAC">
                                                    </div>



                                                    <div class="m-stack__item ">
                                                        Qty <i class="la la-sort-numeric-asc"></i>
                                                        <div class="input-group m-input-group priupdate">

                                                            <input type="number"
                                                                   class="form-control m-input product-qty"
                                                                   placeholder="Qty"
                                                                   name="product_qty[]" value="1"
                                                                   aria-describedby="basic-addon1"

                                                            >
                                                        </div>
                                                    </div>


                                                    <div class="m-stack__item">
                                                        Unit
                                                        <div class="input-group m-input-group">
                                                            <input type="text"
                                                                   class="form-control m-input product-unit"
                                                                   readonly
                                                                   name="product_unit[]"
                                                                   placeholder="Unit" aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>


                                                    <div class="m-stack__item">
                                                        Rate
                                                        <div class="input-group m-input-group ">

                                                            <input type="text" class="form-control
                                                        m-input product-rate"
                                                                   placeholder="Rate"
                                                                   name="product_rate[]"
                                                                   id="itm_rate"
                                                                   readonly
                                                                   aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>




                                                    <div class="m-stack__item">
                                                        Disc %

                                                        <div class="input-group m-input-group">

                                                            <input type="number" name="product_discount_percentage[]"
                                                                   class="form-control m-input product-discount-percentage"
value="0"
                                                                   placeholder="" aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>



                                                </div>

                                                <div class="m-stack m-stack--ver m-stack--general  m-stack--demo">

                                                    <div class="m-stack__item">
                                                        Taxable Rate
                                                        <div class="input-group m-input-group ">

                                                            <input type="text" class="form-control
                                                        m-input product-taxable-rate"
                                                                   placeholder="Rate"
                                                                   name="product_taxable_rate[]"

                                                                   readonly
                                                                   aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>

                                                    <div class="m-stack__item">
                                                        Tax Rate
                                                        <div class="input-group m-input-group ">

                                                            <input type="text" class="form-control
                                                        m-input product_tax_rate"
                                                                   placeholder="Rate"
                                                                   name="product_tax_rate[]"

                                                                   readonly
                                                                   aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>





                                                    <div class="m-stack__item">
                                                       Afr Dsc R
                                                        <div class="input-group m-input-group ">

                                                            <input type="text" class="form-control
                                                        m-input product-discount-rate"
                                                                   placeholder="Rate"
                                                                   name="product_taxable_rate[]"

                                                                   readonly
                                                                   aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>















                                                    <div class="m-stack__item">
                                                        CGST %
                                                        <div class="input-group m-input-group">

                                                            <input type="text" class="form-control m-input product-cgst-percentage"
                                                                   placeholder="" readonly
                                                                   aria-describedby="basic-addon1">

                                                        </div>
                                                    </div>
                                                    <div class="m-stack__item">
                                                        SGST %
                                                        <div class="input-group m-input-group">

                                                            <input type="text" class="form-control m-input product-sgst-percentage"
                                                                   placeholder="" readonly
                                                                   aria-describedby="basic-addon1">

                                                        </div>
                                                    </div>
                                                    <div class="m-stack__item">
                                                        IGST %
                                                        <div class="input-group m-input-group">

                                                            <input type="text" class="form-control m-input product-igst-percentage"
                                                                   placeholder="" readonly
                                                                   aria-describedby="basic-addon1">

                                                        </div>
                                                    </div>

                                                    <input type="hidden" class="product-igst-amount" readonly>
                                                    <input type="hidden" class="product-cgst-amount" readonly>
                                                    <input type="hidden" class="product-sgst-amount" readonly>
                                                    <input type="hidden" class="product-discount-amount" readonly>
                                                    <input type="hidden" class="product-unit-discount-amount" readonly>
                                                    <input type="hidden" class="product-id" readonly>
                                                    <input type="hidden" class="color" readonly>
                                                    <input type="hidden" class="size" readonly>
                                                    <input type="hidden" class="parent_product_id" readonly>
                                                    <input type="hidden" class="stock" >

                                                    <div class="m-stack__item">
                                                        Disc Amt
                                                        <div class="input-group m-input-group">


                                                            <input type="text" class="form-control m-input product-discount-amount"
                                                                   id="itm_tax_val" readonly
                                                                   name="product_discount_amount[]"
                                                                   placeholder="Disc Amt" aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>


                                                    <div class="m-stack__item">
                                                        Tax. Amt
                                                        <div class="input-group m-input-group">

                                                            <input type="text" class="form-control
                                                            m-input product-tax-amount"
                                                                   name="product_taxable_amount[]"
                                                                   id="itm_tax_val" readonly=""
                                                                   placeholder="Tax. Amt" aria-describedby="basic-addon1">


                                                        </div>
                                                    </div>



                                                    <div class="m-stack__item">
                                                        Tax Val
                                                        <div class="input-group m-input-group ">

                                                            <input type="text" class="form-control
                                                        m-input product-tax-value"
                                                                   placeholder="Taxable Value"
                                                                   readonly
                                                                   name="product_taxable_value[]"
                                                                   id="item_total_taxable_rate"
                                                                   aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>


                                                    <div class="m-stack__item">
                                                        Total <i class="la la-inr"></i>
                                                        <div class="input-group m-input-group">

                                                            <input type="text" readonly
                                                                   class="form-control m-input product-total-amount"
                                                                   placeholder="Total"
                                                                   aria-describedby="basic-addon1"
                                                            >
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <br>
                                                    <div
                                                            class="btn btn-outline-success btn-block add-row">
														<span>
															<i class="la la-plus"></i>
															<span>
																Add New Product/ Item
															</span>
														</span>
                                                    </div>
                                                </div>

                                            </div>
                            </div>




                            <div class="form-group m-form__group row">

                                    <div class="m-section__content" style="width: 100%">
                                        <table class="table m-table m-table--head-bg-success" style="width: 100%"
                                               id="inv_table">
                                            <thead style="width: 100%">
                                            <tr>
                                                <th>
                                                    #
                                                </th>
                                                <th style="max-width: 100px;!important;width: 100px!important;">
                                                    Name
                                                </th>
                                                <th>
                                                    Type
                                                </th>
                                                <th>
                                                    HSN
                                                </th>
                                                <th>
                                                    Qty
                                                </th>
                                                <th>
                                                    Ut
                                                </th>
                                                <th>
                                                     Rate<i class="la la-inr"></i>
                                                </th>
                                                <th>
                                                    Dsc %
                                                </th>

                                                <th>
                                                    D Amt.
                                                </th>

                                                <th>
                                                    T Rate.
                                                </th>
                                                <th>
                                                    T Amt. <i class="la la-inr"></i>
                                                </th>

                                                <th>
                                                    T Val. <i class="la la-inr"></i>
                                                </th>
                                                <th>
                                                    CGST
                                                </th>
                                                <th>
                                                    SGST
                                                </th>
                                                <th>
                                                    IGST
                                                </th>
                                                <th>
                                                    TOT<i class="la la-inr"></i>
                                                </th>


                                            </tr>
                                            </thead>

                                            <tbody>

                                            {{--<tr>
                                                <td>fff</td>
                                                <td>fff</td>
                                                <td>fff</td>
                                                <td>fff</td>
                                                <td>fff</td>
                                                <td>fff</td>
                                                <td>fff</td>
                                                <td>fff</td>
                                                <td>fff</td>
                                                <td>fff</td>
                                                <td>fff</td>
                                                <td>fff</td>
                                                <td>fff</td>
                                                <td>fff</td>
                                                <td>fff</td>
                                                <td>fff</td>
                                                <td>fff</td>
                                            </tr>--}}

                                            </tbody>
                                        </table>
                                    </div>
                            </div>





                            <div class="m-section__content">
                                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                    <div class="m-demo__preview">
                                        <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                                            <div class="m-stack__item">

                                                <div class="m-widget1__item">
                                                    <div class="row m-row--no-padding align-items-center">
                                                        <div class="col">
                                                            <h5 class="m-widget1__title">
                                                                Total
                                                            </h5>
                                                            <span class="m-widget1__desc">
															Total Invoice Value
                                                        <div class="democlass">

                                                        </div>
														</span>
                                                        </div>
                                                        <div class="col m--align-right">
														<span class="m-widget1__number m--font-brand">
															<h3><i class="la la-rupee"></i>
                                                            <spam class="invoice_total_value">
                                                                0
                                                            </spam>
                                                            </h3>
														</span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="m-stack__item">
                                                <div class="m-widget1__item">
                                                    <div class="row m-row--no-padding align-items-center">
                                                        <div class="col">
                                                            <h5 class="m-widget1__title">
                                                                Taxable
                                                            </h5>
                                                            <span class="m-widget1__desc">
															Total Taxable Value
														</span>

                                                        </div>
                                                        <div class="col m--align-right">
														<span class="m-widget1__number m--font-danger">
															<h3><i class="la la-rupee"></i>
                                                            <spam class="invoice_tax_value">
                                                                0
                                                            </spam>
                                                            </h3>
														</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                <div class="m-widget1__item">
                                                    <div class="row m-row--no-padding align-items-center">
                                                        <div class="col">
                                                            <h5 class="m-widget1__title">
                                                                Total Taxable
                                                            </h5>
                                                            <span class="m-widget1__desc">
															Total Taxable
														</span>
                                                        </div>
                                                        <div class="col m--align-right">
														<span class="m-widget1__number m--font-success">
															<h3><i class="la la-rupee"></i>
                                                            <spam class="invoice_total_taxable_value">
                                                                0
                                                            </spam>
                                                            </h3>
														</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                <div class="m-widget1__item">
                                                    <div class="row m-row--no-padding align-items-center">
                                                        <div class="col">
                                                            <h5 class="m-widget1__title">
                                                                SGST / SGST
                                                            </h5>
                                                            <span class="m-widget1__desc">
															Total SGST Value
														</span>
                                                        </div>
                                                        <div class="col m--align-right">
														<span class="m-widget1__number m--font-success">
															<h3><i class="la la-rupee"></i>
                                                            <spam class="invoice_sgst_value">
                                                                0
                                                            </spam>
                                                            </h3>
														</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="m-stack__item">
                                                <div class="m-widget1__item">
                                                    <div class="row m-row--no-padding align-items-center">
                                                        <div class="col">
                                                            <h5 class="m-widget1__title">
                                                                Total Discount
                                                            </h5>
                                                            <span class="m-widget1__desc">
															Total Discount Value
														</span>
                                                        </div>
                                                        <div class="col m--align-right">
														<span class="m-widget1__number m--font-success">
															<h3><i class="la la-rupee"></i>
                                                            <spam class="invoice_disc_value">
                                                                0
                                                            </spam>
                                                            </h3>
														</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>






                        </div>





                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">

                                    <div class="col-lg-9">
                                        <a id="invsubmit" class=" btn btn-primary" href="">Submit Invoice</a>
                                    </div>

                                    <div class="col-lg-3">

                                        <button id="inv_reset" class="btn btn-secondary inv_reset">
                                            Reset
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->





            </div>
        </div>
    </div>

@endsection

@section('admin_footer_script')


{{--jq ui--}}
<style>
    input.ui-autocomplete-loading {
        background: white url("{{asset('loader/ajaxloader.gif')}}") right center no-repeat;
        background-size: 32px 32px;

    }

    .item_name{
        max-width: 200px;!important;
        width: 200px;!important;
    }


</style>


<link rel="stylesheet" href="{{ asset('/plugin/ui-autocomplete/jquery-ui.min.css')}}"/>
<link rel="stylesheet" href="{{ asset('/plugin/ui-autocomplete/jquery-ui.theme.min.css')}}"/>
<script src="{{ asset('/plugin/ui-autocomplete/jquery-ui.min.js')}}" type="text/javascript"></script>

{{--auto serch--}}
<script>
    $(document).ready(function () {

        $('.inv_reset').click(function() {
            location.reload();
        });

        $("#inv_reset").click(function(){
            location.reload();
        });

        var src = "{{ route('api.get.customer.apisearch') }}";
        $("#customername").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: src,
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    success: function (data) {
                        response(data);

                    }


                });
            },
            minLength: 1,
            select: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.value);
                $('.inv_person_name').val(ui.item.fname);
                $('.inv_customer_id').val(ui.item.id);
                $('.inv-street').val(ui.item.street);
                $('.inv-locality').val(ui.item.locality);
                $('.inv-city').val(ui.item.city);
                $('.inv-state').val(ui.item.state);
                $('.inv-country').val(ui.item.country);
                $('.inv-pincode').val(ui.item.pin);

                $('.inv-shipping-street').val(ui.item.street);
                $('.inv-shipping-locality').val(ui.item.locality);
                $('.inv-shipping-city').val(ui.item.city);
                $('.inv-shipping-state').val(ui.item.state);
                $('.inv-shipping-country').val(ui.item.country);
                $('.inv-shipping-pincode').val(ui.item.pin);
                $('.customer-gstin').val(ui.item.gstin);
                $('.inv-placeofsupply').val(ui.item.gstin_state);
                $('.user_commission').val(ui.item.commission);

                $("#product-name-flexdatalist").prop('disabled', false);







            }

        });
    });
</script>



<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-flexdatalist/2.1.3/jquery.flexdatalist.min.css" rel="stylesheet" type="text/css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-flexdatalist/2.1.3/jquery.flexdatalist.min.js"></script>

<script>
    $(document).ready(function () {

        var src = "{{ route('product.apisearch') }}";
        $("#product-name").flexdatalist({
            url: src,
            minLength: 1,
            selectionRequired: true,
            visibleProperties: ["value", "color", "size"],
            searchIn: ["value", "color", "code_name"]
        })

            .on("select:flexdatalist", function (event, data) {
                event.preventDefault();
                $(this).val(data.value);
                $('.product-type').val(data.type);
                $('.product-hsn').val(data.hsn);
               // $('.product-qty').val(data.item.qty);
                $('.product-unit').val(data.unit);
                $('.product-rate').val(data.mrp_price);
                $('.product-discount-rate').val(data.mrp_price);
                $('.product-taxable-rate').val(data.mrp_price);

                $('.product-discount-percentage').val('0');

                $('.product-tax-amount').val(data.total_tax);
                $('.product_tax_rate').val(data.igst_percentage);
                $('.product-tax-value').val(data.mrp_price);
                $('.product-cgst-percentage').val(data.cgst_percentage);
                $('.product-sgst-percentage').val(data.sgst_percentage);
                $('.product-igst-percentage').val(data.igst_percentage);
                $('.product-total-amount').val(data.listing_price);


                $('.product-igst-amount').val(data.igst);
                $('.product-cgst-amount').val(data.cgst);
                $('.product-sgst-amount').val(data.sgst);
                $('.product-id').val(data.id);
                $('.color').val(data.color);
                $('.size').val(data.size);
                $('.parent_product_id').val(data.parent_product_id);
                $('.stock').val(data.stock);



            })

     //   });


        //price sync

        $(".product-qty").on("change paste keyup keydown keypress", (function () {
            discountUpdate()
            onQtyUpodate()


        }));

        $(".product-discount-percentage").on("change paste keyup keydown keypress", (function () {
          //  onQtyUpodate()
            discountUpdate()





        }));

        function onQtyUpodate() {
            var QUALTITY = $(".product-qty").inputVal();
            var TAXABLE_RATE = $(".product-taxable-rate").inputVal();
            var IGST = $(".product-igst-percentage").inputVal();
            var DISCOUNT_RATE = $(".product-discount-rate").inputVal();
            var TAX_RATE = $(".product_tax_rate").inputVal();
            var CALCULATE_TOTAL_TAX =  (TAXABLE_RATE*QUALTITY);
            var TAX_PERCENTAGE_TO_MULTIPLE = (IGST/100)+1;
            var CALCULATE_TOTAL =  (DISCOUNT_RATE*QUALTITY*TAX_PERCENTAGE_TO_MULTIPLE).toFixed(2);


            var PRODUCT_TAX_UPDATE = CALCULATE_TOTAL_TAX.toFixed(2);
            var PRODUCT_TOTAL_AMOUNT = CALCULATE_TOTAL;
            var PRODUCT_TAX_AMOUNT = (DISCOUNT_RATE*QUALTITY*(IGST/100)).toFixed(2);
            var PRODUCT_CGST= (PRODUCT_TAX_AMOUNT/2).toFixed(2);

            $(".product-tax-value").inputVal(PRODUCT_TAX_UPDATE);
            $(".product-total-amount").inputVal(PRODUCT_TOTAL_AMOUNT);
            $(".product-tax-amount").inputVal(PRODUCT_TAX_AMOUNT);
            $(".product-sgst-amount").inputVal(PRODUCT_CGST);
            $(".product-cgst-amount").inputVal(PRODUCT_CGST);

        }


        function discountUpdate() {
            var DISCOUNT_PER = $(".product-discount-percentage").inputVal();
            var QUALTITY = $(".product-qty").inputVal();
            var IGST = $(".product-igst-percentage").inputVal();
            var RATE = $(".product-rate").inputVal();


            var CALCU_DISCOUNT_AMOUNT = (Number(RATE) * Number(DISCOUNT_PER/100));
            var PER_ITEM_DISC = (Number(RATE) - CALCU_DISCOUNT_AMOUNT).toFixed(2);


            var TOTAL_DISCOUNT = (QUALTITY* RATE * (DISCOUNT_PER/100));
            var TOTAL_UNIT_DISCOUNT = (RATE * (DISCOUNT_PER/100));

           // var TAX_AMT = ((IGST/100)*TOTAL ).toFixed(2);
           // var TAXABLE_VAL = (TOTAL - TAX_AMT).toFixed(2);

            $(".product-taxable-rate").inputVal(PER_ITEM_DISC);
            $(".product-discount-rate").inputVal(PER_ITEM_DISC);


            $(".product-discount-amount").inputVal(TOTAL_DISCOUNT);
            $(".product-unit-discount-amount").inputVal(TOTAL_UNIT_DISCOUNT);




            $(".product-tax-value").inputVal((PER_ITEM_DISC*QUALTITY).toFixed(2))

            $(".product-tax-amount").inputVal((PER_ITEM_DISC*QUALTITY*(IGST/100)).toFixed(2))

            $(".product-total-amount").inputVal((PER_ITEM_DISC*QUALTITY*((IGST/100)+1)).toFixed(2));

            $(".product-sgst-amount").inputVal((PER_ITEM_DISC*QUALTITY*(IGST/100)/2).toFixed(2));
            $(".product-cgst-amount").inputVal((PER_ITEM_DISC*QUALTITY*(IGST/100)/2).toFixed(2));





        }




    });
</script>
{{--auto search--}}


{{--copy products--}}
<script>
    $(document).ready(function () {

        // copy customer details to shipping


        var productname = $(".product-name");
        var productid = $(".product-id");







        $(".add-row").click(function (e) {
            e.preventDefault();






            var rowCount = $('#inv_table tr').length;




            var QTY = $(".product-qty").inputVal();
            var STOCK = $(".stock").inputVal();





            if(Number(QTY) > Number(STOCK)){
             var CONTENT = "You  have <strong>" + Number(STOCK) + "</strong> Unit in stock";
                swal("Sorry", CONTENT, "error");
            } else {


                if ($('.product-name').val()) {
                    var $tableBody = $('#inv_table').find("tbody"),
                        //$trLast = $tableBody.find("tr:last"),

                        $trNew = $tableBody.append(
                            "<tr class='productstab'>" +
                            // "<td >" + rowCount  + "</td>" +
                            "<td class='item_sno'>"  + rowCount  + "</td>" +
                            "<td class='item_name'> " +   productname.inputVal()+' ' +$(".size").inputVal() + ': '+ $(".color").inputVal()+ "</td>" +
                            "<td class='item_type'>"  + $(".product-type").val() +  "</td>" +
                            "<td class='item_hsn'>" + $(".product-hsn").inputVal() + "</td>" +
                            "<td class='item_qty'>"  + $(".product-qty").inputVal()  + "</td>" +
                            "<td class='item_unit'>" +   $(".product-unit").inputVal()  + "</td>" +
                            "<td class='taxable_rate'>" + +$(".product-rate").inputVal() + "</td>" +
                            "<td class='item_discount-percentage'>"   + $(".product-discount-percentage").inputVal() + "</td>" +
                            "<td class='item_discount-amount'>" + $(".product-unit-discount-amount").inputVal() + "</td>" +
                            "<td class='item_taxval'>" + $(".product-taxable-rate").inputVal() + "</td>" +


                            "<td class='item_taxper'>"  + $(".product-tax-amount").inputVal() +  "</td>" +
                            "<td class='item_cgst'>"  + $(".product-tax-value").inputVal() +
                            "</td>" +
                            "<td class='item_cgst'>"+  $(".product-cgst-amount").inputVal()  +
                            "</td>" +
                            "<td class='item_sgst'>" + $(".product-sgst-amount").inputVal() +
                            " </td>" +
                            "<td class='item_igst'>" +   $(".product-igst-amount").inputVal()+
                            "</td>" +

                            "<td class='item_prototal'>" + $(".product-total-amount").inputVal() + "</td>" +

                            "<td  style='display: none'> "+ $(".product-id").inputVal() +" </td>" +//16
                            "<td  style='display: none'> "+ $(".product-cgst-percentage").inputVal() +" </td>" +//16
                            "<td  style='display: none'> "+$(".product-sgst-percentage").inputVal() +" </td>" +//16
                            "<td  style='display: none'> "+$(".product-igst-percentage").inputVal()+" </td>" +//16
                            "<td  style='display: none'> "+$(".color").inputVal()+" </td>" +//16
                            "<td  style='display: none'> "+$(".size").inputVal()+" </td>" +//16
                            "<td  style='display: none'> "+$(".parent_product_id").inputVal()+" </td>" +//16
                            "<td  style='display: none'> "+$(".product-discount-amount").inputVal()+" </td>" //16
                        );



                    var INV_TOTAL = ( Number($(".product-total-amount").inputVal()) +
                        Number($(".invoice_total_value").text()))
                    $('.invoice_total_value').replaceWith('<spam class="invoice_total_value">' + INV_TOTAL.toFixed(2) + '</spam>');

                    //total tax update



                    var INV_TOTAL_TAX = ( Number($(".product-tax-amount").inputVal()) +
                        Number($(".invoice_tax_value").text()))

                    $('.invoice_tax_value').replaceWith('<spam class="invoice_tax_value">' + INV_TOTAL_TAX.toFixed(2) + '</spam>');


                    //total cgst update



                    var INV_TOTAL_CGST = ( Number($(".product-tax-value").inputVal()) +
                        Number($(".invoice_total_taxable_value").text()))

                    $('.invoice_total_taxable_value').replaceWith('<spam class="invoice_total_taxable_value">' + INV_TOTAL_CGST.toFixed(2) + '</spam>');


                    //total sgst update
                    var INV_TOTAL_SGST = ( Number($(".product-sgst-amount").inputVal()) +
                        Number($(".invoice_sgst_value").text()))
                    $('.invoice_sgst_value').replaceWith('<spam class="invoice_sgst_value">' + INV_TOTAL_SGST.toFixed(2) + '</spam>');

                    //total igst update


                    var INV_TOTAL_IGST = ( Number($(".product-discount-amount").inputVal())
                        + Number($(".invoice_disc_value").text()))

                    $('.invoice_disc_value').replaceWith('<spam class="invoice_disc_value">' +
                        INV_TOTAL_IGST.toFixed(2) + '</spam>');






                    $('.sno').val(rowCount);
                    $tableBody.after($trNew);

                    $(this).closest('.producthead').find("input[type=text], textarea").val("");
                    $(".product-qty").inputVal('1');
                    $(".product-discount-percentage").inputVal('0');
                    $('.prototal').val('0');

                } else {
                    swal("OOPS!", "Please Search for Product and  try to add!", "error");

                }


            }









        });








        $(".deleteaddress").click(function (e) {

            $(this).closest('.nameform').find("input[type=text], textarea").val("");
            $('.billreplace').replaceWith(
                "<div class='billreplace'>" +
                "</div>"
            );


        });


    });


</script>

{{--copy products end--}}

{{--jq uiend--}}


    <script>
        //== Class definition

        var BootstrapDatepicker = function () {

            //== Private functions
            var demos = function () {
                // minimum setup
                $('#m_datepicker_1, #m_datepicker_1_validate').datepicker({
                    format: "dd-mm-yyyy",
                    todayHighlight: true,
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    }
                });

                $('#m_datepicker_return_month').datepicker({
                    format: "mm-yyyy",
                    viewMode: "months",
                    minViewMode: "months",
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    }
                });

                // minimum setup for modal demo
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        jQuery(document).ready(function() {
            BootstrapDatepicker.init();
        });
    </script>

<script>
    $('#invsubmit').click( function(e) {
        e.preventDefault();

        var CUSTOMER_ID = $(".inv_customer_id").val();
        var INV_TTL = $(".invoice_total_value").text()

        if(CUSTOMER_ID < 1){

            swal("OOPS!", "Please Add Customer", "error");
        }

        else if(Number(INV_TTL) < 1){
            swal("Invoice Value is Zero", "Please Add Products", "error");
        }

        else{




        var rows = [];
        $('#inv_table tbody tr').each(function(i, n){


            var $row = $(n);


            /*var calcu_item_tax_cgst =
                ($row.find('td:eq(6)').text() * ( Number($row.find('td:eq(10)').text() / 100) * $row.find('td:eq(4)').text() )).toFixed(2);

            var calcu_item_tax_sgst = ($row.find('td:eq(6)').text() * ( Number($row.find('td:eq(11)').text() / 100) * $row.find('td:eq(4)').text() )).toFixed(2);
            var calcu_item_tax_igst = ($row.find('td:eq(6)').text() * ( Number($row.find('td:eq(12)').text() / 100) * $row.find('td:eq(4)').text() )).toFixed(2);
            var item_tax_value_calcu = (Number($row.find('td:eq(6)').text()) * Number($row.find('td:eq(4)').text())).toFixed(2);
*/




            rows.push({
                product_id: $row.find('td:eq(16)').text(),
                sno: $row.find('td:eq(0)').text(),
                name: $row.find('td:eq(1)').text(),
                type: $row.find('td:eq(2)').text(),
                hsn: $row.find('td:eq(3)').text(),
                quantity: $row.find('td:eq(4)').text(),
                unit: $row.find('td:eq(5)').text(),

                rate: $row.find('td:eq(6)').text(),
              //  taxable_rate: $row.find('td:eq(7)').text(),
                discount_percentage: $row.find('td:eq(7)').text(),
                discount_amount: $row.find('td:eq(8)').text(),

                taxable_rate: $row.find('td:eq(9)').text(),
                taxable_amount: $row.find('td:eq(10)').text(),


                tax_value: $row.find('td:eq(11)').text(),
                cgst: $row.find('td:eq(12)').text(),
                sgst: $row.find('td:eq(13)').text(),
                igst: $row.find('td:eq(14)').text(),
                sgst_percentage: $row.find('td:eq(18)').text(),
                cgst_percentage: $row.find('td:eq(17)').text(),
                igst_percentage: $row.find('td:eq(19)').text(),
                color: $row.find('td:eq(20)').text(),
                size: $row.find('td:eq(21)').text(),
                parent_product_id: $row.find('td:eq(22)').text(),
                product_total_disc: $row.find('td:eq(23)').text(),
                total: $row.find('td:eq(15)').text()
            });
        });


        var _token = $("input[name='_token']").val();
        var invoice_number = $(".invoice_number").val();
        var remark = $(".remark").val();
        var invoice_date = $(".invoice_date").val();
        var invoice_return_month = $(".invoice_return_month").val();
        //var invoice_retun_quater = $("input[name='invoice_retun_quater']").val();
      //  var inv_due_date = $("input[name='inv_due_date']").val();
        var invoice_customer_name = $(".inv_customer_name").val();
        var invoice_customer_id = $(".inv_customer_id").val();
        var invoice_customer_gstin = $(".customer-gstin").val();
        var invoice_place_of_supply = $(".inv-placeofsupply").val();
        var invoice_return_quater = $(".invoice_return_quater").val();
        var invoice_billing_address_street = $(".inv-street").val();
        var invoice_billing_address_locality = $(".inv-locality").val();
        var invoice_billing_address_city = $(".inv-city").val();
        var invoice_billing_address_state = $(".inv-state").val();
        var invoice_billing_address_country = $(".inv-country").val();
        var invoice_billing_address_pin = $(".inv-pincode").val();
        var invoice_shipping_address_street = $(".inv-shipping-street").val();
        var invoice_shipping_address_locality = $(".inv-shipping-locality").val();
        var invoice_shipping_address_city = $(".inv-shipping-city").val();
        var invoice_shipping_address_country = $(".inv-shipping-country").val();
        var invoice_shipping_address_pin = $(".inv-shipping-pincode").val();
        var invoice_total_taxable_amount = Number($(".invoice_total_taxable_value").text())
        var invoice_total_tax_cgst =  Number($(".invoice_sgst_value").text());
        var invoice_total_tax_sgst = Number($(".invoice_sgst_value").text())
        var invoice_total_tax_igst = Number($(".invoice_tax_value").text());
        var invoice_total_tax = Number($(".invoice_tax_value").text());
        var invoice_total_taxable_amt = Number($(".invoice_total_taxable_value").text());
        var invoice_total_discount_amt = Number($(".invoice_disc_value").text());
        var invoice_total = Number($(".invoice_total_value").text())







        var d = {

            _token:_token,
            invoice_number:invoice_number,
            invoice_date: invoice_date,
            remark: remark,
            invoice_return_month:invoice_return_month,
            invoice_customer_name:invoice_customer_name,
            customer_id:invoice_customer_id,
            invoice_customer_gstin:invoice_customer_gstin,
            invoice_place_of_supply: invoice_place_of_supply,
            invoice_return_quater:invoice_return_quater,
            invoice_billing_address_street:invoice_billing_address_street,
            invoice_billing_address_locality:invoice_billing_address_locality,
            invoice_billing_address_city: invoice_billing_address_city,
            invoice_billing_address_state: invoice_billing_address_state,
            invoice_billing_address_country: invoice_billing_address_country,
            invoice_billing_address_pin: invoice_billing_address_pin,
            invoice_shipping_address_street: invoice_shipping_address_street,
            invoice_shipping_address_locality: invoice_shipping_address_locality,
            invoice_shipping_address_city: invoice_shipping_address_city,
            invoice_shipping_address_country: invoice_shipping_address_country,
            invoice_shipping_address_pin: invoice_shipping_address_pin,
            invoice_total_taxable_amount: invoice_total_taxable_amount,
            invoice_total_tax_cgst: invoice_total_tax_cgst,
            invoice_total_tax_sgst: invoice_total_tax_sgst,
            invoice_total_tax_igst: invoice_total_tax_igst,
            invoice_total_tax: invoice_total_tax,
            invoice_total_taxable_amt: invoice_total_taxable_amt,
            invoice_total_discount_amt: invoice_total_discount_amt,
            invoice_total: invoice_total,


            //invoice items
            products: rows

        };


       // console.log(d);
        $.ajax({
            url: "{{route('dashboard.returninvoice.manage.save')}}",
            type:'POST',
            data: d,
            success: function (response) {
                var saved_inv_id = response.id;

                console.log(response)

                if (response.data === 'success') {
                    window.location = "/dashboard/returninvoice/view/" + saved_inv_id;
                } else {
                    alert('failed');
                }
            }
        });
        }

    });


</script>






@endsection