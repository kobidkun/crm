@extends('admin.index');

@section('content')
    <link rel="stylesheet" type="text/css" href="{{asset('/css/print/print.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/print/invoicedetails.css')}}">
    <style>
        .select2-options {


        }
    </style>
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">



                <div class="m-portlet">






                    <style>
                        thead{display: table-header-group;}
                        tfoot {display: table-row-group;}
                        tr {page-break-inside: avoid;}
                        .container{
                            font-family: 'Montserrat', sans-serif;
                        }
                    </style>
                    <style>
                        .product .img-responsive {
                            margin: 0 auto;
                        }
                    </style>


                    <div class="container">
                        <div class="row">


                            <div class="col-xs-12" style="text-align: center">

                                <h2><srrong>Emporium Marketing Pvt.Ltd</srrong></h2>
                                <p> 2 1/2 Mile,Prakash Nagar, <br>
                                    Limbu Busty,(Kaviyatri Parijat Path) <br>P.O. Salugara P.S. Bhakti Nagar <br>Dist :
                                    Jalpaiguri Siliguri 734008, West Bengal</p>
                                <P>GSTIN: <strong>19AADCE7821P1Z2</strong>  	&nbsp;	&nbsp;   PAN: <strong>AADCE7821P</strong>   </P>

                                <p>
                                    <span>Phone: <i class="fas fa-rupee-sign"></i>  +91 9593488168</span> &nbsp; &nbsp;
                                    <span>Email: <i class="fas fa-rupee-sign"></i>  info@emporiummkt.com</span>
                                </p>

                            </div>









                            <div class="col-xs-12">
                                <div style="border-top: 2px solid #000000;"></div>

                                <div class="col-xs-12">
                                    <h3>Return Invoice: <span style="font-size: 18px;font-weight: 700">EMPL/R/2018-19-{{$invoice->invoice_number_generated}}</span></h3>
                                </div>




                                <div class="col-xs-4">
                                    <address>
                                        Return  Invoice No: <strong>{{$invoice->invoice_number_generated}}</strong> <br>
                                        Return Invoice Date: <strong>{{$invoice->invoice_date}} </strong> <br>
                                        Return  Invoice Reference: <strong>{{$invoice->id}} </strong> <br>
                                        Return  Return Month:<strong>{{$invoice->invoice_return_month}} </strong> <br>
                                        Return  Return Quater: <strong>{{$invoice->invoice_return_quater}} </strong> <br>
                                    </address>

                                    <address>
                                        Customer Name: <strong>{{$invoice->invoice_customer_name}}</strong> <br>
                                        GSTIN: <strong>{{$invoice->invoice_customer_gstin}}</strong> <br>
                                        Place of Supply: <strong>{{$invoice->invoice_place_of_supply}}</strong> <br>
                                    </address>
                                    Remark: {{$invoice->remark}}
                                </div>
                                <div class="col-xs-4">
                                    <address>
                                        <strong>Billed To:</strong><br>
                                        {{$invoice->invoice_billing_address_street}}  {{$invoice->invoice_billing_address_locality}}<br>
                                        {{$invoice->invoice_billing_address_city}}  {{$invoice->invoice_billing_address_state}}<br>
                                        {{$invoice->invoice_billing_address_country}} <br>
                                        {{$invoice->invoice_billing_address_pin}}
                                    </address>
                                    <address style="width: 250px">
                                        <strong>Delived at:</strong><br>
                                        {{$invoice->invoice_shipping_address_street}}  {{$invoice->invoice_shipping_address_locality}}
                                        <br>
                                    </address>
                                </div>
                                <div class="col-xs-4" >
                                    <figure class="image is-96x96" style="display: block;margin-left: auto;margin-right: auto;">
                                        {!! QrCode::size(150)->generate($invoice->invoice_secure_id); !!}
                                    </figure>
                                </div>



                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">


                                <div class="panel-body">

                                    <style>
                                        thead{
                                            background-color: #0a8cf0;
                                            color: #ffffff;
                                        }
                                        tbody tr td{
                                            background-color: #ffffff;
                                        }
                                    </style>

                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                        <tr>
                                            <td><strong>S No</strong></td>
                                            <td class="text-center"><strong>Name</strong></td>
                                            <td class="text-center"><strong>Type</strong></td>
                                            <td class="text-center"><strong>HSN</strong></td>
                                            <td class="text-center"><strong>QTY</strong></td>
                                            <td class="text-center"><strong>Unit</strong></td>
                                            <td class="text-center"><strong>Rate &#8377;</strong></td>
                                            <td class="text-center"><strong>Dsc %</strong></td>
                                            <td class="text-center"><strong>Dsc Amt &#8377;</strong></td>
                                            <td class="text-center"><strong>Tax. Rate &#8377;</strong></td>
                                            <td class="text-center"><strong>Tax. Amt &#8377;</strong></td>
                                            <td class="text-center"><strong>Tax. Val &#8377;</strong></td>
                                            <td class="text-center"><strong>CGST</strong></td>
                                            <td class="text-center"><strong>SGST</strong></td>

                                            <td class="text-center"><strong>Total &#8377;</strong></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                        @foreach ($invoice->invoice_to_products as $key => $product)
                                            <tr>

                                                <td class="text-center"><i class="fas fa-rupee-sign"></i>     {{$product->sno}}</td>
                                                <td class="text-center" style="min-width: 100px;">
                                                    {{$product->name}}</td>
                                                <td class="text-center">
                                                    {{$product->type}}</td>
                                                <td class="text-center">
                                                    {{$product->hsn}}</td>
                                                <td class="text-center"> {{$product->quantity}}</td>
                                                <td class="text-center">
                                                    {{$product->unit}}</td>
                                                <td class="text-center"> {{$product->rate}}</td>
                                                <td class="text-center"> {{$product->discount_percentage}}</td>
                                                <td class="text-center"> {{$product->product_total_disc}}</td>


                                                <td class="text-center"> {{$product->taxable_rate}}</td>



                                                <td class="text-center"> {{$product->taxable_amount}}</td>

                                                <td class="text-center"> {{$product->tax_value}}</td>

                                                <td class="text-center"> <i class="fas fa-rupee-sign"></i> {{$product->cgst}}
                                                    <hr>{{$product->cgst_percentage}}%
                                                </td>

                                                <td class="text-center">  {{$product->sgst}}
                                                    <hr>{{$product->sgst_percentage}}%
                                                </td>


                                                <td class="text-center">  {{$product->total}}</td>

                                            </tr>
                                        @endforeach
                                        <tr>

                                            <td class="text-center" style="background-color: #ffffff; color: #ffffff; border:none;"></td>
                                            <td class="text-center" style="background-color: #ffffff; color: #ffffff; border:none;"></td>
                                            <td class="text-center" style="background-color: #ffffff; color: #ffffff; border:none;"></td>
                                            <td class="text-center" style="background-color: #ffffff; color: #ffffff; border:none;"></td>
                                            <td class="text-center" style="background-color: #ffffff; color: #ffffff; border:none;"></td>
                                            <td class="text-center" style="background-color: #ffffff; color: #ffffff; border:none;"></td>
                                            <td class="text-center" style="background-color: #ffffff; color: #000000;">
                                                <strong>Total</strong></td>
                                            <td class="text-center" style="background-color: #0a8cf0; color: #ffffff;">
                                            </td>
                                            <td class="text-center" style="background-color: #0a8cf0; color: #ffffff; ">
                                                {{$invoice->invoice_total_discount_amt}}</td>
                                            <td class="text-center" style="background-color: #0a8cf0; color: #ffffff;">
                                            </td>
                                            <td class="text-center" style="background-color: #0a8cf0; color: #ffffff;">

                                                {{$invoice->invoice_total_tax}}

                                            </td>
                                            <td class="text-center" style="background-color: #0a8cf0; color: #ffffff;">

                                                {{$invoice->invoice_total_tax_value}}
                                            </td>

                                            <td class="text-center" style="background-color: #0a8cf0; color: #ffffff;">
                                                {{$invoice->invoice_total_tax_sgst}}</td>

                                            <td class="text-center" style="background-color: #0a8cf0; color: #ffffff;">
                                                {{$invoice->invoice_total_tax_cgst}}</td>

                                            <td class="text-center" style="background-color: #0a8cf0; color: #ffffff;">
                                                {{$invoice->invoice_total}}</td>

                                        </tr>
                                        </tbody>
                                    </table>



                                </div>













                            </div>
                        </div>
                    </div>

                    <!--end::Form-->
                </div>
                <!--end::Portlet-->







            </div>
        </div>
    </div>

























    <div class="m-content">
        <div class="row">
            <div class="col-md-12">



                <div class="m-portlet">

                    <div class="container">
                        <div class="row">


                            <div class="panel-body">
                                <div class="col-lg-4">
                                    <a class="btn btn-success"
                                       href="{{route('dashboard.returninvoice.download.pdf',$invoice->id)}}">Download PDF</a>

                                </div>

                                <div class="col-lg-4">

   {{--              <a class="btn btn-info" href="{{route('dashboard.invoice.print.pdf',$invoice->id)}}" target="_blank">Print Invoice</a>--}}



            <button class="btn btn-info" onclick="printJS({printable:'/dashboard/returninvoice/download/{{$invoice->id}}/print', type:'pdf', showModal:true})">Print Invoice</button>



                                </div>

                                <div class="col-lg-4">
                                    <a class="btn btn-primary" href="">Export Invoice</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('admin_footer_script')

    <script src="{{asset('/js/print/print.min.js')}}"></script>

@endsection