@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">


            <div class="col-xl-3 col-lg-3">
                <div class="m-portlet">
                    <div class="m-portlet__body" style="height:auto;">
                        <div class="m-card-profile">
                            <div class="m-card-profile__details">
												<span class="m-card-profile__name">
													{{$product->name}}
												</span>

                            </div>
                            <div class="m-card-profile__pic">
                                <div class="m-card-profile__pic-wrapper">
                                    @if( !empty($product->product_primary_images))
                                    <img src="{{asset($product->product_primary_images->cdn_url)}}" alt=""/>
                                        @endif
                                </div>
                            </div>

                            <div class="m-card-profile__pic">
                                <div class="m-card-profile__pic-wrapper">

                                    {!! QrCode::size(150)->generate($product->slug); !!}

                                </div>
                            </div>

                            <div class="m-portlet__body-separator"></div>


                        </div>


                        <div class="m-widget4 m-widget4--chart-bottom" style="min-height: 350px">
                            <div class="m-widget4__item">
                                <div class="m-widget4__ext">
                                    <a href="#" class="m-widget4__icon m--font-brand">
                                        <i class="flaticon-confetti"></i>
                                    </a>
                                </div>
                                <div class="m-widget4__info">
													<span class="m-widget4__text">
														Product MRP <i class="la la-rupee"></i>
													</span>
                                </div>
                                <div class="m-widget4__ext">
													<span class="m-widget4__number m--font-accent">
													{{$product->mrp_price}}
													</span>
                                </div>
                            </div>
                            <div class="m-widget4__item">
                                <div class="m-widget4__ext">
                                    <a href="#" class="m-widget4__icon m--font-brand">
                                        <i class="flaticon-exclamation-1"></i>
                                    </a>
                                </div>
                                <div class="m-widget4__info">
													<span class="m-widget4__text">
														Product Discount Amount <i class="la la-rupee"></i>
													</span>
                                </div>
                                <div class="m-widget4__ext">
													<span class="m-widget4__stats m--font-info">
														<span class="m-widget4__number m--font-accent">
														{{$product->disc_amount}}
														</span>
													</span>
                                </div>
                            </div>
                            <div class="m-widget4__item">
                                <div class="m-widget4__ext">
                                    <a href="#" class="m-widget4__icon m--font-brand">
                                        <i class="flaticon-coins"></i>
                                    </a>
                                </div>
                                <div class="m-widget4__info">
													<span class="m-widget4__text">
														Product Listing Price <i class="la la-rupee"></i>
													</span>
                                </div>
                                <div class="m-widget4__ext">
													<span class="m-widget4__stats m--font-info">
														<span class="m-widget4__number m--font-accent">
															{{$product->listing_price}}
														</span>
													</span>
                                </div>
                            </div>
                            <div class="m-widget4__item">
                                <div class="m-widget4__ext">
                                    <a href="#" class="m-widget4__icon m--font-brand">
                                        <i class="flaticon-open-box"></i>
                                    </a>
                                </div>
                                <div class="m-widget4__info">
													<span class="m-widget4__text">
														Product In Stock (PC)
													</span>
                                </div>
                                <div class="m-widget4__ext">
													<span class="m-widget4__stats m--font-info">
														<span class="m-widget4__number m--font-accent">
															{{$product->product_to_varients()->sum('stock')}}
														</span>
													</span>
                                </div>
                            </div>

                            <div class="m-widget4__item">
                                <div class="m-widget4__ext">
                                    <a href="#" class="m-widget4__icon m--font-brand">
                                        <i class="flaticon-line-graph"></i>
                                    </a>
                                </div>
                                <div class="m-widget4__info">
													<span class="m-widget4__text">
														Last Product Sale (PC)
													</span>
                                </div>
                                <div class="m-widget4__ext">
													<span class="m-widget4__stats m--font-info">
														<span class="m-widget4__number m--font-accent">
															{{$productSale}}
														</span>
													</span>
                                </div>
                            </div>

                            <br>

                            <div >




                                                        @foreach($product->product_to_varients as $var)
														<span class="m-widget4__number m--font-accent"
                                                              style="width: 200px!im;">



															{{$var->color}}-{{$var->size}} ({{$var->stock}})

                                                            <hr>

														</span>


                                                            @endforeach


                            </div>




                        </div>
                    </div>
                </div>
            </div>


            <div class="col-xl-9 col-lg-9">

                <style>
                    .tabbedmentitems{
                        align-content: center;
                        text-align: center;
                    }
                </style>
                <div class="m-portlet m-portlet--tabs">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--danger m-tabs-line--2x m-tabs-line--right"
                                role="tablist">


                                <style>

                                    .product-view .add-to-links a {
                                        display:none;!important;
                                    }
                                </style>


                                {{--<li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link " data-toggle="tab" href="#product_stock" role="tab">
                                        <i class="flaticon-grid-menu-v2 " aria-hidden="true"></i>
                                        Stock
                                    </a>
                                </li>
--}}
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link " data-toggle="tab" href="#product_varent" role="tab">
                                        <i class="flaticon-line-graph" aria-hidden="true"></i>
                                        Product Variant
                                    </a>
                                </li>

                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#report_tab" role="tab">
                                        <i class="flaticon-graph" aria-hidden="true"></i>
                                        Report
                                    </a>
                                </li>

                                <li class="nav-item dropdown m-tabs__item">
                                    <a class="nav-link m-tabs__link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">
                                        <i class="fa fa-cogs"></i>
                                        More
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="nav-link m-tabs__link tabbedmentitems" data-toggle="tab" href="#profileedit" role="tab">

                                           Edit Details
                                        </a>
                                        <a class="nav-link m-tabs__link tabbedmentitems" data-toggle="tab" href="#m_tabs_6_2" role="tab">

                                           Edit Category
                                        </a>
                                        <a class="nav-link m-tabs__link tabbedmentitems" data-toggle="tab" href="#m_tabs_6_1"
                                           role="tab">

                                            Images
                                        </a>
                                        <div class="dropdown-divider tabbedmentitems"></div>
                                        <a class="dropdown-item" data-toggle="tab" href="#m_tabs_10_3">
                                            Separated link
                                        </a>
                                    </div>
                                </li>


                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane " id="m_tabs_6_1" role="tabpanel">


                                <div class="m-section">
                                    <h3 class="m-section__heading">
                                        Primary Product Image
                                    </h3>



                                    {{--pri img--}}


                                    <div class="m-section__content">
                                        <div class="m-demo" data-code-preview="true" data-code-html="true"
                                             data-code-js="false">
                                            <br>
                                            @if( !empty($product->product_primary_images))
                                            <form method="POST" action="{{ '/admin/product/image/primaryimage/'.$product->product_primary_images->id }}"
                                                  class="m-dropzone dropzone m-dropzone--primary"
                                                  style="height: 450px; width: auto"
                                                  id="mDropzoneTwoPrimary"
                                            >

                                                {{ csrf_field() }}
                                                <input name="id" value="{{$product->id}}" type="hidden">
                                                <div class="m-dropzone__msg dz-message needsclick">











                                                    <img src="{{asset($product->product_primary_images->cdn_url)}}"


                                                         style="display: block;
                                                 margin-left: auto;
                                                 margin-right: auto;
                                                 height: auto;
                                                 width:250px;"
                                                         alt="">
                                                    <br>
                                                    <h3 class="m-dropzone__msg-title">
                                                        Please click the image to replace with new image you cannot delete Primary Image

                                                    </h3>
                                                </div>
                                            </form>

                                                @else
                                                <form method="POST" action="{{ route('admin.product.primaryuploadimages') }}"
                                                      class="m-dropzone dropzone m-dropzone--primary"
                                                      id="mDropzoneTwoPrimaryImage"
                                                >

                                                    {{ csrf_field() }}
                                                    <input name="id" value="{{$product->id}}" type="hidden">
                                                    <div class="m-dropzone__msg dz-message needsclick">
                                                        <h3 class="m-dropzone__msg-title">
                                                            Drop Primary Image here or click to upload.
                                                        </h3>
                                                        <span class="m-dropzone__msg-desc">
														Primary Image
													</span>
                                                    </div>
                                                </form>

                                            @endif

                                        </div>
                                    </div>


                                    {{--sec img--}}


                                    <div class="m-section__content">
                                        <div class="m-demo" data-code-preview="true" data-code-html="true"
                                             data-code-js="false">


                                            @foreach($product->product_images as $image)
                                                <div class="m-demo" data-code-preview="true" data-code-html="true"
                                                     data-code-js="false">
                                                    <div class="m-demo__preview">
                                                        <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                                                            <div class="m-stack__item" style="width: 250px;">
                                                                <img src="{{asset($image->images_url)}}"
                                                                     style="display: block;
    margin-left: auto;
    margin-right: auto;
    width: 220px;"
                                                                >
                                                            </div>
                                                            <div class="m-stack__item m-stack__item--center m-stack__item--middle">
                                                                <div style="text-align: center">

                                                                    <a href="#"
                                                                       class="btn btn-success m-btn btn-lg 	m-btn m-btn--icon m-btn--pill">
															<span>
																<i class="fa flaticon-folder-4"></i>
																<span>
																	Replace Image
																</span>
															</span>
                                                                    </a>

                                                                    <br>
                                                                    <br>
                                                                    <br>


                                                                    <a href="{{route('admin.product.secondary.image.delete.save',[$image->id])}}"
                                                                       class="btn m-btn--pill m-btn--air  btn-danger"
                                                                    >

                                                                        <i class="la la-trash"></i> Delete Image

                                                                    </a>


                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            @endforeach




                                                <form method="POST" action="{{ route('admin.product.uploadimages') }}"
                                                      class="m-dropzone dropzone m-dropzone--primary"
                                                      id="m-dropzone-two"
                                                >

                                                    {{ csrf_field() }}
                                                    <input name="id" value="{{$product->id}}" type="hidden">
                                                    <div class="m-dropzone__msg dz-message needsclick">
                                                        <h3 class="m-dropzone__msg-title">
                                                            Drop new Secondary Images here or click to upload and refresh the page
                                                        </h3>
                                                        <span class="m-dropzone__msg-desc">
														Upload up to 10 files
													</span>
                                                    </div>
                                                </form>

                                        </div>
                                    </div>


                                </div>
                            </div>


                            <div class="tab-pane" id="profileedit" role="tabpanel">

                                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                                      role="form" method="post"
                                      action="/admin/product/edit/save/{{$product->id}}"
                                >
                                    <input name="_method" type="hidden" value="PUT">
                                    {{ csrf_field() }}
                                    <div class="m-portlet__body">
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-12">
                                                <label for="exampleInputEmail1">
                                                    Product Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
                 <span class="input-group-text">
                     <i class="la la-barcode"></i>
                 </span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Product Name"
                                                           value="{{$product->name}}"
                                                           name="name"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                            </div>


                                        </div>




                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Product HSN
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                             <i class="la la-barcode"></i>
                                                         </span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Product HSN"
                                                           name="hsn"
                                                           value="{{$product->hsn}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                            </div>


                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Product Code
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                             <i class="la la-barcode"></i>
                                                         </span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Product Code"
                                                           name="code_name"
                                                           value="{{$product->code_name}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                            </div>


                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Product Type
                                                </label>
                                                <div class="input-group m-input-group">

                                                    <select

                                                            class="form-control"
                                                            name="type"
                                                    >
                                                        <option value="{{$product->type}}">{{$product->type}}</option>
                                                        <option value="Goods">Goods</option>
                                                        <option value="Service">Service</option>

                                                    </select>
                                                </div>
                                            </div>


                                        </div>








                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Product Unit
                                                </label>
                                                <div class="input-group m-input-group">

                                                    <select

                                                            class="form-control"
                                                            name="unit"


                                                    >
                                                        <option value="{{$product->unit}}" selected>{{$product->unit}}</option>
                                                        <option value="PC">PC</option>
                                                        <option value="KG">KG</option>
                                                        <option value="Unit">Unit</option>
                                                        <option value="Meter">Meter</option>
                                                        <option value="Gram">Gram</option>
                                                        <option value="Decimal">Decimal</option>

                                                    </select>
                                                </div>
                                            </div>


                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Maximum Retail Price
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                             <i class="la la-rupee"></i>
                                                         </span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--ai mrp"
                                                           placeholder="Price"
                                                           name="mrp_price"
                                                           value="{{$product->mrp_price}}"
                                                           required>
                                                </div>
                                            </div>


                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    IGST / TAX %
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                             %
                                                         </span>
                                                    </div>
                                                    <select class="form-control m-input--ai igst-percentage"

                                                            name="igst_percentage" id="">
                                                        <option value="{{$product->igst_percentage}}" selected>{{$product->igst_percentage}}%</option>
                                                        <option value="0">0%</option>
                                                        <option value="5">5%</option>
                                                        <option value="12">12%</option>
                                                        <option value="18">18%</option>
                                                        <option value="28">28%</option>
                                                    </select>
                                                </div>
                                            </div>


                                        </div>













                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    CGST / SGST %
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                            %
                                                         </span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air cgst-percentage"
                                                           value="{{$product->cgst_percentage}}"
                                                           placeholder="CGST / SGST"
                                                           readonly
                                                           name="cgst_percentage">
                                                </div>
                                            </div>


                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    CGST / SGST Value
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                             <i class="la la-rupee"></i>
                                                         </span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air cgst-amount"
                                                           placeholder="CGST / SGST"
                                                           name="cgst"
                                                           readonly
                                                           value="{{$product->cgst}}"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                            </div>


                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Taxable Rate
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                            <i class="la la-rupee"></i>
                                                         </span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air taxable-rate"
                                                           id="discount-percentage"
                                                           placeholder="Auto Calculated"
                                                           name="taxable_rate"
                                                           value="{{$product->taxable_rate}}"

                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
                                        Auto Calculated
                                                 </span>
                                            </div>


                                        </div>













                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Total  Tax Amount
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
                                                          <span class="input-group-text">
                                                             <i class="la la-rupee"></i>
                                                         </span>
                                                    </div>
                                                    <input type="text"
                                                           readonly
                                                           value="{{$product->total_tax}}"
                                                           class="form-control m-input--air tax-amount"
                                                           placeholder="Total Tax Amount"
                                                           name="total_tax"
                                                    >
                                                </div>
                                            </div>


                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Listing Price (TOTAL)
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
                                                          <span class="input-group-text">
                                                             <i class="la la-rupee"></i>
                                                         </span>
                                                    </div>
                                                    <input type="text"
                                                           readonly
                                                           class="form-control m-input--air total"
                                                           id="listing-price"
                                                           value="{{$product->listing_price}}"
                                                           placeholder="Discounted Price"
                                                           name="listing_price"

                                                    >
                                                </div>
                                            </div>


                                            <div class="col-lg-4">

                                            </div>


                                        </div>






























                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-12">
                                                <label for="exampleInputEmail1">
                                                    Short Description
                                                </label>
                                                <div class="input-group m-input-group">

     <textarea class="form-control m-input"
               name="s_desc"
               id="mytextarea"
               rows="6">
         {!!   $product->s_desc!!}
     </textarea>
                                                </div>
                                                <span class="m-form__help">
             Short Description
         </span>
                                            </div>

                                            <div class="col-lg-12">

                                                <label for="exampleInputEmail1">
                                                    Long Description
                                                </label>
                                                <div class="input-group m-input-group">

     <textarea class="form-control m-input"
               name="l_desc"
               id="mytextarea2" rows="6">

          {!!   $product->l_desc!!}
     </textarea>
                                                </div>
                                                <span class="m-form__help">
             Long Description
         </span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                        <div class="m-form__actions m-form__actions--solid">
                                            <div class="row">
                                                <div class="col-lg-4"></div>
                                                <div class="col-lg-8">
                                                    <button type="submit" class="btn btn-primary">
                                                        Next
                                                    </button>
                                                    <button type="reset" class="btn btn-secondary">
                                                        Reset
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>


                            </div>


                            <div class="tab-pane" id="m_tabs_6_2" role="tabpanel">



                                <div class="m-section">
                                    <div class="m-section__content">
                                        <table class="table m-table m-table--head-bg-success">
                                            <thead>
                                            <tr>

                                                <th>
                                                    Attribute
                                                </th>
                                                <th>
                                                    Attribute Value
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($product->product_to_atributes as $attr)

                                            <tr>
                                                <th scope="row">
                                                   {{$attr->name}}
                                                </th>
                                                <td>
                                                    {{$attr->value}}
                                                </td>
                                            </tr>

                                                @endforeach




                                            </tbody>
                                        </table>
                                    </div>
                                </div>






                                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                                      role="form" method="post"
                                      action="{{ route('admin.attribute.product.store') }}"
                                >
                                    {{ csrf_field() }}
                                    <div class="m-portlet__body">

                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">

                                            </div>



                                            <select name="" id="" class=" form-control attribute-select">


                                                @foreach($atts as $att)
                                                    <option value="{{$att->default_value}}">{{$att->name}}</option>
                                                @endforeach

                                            </select>




                                        </div>

                                        <div class="col-lg-12">


                                            <button class="btn btn-block btn-primary add-attribute">Add Seceded Attribute</button>



                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row attribute-items ">




                                    </div>





















                                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                        <div class="m-form__actions m-form__actions--solid">
                                            <div class="row">
                                                <div class="col-lg-4"></div>
                                                <div class="col-lg-8">
                                                    <button type="submit" class="btn btn-primary">
                                                        Next
                                                    </button>
                                                    <button type="reset" class="btn btn-secondary">
                                                        Reset
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>


                            <div class="tab-pane " id="product_varent" role="tabpanel">




                                <div class="m-section">
                                    <div class="m-section__content">

                                        <div class="m-stack m-stack--ver m-stack--general m-stack--demo"
                                             style="background-color: #ffffff">
                                            <div class="m-stack__item" style="background-color: #ffffff;border: none">
                                            <h5>All Variant </h5>
                                        </div>

                                            <div class="m-stack__item" style="background-color: #ffffff; border: none">
                                            <button type="button" class="btn m-btn--pill m-btn--air         btn-primary btn-sm"
                                                    data-toggle="modal" data-target="#m_modal_4"
                                            >
                                                 New Variant
                                            </button>


                                            </div>

                                        </div>

                                        <div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">
                                                            Add new Product Variant for {{$product->name}}
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="{{route('admin.product.varient.save')}}" method="post"
                                                        >
                                                            {{ csrf_field() }}

                                                            <input type="hidden"  name="product_id" value="{{$product->id}}">
                                                            <input type="hidden"  name="name" value="{{$product->name}}">
                                                            <input type="hidden"  name="unit" value="{{$product->unit}}">
                                                            <input type="hidden"  name="hsn" value="{{$product->hsn}}">
                                                            <input type="hidden"  name="type" value="{{$product->type}}">
                                                            <input type="hidden"  name="catagory_id" value="{{$product->catagory_id}}">
                                                            <input type="hidden"  name="catagory2_id" value="{{$product->catagory2_id}}">
                                                            <input type="hidden"  name="catagory3_id" value="{{$product->catagory3_id}}">
                                                            <input type="hidden"  name="mrp_price" value="{{$product->mrp_price}}">
                                                            <input type="hidden"  name="igst_percentage" value="{{$product->igst_percentage}}">
                                                            <input type="hidden"  name="cgst_percentage" value="{{$product->cgst_percentage}}">
                                                            <input type="hidden"  name="cgst" value="{{$product->cgst}}">
                                                            <input type="hidden"  name="taxable_rate" value="{{$product->taxable_rate}}">
                                                            <input type="hidden"  name="total_tax" value="{{$product->total_tax}}">
                                                            <input type="hidden"  name="listing_price" value="{{$product->listing_price}}">



                                                            <div class="form-group">

                                                                <input type="text"
                                                                       name="code_name"
                                                                       class="form-control" placeholder="Product Code">
                                                            </div>


                                                            <div class="form-group">

                                                                <input type="hidden" class="form-control"
                                                                       name="stock"
                                                                       value="0"
                                                                       placeholder="Initial Stock">
                                                            </div>


                                                            <div class="form-group">
                                                                <select class="form-control"
                                                                        name="color"
                                                                >

                                                                    @foreach($colors as $color)
                                                                        <option value="{{$color->value}}">{{$color->value}}</option>

@endforeach
                                                                </select>

                                                            </div>


                                                            <div class="form-group">
                                                                <select


                                                                        class="form-control  value"
                                                                        name="size"
                                                                        >


                                                                    @foreach($sizes as $size)
                                                                        <option value="{{$size->value}}">{{$size->value}}</option>

                                                                    @endforeach



                                                                </select>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                                    Close
                                                                </button>
                                                                <button type="submit" class="btn btn-primary">
                                                                    Add new Variant
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>




                                        <table class="table table-bordered" id="users-table">
                                            <thead>
                                            <tr>
                                                <th>In Stock</th>
                                                <th>Color</th>
                                                <th>Size</th>
                                                <th>Action</th>

                                            </tr>
                                            </thead>
                                        </table>
                                </div>
                                </div>



                            </div>


                            <div class="tab-pane " id="product_stock" role="tabpanel">





                                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                                      role="form" method="post"
                                      action="{{route('admin.stock.product.store')}}"
                                >
                                    {{ csrf_field() }}
                                    <div class="m-portlet__body">



                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="exampleInputEmail1">
                                                    Quantity
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
                 <span class="input-group-text">
                     <i class="la la-circle"></i>
                 </span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--ai mrp"
                                                           placeholder="Quantity"
                                                           name="stock_number"
                                                           value=""
                                                           required>

                                                    <input type="hidden"
                                                           class="form-control m-input--ai mrp"

                                                           name="product_id"
                                                           value="{{$product->id}}"
                                                           required>


                                                </div>
                                            </div>


                                            <div class="col-lg-6">
                                                <label for="exampleInputEmail1">
                                                    Description
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
                 <span class="input-group-text">
                     <i class="la la-bar-chart"></i>
                 </span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air discount-amount"
                                                           placeholder="Description"

                                                           name="description">

                                                </div>


                                            </div>


                                        </div>



                                    </div>


                                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                        <div class="m-form__actions m-form__actions--solid">
                                            <div class="row">
                                                <div class="col-lg-4"></div>
                                                <div class="col-lg-8">
                                                    <button type="submit" class="btn btn-primary">
                                                        Add to Stock
                                                    </button>
                                                    <button type="reset" class="btn btn-secondary">
                                                        Reset
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>


                                <br>















                            </div>
                            <div class="tab-pane active" id="report_tab" role="tabpanel">





                                <table class="table table-bordered" id="report-table">
                                    <thead>
                                    <tr>
                                        <th>Product Name</th>
                                        <th>Qty</th>
                                        <th>Color</th>
                                        <th>Size</th>
                                        <th>Customer</th>
                                        <th>Total</th>
                                        <th>Date</th>

                                    </tr>
                                    </thead>
                                </table>















                            </div>
                        </div>
                    </div>
                </div>


            </div>


        </div>
    </div>



@endsection




@section('admin_footer_script')


    <script>
        //== Class definition

        var DropzoneDemo = function () {
            //== Private functions
            var demos = function () {
                // single file upload
                // multiple file upload
                Dropzone.options.mDropzoneTwoPrimaryImage = {
                    paramName: 'image',
                    method: 'POST',
                    maxFilesize: 1, // MB
                    maxFiles: 1,
                    acceptedFiles: ".jpeg,.jpg,.png,.gif"

                };
                Dropzone.options.mDropzoneTwoPrimary  = {
                    paramName: 'image',
                    method: 'POST',
                    maxFilesize: 1, // MB
                    maxFiles: 1,
                    acceptedFiles: ".jpeg,.jpg,.png,.gif",
                    init: function () {
                        this.on("maxfilesexceeded", function (file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        })

                    }
                };
                    Dropzone.options.mDropzoneTwo = {
                        paramName: 'image',
                        method: 'POST',
                        maxFilesize: 5, // MB
                        maxFiles: 20,
                        acceptedFiles: ".jpeg,.jpg,.png,.gif"

                    };


            };

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        DropzoneDemo.init();
    </script>


    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src='{{asset('/production/js/admin/tinymce/tinymce.min.js')}}'></script>
    <script>
        tinymce.init({
            selector: '#mytextarea',

        });

        tinymce.init({
            selector: '#mytextarea2',

        });
    </script>
    <script>
        //== Class definition

        var BootstrapSelect = function () {

            //== Private functions
            var demos = function () {
                // minimum setup
                $('.m_selectpicker').selectpicker();
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        jQuery(document).ready(function() {
            BootstrapSelect.init();
        });



        $(".js-example-placeholder-single").select2({
            placeholder: "Select a Secondary Category",
            allowClear: true
        });
    </script>

    <script>

        $( ".m_selectpicker" )
            .change(


                function () {
                    // preventDefault();
                    var abr = '<option value="">==select Primary categories==</option>';
                    $("#secondarycatplaceholder").html(abr);


                    var str = $( "#primarycatplaceholder option:selected" ).val();


                    // console.log(str)
                    $.ajax({
                        type: "GET",
                        url: "/admin/category/sec/searchforproducy/"+str,

                        success: function( resp) {
                            //   resp.preventDefault();
                            // $("#msg").prepend(resp);
                            console.log(resp.cat2.length);
                            //  var str = '<option value="0">Select Secondary Category</option>';
                            if (resp.cat2.length > 0 ) {
                                $.each(resp.cat2, function (index, value) {
                                    str = str+'<option  value="'+value.id+'">'+value.name+'</option>';
                                });
                                $("#secondarycatplaceholder").html(str);
                            } else {
                                //  var abr = '';
                                $("#secondarycatplaceholder").html('<option value="0">No Secondary categories</option>');
                            }

                            //turtiary

                            var abr2 = '<option value="">==select Secondary categories==</option>';
                            $("#turtiaryycatplaceholder").html(abr2);
                            var str2 = $( "#secondarycatplaceholder option:selected" ).val();
                            $.ajax({
                                type: "GET",
                                url: "/admin/category/tur/searchforproducy/"+str2,
                                success: function( resp2) {
                                    console.log(resp2.cat3.length);
                                    if (resp2.cat3.length > 0 ) {
                                        $.each(resp2.cat3, function (index, value) {
                                            str2 = str2+'<option  value="'+value.id+'">'+value.name+'</option>';
                                        });
                                        $("#turtiaryycatplaceholder").html(str2);
                                    } else {
                                        $("#turtiaryycatplaceholder").html('<option value="0">No Tertiary categories</option>');
                                    }
                                }
                            });



                        }
                    });




                    // console.log(str)
                }



            )
            .change();




        //turtiary



        $( ".seccatph" )
            .change(
                function () {
                    var abr2 = '<option value="">==select Secondary categories==</option>';
                    $("#turtiaryycatplaceholder").html(abr2);
                    var str2 = $( "#secondarycatplaceholder option:selected" ).val();
                    $.ajax({
                        type: "GET",
                        url: "/admin/category/tur/searchforproducy/"+str2,
                        success: function( resp2) {
                            console.log(resp2.cat3.length);
                            if (resp2.cat3.length > 0 ) {
                                $.each(resp2.cat3, function (index, value) {
                                    str2 = str2+'<option  value="'+value.id+'">'+value.name+'</option>';
                                });
                                $("#turtiaryycatplaceholder").html(str2);
                            } else {
                                $("#turtiaryycatplaceholder").html('<option value="0">No Tertiary categories</option>');
                            }
                        }
                    });
                }).change();
    </script>

    <script>
        $(document).ready(function() {







            $(".mrp").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                triggertaxupdate()
            }));



            $(".igst-percentage").on("change paste keyup keydown keypress", (function () {
                triggertaxupdate()
            }));



            $(".total").on("change paste keyup keydown keypress", (function () {
                triggetotalupdate()
            }));





            function triggetotalupdate() {


                var MRP = Number($(".total").inputVal());
                var IGST = Number($(".igst-percentage").val());
                var  TAXPERCENTAGE = (Number(IGST/100)).toFixed(2);

                var TOTALTAX = Number(MRP*TAXPERCENTAGE).toFixed(2);
                var TOTAL = (Number(MRP) - Number(TOTALTAX)).toFixed(2);


                $(".cgst-percentage").inputVal(IGST/2);
                $(".cgst-amount").inputVal(TOTALTAX/2);
                $(".tax-amount").inputVal(TOTALTAX);
                $(".taxable-rate").inputVal(MRP);
                $(".mrp").inputVal(TOTAL);
                // $(".total").inputVal(TOTAL);





                // console.log(IGST);

            }


            function triggertaxupdate() {


                var MRP = Number($(".mrp").inputVal());
                var IGST = Number($(".igst-percentage").val());
                var  TAXPERCENTAGE = (Number(IGST/100)).toFixed(2);

                var TOTALTAX = Number(MRP*TAXPERCENTAGE).toFixed(2);
                var TOTAL = (Number(MRP) + Number(TOTALTAX)).toFixed(2);


                $(".cgst-percentage").inputVal(IGST/2);
                $(".cgst-amount").inputVal(TOTALTAX/2);
                $(".tax-amount").inputVal(TOTALTAX);
                $(".taxable-rate").inputVal(MRP);
                $(".total").inputVal(TOTAL);





                // console.log(IGST);

            }



        });
    </script>

    <script>
        $(document).ready(function() {

            $(".add-attribute").click(function (event) {
                event.preventDefault()



                var ATTributeproductname = $('.attribute-select :selected').text();
                var ATTributeproductvalue =  $('.attribute-select').val();



                $(".attribute-items").append('' +
                    '<div class="col-lg-4" style="padding-top: 5px">' +
                    '<h2 style="margin-top: 25px">'+ATTributeproductname+'</h2>'+
                    '<input type="hidden"' +
                    'value="'+ATTributeproductname+'"' +
                    'name="attribute_name[]">' +
                    '<input type="hidden"' +
                    'value="{{$product->id}}"' +
                    'name="product_id[]">' +
                    '</div>' +

                    '<div class="col-lg-8">' +
                    '<label for="">Attribute Value</label>'+
                    '<input type="text"' +
                    'class="form-control m-input--air"' +
                    'placeholder="Name"' +
                    'value="'+ATTributeproductvalue+'"' +
                    'name="attribute_value[]">' +
                    '</div>' +

                    '</div>' +
                    '')




            });



        });

    </script>


    <script>
        $( ".selectvarient" )
            .change(
                function () {

                    var str2 = $( ".selectvarient" ).val();
                    console.log(str2)
                    $.ajax({
                        type: "GET",
                        url: "/admin/variant/findoption/"+str2,
                        success: function( resp2) {
                            console.log(resp2.length);
                            if (resp2.length > 0 ) {
                                $.each(resp2, function (index, value) {
                                    str2 = str2+'<option  value="'+value.value+'">'+value.value+'</option>';
                                });
                                $("#value").html(str2);
                            } else {
                                $("#value").html('<option value="0">No Vatiant Available</option>');
                            }
                        }
                    });
                }).change();
    </script>


    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script>
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('admin.product.varient.datatable',$product->id)}}',
                columns: [
                    { data: 'stock', name: 'stock' },
                    { data: 'color', name: 'color' },
                    { data: 'size', name: 'size' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });

            $('#report-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('dashboard.invoice.product.datatable',$product->id)}}',
                columns: [
                    { data: 'name', name: 'name' },
                   // { data: 'name', name: 'name' },
                    { data: 'quantity', name: 'quantity' },
                    { data: 'color', name: 'color' },
                    { data: 'size', name: 'size' },
                    { data: 'customer', name: 'customer'},
                    { data: 'total', name: 'total' },
                    {data: 'created_at', name: 'created_at', orderable: false, searchable: false}
                ]
            });
        });
    </script>




@endsection