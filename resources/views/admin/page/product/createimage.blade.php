@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Product Details
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">








                                <div class="m-portlet__body">

                                    <div class="form-group m-input-group--air">
                                        <div class="m-section__content">
                                            <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                                <div class="m-demo__preview">
                                                    <h3>
                                                        {{$product->name}}

                                                    </h3>

                                                    <h3>
                                                      Listing Price: <i class="fa fa-rupee"></i>  {{$product->listing_price}}
                                                        <small class="text-muted">
                                                            <br>
                                                        Discount Amount: <i class="fa fa-rupee"></i>   {{$product->disc_amount}}
                                                            <br>

                                                            Discount %: {{$product->discount_per}}%
                                                            <br>
                                                        </small>
                                                    </h3>
                                                    <br>
                                                    <h3>
                                                      Primary Category:  {{$product->products_to_categories->name}}
                                                        <small class="text-muted">
                                                            <br>
                                                            Secondary Category:    {{$product->products_to_categories2->name}}
                                                        </small>

                                                        @if($product->catagory3_id !== null)

                                                        <small class="text-muted">
                                                            <br>
                                                            Tertiary Category:   {{$product->products_to_categories3->name}}
                                                        </small>

                                                            @endif
                                                    </h3>


                                                    <div style="height: auto">
                                                        {!! $product->s_desc !!}
                                                    </div>


                                                    <div style="height: auto">
                                                        {!! $product->s_desc !!}
                                                    </div>







                                                </div>
                                            </div>
                                        </div>

                                    </div>




                                </div>

                        </div>
                        <!--end::Section-->

                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Upload Images
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">





                                <div class="m-portlet__body">

                                    <div class="form-group m-input-group--air">


                                        <form method="POST" action="{{ route('admin.product.primaryuploadimages') }}"
                                              class="m-dropzone dropzone m-dropzone--primary"
                                              id="mDropzoneTwoPrimary"
                                        >

                                            {{ csrf_field() }}
                                            <input name="id" value="{{$product->id}}" type="hidden">
                                            <div class="m-dropzone__msg dz-message needsclick">
                                                <h3 class="m-dropzone__msg-title">
                                                    Drop Primary Image here or click to upload.
                                                </h3>
                                                <span class="m-dropzone__msg-desc">
														Primary Image
													</span>
                                            </div>
                                        </form>


                                        <form method="POST" action="{{ route('admin.product.uploadimages') }}"
                                              class="m-dropzone dropzone m-dropzone--primary"
                                              id="m-dropzone-two"
                                        >

                                            {{ csrf_field() }}
                                            <input name="id" value="{{$product->id}}" type="hidden">
                                            <div class="m-dropzone__msg dz-message needsclick">
                                                <h3 class="m-dropzone__msg-title">
                                                    Drop Secondary Images here or click to upload.
                                                </h3>
                                                <span class="m-dropzone__msg-desc">
														Upload up to 10 files
													</span>
                                            </div>
                                        </form>


                                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                            <div class="m-form__actions m-form__actions--solid">

                                                <form method="POST" action="{{ route('admin.product.add.attributes') }}"

                                                >

                                                    {{ csrf_field() }}
                                                    <input name="id" value="{{$product->id}}" type="hidden">

                                                <div class="row">
                                                    <div class="col-lg-4"></div>
                                                    <div class="col-lg-8">
                                                        <button type="submit" class="btn btn-primary">
                                                            Next
                                                        </button>
                                                        <button type="reset" class="btn btn-secondary">
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>




                                            </div>
                                        </div>





                                    </div>


                                </div>



                        </div>
                        <!--end::Section-->

                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>
        </div>
    </div>

 @endsection

@section('admin_footer_script')
    <script>
        //== Class definition

        var DropzoneDemo = function () {
            //== Private functions
            var demos = function () {
                // single file upload
                // multiple file upload
                Dropzone.options.mDropzoneTwoPrimary = {
                    paramName: 'image',
                    method: 'POST',
                    maxFilesize: 1, // MB
                    maxFiles: 1,
                    acceptedFiles: ".jpeg,.jpg,.png,.gif"

                };

                Dropzone.options.mDropzoneTwo = {
                    paramName: 'image',
                    method: 'POST',
                    maxFilesize: 5, // MB
                    maxFiles: 20,
                    acceptedFiles: ".jpeg,.jpg,.png,.gif"

                };
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        DropzoneDemo.init();
    </script>
    @endsection