@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">




            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Create Store Users
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">


                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif




                            <form class="m-form m-form--fit m-form--label-align-right"
                                  role="form" method="post"
                                  action="{{ route('admin.cashier.create.save') }}"
                            >
                                {{ csrf_field() }}
                                <div class="m-portlet__body">

                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Name
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="flaticon-list"></i>
														</span>
                                            </div>
                                            <input type="text"
                                                   class="form-control m-input--air category-name"
                                                   placeholder="Name"
                                                   name="name"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>

                                    </div>




                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Email
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="flaticon-more-v6"></i>
														</span>
                                            </div>
                                            <input type="email"

                                                   class="form-control m-input--air category-slug"
                                                   placeholder="Email"
                                                   name="email"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>

                                    </div>


                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Mobile
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="flaticon-more-v6"></i>
														</span>
                                            </div>
                                            <input type="tel"

                                                   class="form-control m-input--air category-slug"
                                                   placeholder="Mobile"
                                                   name="mobile"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>

                                    </div>


                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Password
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="flaticon-more-v6"></i>
														</span>
                                            </div>
                                            <input type="tel"

                                                   class="form-control m-input--air category-slug"
                                                   placeholder="Password"
                                                   name="password"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>

                                    </div>







                                </div>






                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--end::Section-->

                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    All Cashier
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">



                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Mobile</th>
                                    <th scope="col">Delete</th>

                                </tr>
                                </thead>
                                <tbody>


                                @foreach($cashier as $s)

                                    <tr>
                                        <th scope="row">{{$s->id}}</th>
                                        <td>{{$s->name}}</td>
                                        <td>{{$s->email}}</td>
                                        <td>{{$s->mobile}}</td>
                                        <td>
                                            <a href="{{route('admin.cashier.delete',$s->id)}}" class=" btn btn-danger">Delete</a>
                                        </td>


                                    </tr>


                                @endforeach





                                </tbody>
                            </table>








                        </div>
                        <!--end::Section-->

                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>








        </div>
    </div>

 @endsection



@section('admin_footer_script')


    @endsection