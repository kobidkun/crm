@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">


                            <div class="m-portlet m-portlet--mobile">

                                <div class="m-portlet__body">
                                    <!--begin: Search Form -->

                                    <style>
                                        .table > tbody > tr > td {
                                            vertical-align: middle;
                                        }


                                        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
                                            /* color: white !important; */
                                            /* border: 1px solid #111; */
                                            background: #ffffff !important;
                                            border-color: #5867dd !important;

                                        }


                                        .table .thead-dark th {
                                            color: #fff;
                                            background-color: #5867dd!important;
                                            border-color: #32383e;
                                        }





                                    </style>



                                    <table class="table table-striped table-bordered" id="user-table" style="width: 100%">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>F Name</th>
                                            <th>L NAME</th>
                                            <th>EMAIL</th>
                                            <th>MOBILE</th>
                                            <th>IC NUMBER</th>
                                            <th>PAN</th>
                                            <th>SPONCER</th>
                                            <th>Commission</th>

                                            <th>ACTION</th>



                                        </tr>
                                        </thead>
                                    </table>







                        </div>
                        <!--end::Section-->

                    </div>


            </div>
        </div>
    </div>

 @endsection


@section('admin_footer_script')


    <link rel="stylesheet" type="text/css" href="{{asset('/datatable/datatables.css')}}"/>

    <script type="text/javascript" src="{{asset('/datatable/datatables.min.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(function() {
            $('#user-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('admin.customer.alldatatable')}}',
                columns: [
                    { data: 'fname', name: 'fname' },
                    { data: 'lname', name: 'lname' },
                    { data: 'email', name: 'email' },
                    { data: 'mobile', name: 'mobile' },
                    { data: 'ic_number', name: 'ic_number' },
                    { data: 'pan', name: 'pan' },
                    { data: 'sponser_id', name: 'sponser_id' },
                    { data: 'commission', name: 'commission' },

                    {data: 'action', name: 'action', orderable: false, searchable: false}

                ]
            });

        });
    </script>


    @endsection