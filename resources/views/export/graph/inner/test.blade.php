@extends('admin.index');

@section('content')



    <div class="m-content">
        <div class="row">


            <div class="col-md-12" style="height:520px;">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Daily Sales Last 30 Days
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">

                            <div class="m-widget14__chart" >



                                <div id="container"></div>



                            </div>




                        </div>

                        <!--end::Section-->
                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>
        </div>
    </div>

@endsection

@section('admin_footer_script')
    <style>
        #chartdiv {
            width: 100%;
            height: 800px;

        }</style>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

    <script>


        function requestData() {
            $.ajax({
                url: '{{route('realtime.graph')}}',
                success: function(point) {
                    var series = chart.series[0],
                        shift = series.data.length > 20; // shift if the series is
                                                         // longer than 20

                    // add the point
                    chart.series[0].addPoint(point, true, shift);

                    // call it again after one second
                    setTimeout(requestData, 1000);
                },
                cache: false
            });
        }

        document.addEventListener('DOMContentLoaded', (function() {
            chart = Highcharts.chart('container', {
                chart: {
                    type: 'area',
                    events: {
                        load: requestData
                    }
                },
                title: {
                    text: 'Live Order Status'
                },
                xAxis: {
                    type: 'datetime',
                    tickPixelInterval: 150,
                    maxZoom: 20 * 1000
                },
                yAxis: {
                    minPadding: 0.2,
                    maxPadding: 0.2,
                    title: {
                        text: 'No of Order Placed Today',
                        margin: 80
                    }
                },
                series: [{
                    name: 'No of Order Placed Today',
                    data: []
                }]
            });
        })
        );










  /*      /!**
         /!**
         * Init some variables for demo purposes
         *!/
        var day = 0;
        var firstDate = new Date();
        firstDate.setDate( firstDate.getDate() - 500 );

        /!**
         * Function that generates random data
         *!/
        function generateChartData() {
            var chartData = [];

            $.get("{{route('realtime.graph')}}", function(data, status){
                console.log(data);

                chartData.push( {
                    "date": data.time,
                    "visits": data.order
                } );


            });


            return chartData;







        }

        /!**
         * Create the chart
         *!/
        var chart = AmCharts.makeChart( "chartdiv", {
            "type": "serial",
            "theme": "light",
            "zoomOutButton": {
                "backgroundColor": '#000000',
                "backgroundAlpha": 0.15
            },
            "dataProvider": generateChartData(),
            "categoryField": "date",
            "categoryAxis": {
                "parseDates": true,
                "minPeriod": "DD",
                "dashLength": 1,
                "gridAlpha": 0.15,
                "axisColor": "#DADADA"
            },
            "graphs": [ {
                "id": "g1",
                "valueField": "visits",
                "bullet": "round",
                "bulletBorderColor": "#FFFFFF",
                "bulletBorderThickness": 2,
                "lineThickness": 2,
                "lineColor": "#b5030d",
                "negativeLineColor": "#0352b5",
                "hideBulletsCount": 50
            } ],
            "chartCursor": {
                "cursorPosition": "mouse"
            },
            "chartScrollbar": {
                "graph": "g1",
                "scrollbarHeight": 40,
                "color": "#FFFFFF",
                "autoGridCount": true
            }
        } )

        /!**
         * Set interval to push new data points periodically
         *!/
// set up the chart to update every second
        setInterval( function() {



            $.get("{{route('realtime.graph')}}", function(data, status){
                console.log(data);

                chart.dataProvider.push({
                    date: data.time,
                    visits: data.order
                });

                chart.validateData();
            });








        }, 1000 );*/
    </script>

@endsection