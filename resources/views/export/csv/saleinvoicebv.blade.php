@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">


                            <div class="m-portlet m-portlet--mobile">
                                <div class="m-portlet__head">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <h3 class="m-portlet__head-text">
                                                Export Sale Invoice BV
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="m-portlet__head-tools">

                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <!--begin: Search Form -->


                                    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                                          role="form" method="post"
                                          action="{{ route('invoicebv.export.custom.csv.view') }}"
                                    >
                                        {{ csrf_field() }}

<h4>CSV Export</h4>
                                    <div class="form-group m-form__group row">
                                        <div class="col-lg-4">
                                            <label for="exampleInputEmail1">
                                              From  Date
                                            </label>
                                            <div class="input-group m-input-group">
                                                <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                             <i class="la la-calendar-o"></i>
                                                         </span>
                                                </div>
                                                <input type="text"
                                                       class="form-control m-input--air"
                                                       placeholder="From  Date"
                                                       name="from"
                                                       value="{{ date('d-m-Y') }}"
                                                       required
                                                       id="m_datepicker_1" readonly
                                                       autocomplete="off"
                                                       aria-describedby="basic-addon1">
                                            </div>
                                        </div>




                                        <div class="col-lg-4">
                                            <label for="exampleInputEmail1">
                                              To  Date
                                            </label>
                                            <div class="input-group m-input-group">
                                                <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                             <i class="la la-calendar-check-o"></i>
                                                         </span>
                                                </div>
                                                <input type="text"
                                                       class="form-control m-input--air"
                                                       placeholder="To  Date"
                                                       required
                                                       name="to"
                                                       value="{{ date('d-m-Y') }}"
                                                       id="m_datepicker_1" readonly
                                                       autocomplete="off"
                                                       aria-describedby="basic-addon1">
                                            </div>
                                        </div>




                                        <div class="col-lg-4" style="padding-top: 25px">
                                            <button type="submit"
                                               class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la la-file-excel-o"></i>
													<span>
														Export in CSV

													</span>
												</span>
                                            </button>



                                        </div>



                                    </div>



                                </form>



                        </div>
                        <!--end::Section-->

                    </div>


            </div>
        </div>
    </div>

 @endsection


@section('admin_footer_script')


    <script>
        //== Class definition

        var BootstrapDatepicker = function () {

            //== Private functions
            var demos = function () {
                // minimum setup
                $('#m_datepicker_1, #m_datepicker_1_validate').datepicker({
                    format: "dd-mm-yyyy",
                    todayHighlight: true,
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    }
                });

                $('#m_datepicker_return_month').datepicker({
                    format: "mm-yyyy",
                    viewMode: "months",
                    minViewMode: "months",
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    }
                });

                // minimum setup for modal demo
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        jQuery(document).ready(function() {
            BootstrapDatepicker.init();
        });
    </script>



    @endsection