<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 4.8.3
 */

/**
 * Database `crm`
 */

/* `crm`.`admins` */
$admins = array(
);

/* `crm`.`arrtibutes` */
$arrtibutes = array(
);

/* `crm`.`b_v_types` */
$b_v_types = array(
);

/* `crm`.`casher_users` */
$casher_users = array(
);

/* `crm`.`catagories` */
$catagories = array(
);

/* `crm`.`category2s` */
$category2s = array(
);

/* `crm`.`category3s` */
$category3s = array(
);

/* `crm`.`create_b_v_orders` */
$create_b_v_orders = array(
);

/* `crm`.`create_b_v_order_to_invoices` */
$create_b_v_order_to_invoices = array(
);

/* `crm`.`create_b_v_order_to_products` */
$create_b_v_order_to_products = array(
);

/* `crm`.`create_invoices` */
$create_invoices = array(
);

/* `crm`.`create_invoice_b_vs` */
$create_invoice_b_vs = array(
);

/* `crm`.`create_orders` */
$create_orders = array(
);

/* `crm`.`customers` */
$customers = array(
);

/* `crm`.`customer_b_v_wallets` */
$customer_b_v_wallets = array(
);

/* `crm`.`customer_cart_items` */
$customer_cart_items = array(
);

/* `crm`.`customer_files` */
$customer_files = array(
);

/* `crm`.`customer_to_customers` */
$customer_to_customers = array(
);

/* `crm`.`customer_wallets` */
$customer_wallets = array(
);

/* `crm`.`initial_create_orders` */
$initial_create_orders = array(
);

/* `crm`.`initial_create_order_b_vs` */
$initial_create_order_b_vs = array(
);

/* `crm`.`initial_create_order_to_products` */
$initial_create_order_to_products = array(
);

/* `crm`.`initial_create_order_to_product_b_vs` */
$initial_create_order_to_product_b_vs = array(
);

/* `crm`.`invoice_to_products` */
$invoice_to_products = array(
);

/* `crm`.`invoice_to_product_b_vs` */
$invoice_to_product_b_vs = array(
);

/* `crm`.`migrations` */
$migrations = array(
    array('id' => '1','migration' => '2014_10_12_000000_create_users_table','batch' => '1'),
    array('id' => '2','migration' => '2014_10_12_100000_create_password_resets_table','batch' => '1'),
    array('id' => '3','migration' => '2016_06_01_000001_create_oauth_auth_codes_table','batch' => '1'),
    array('id' => '4','migration' => '2016_06_01_000002_create_oauth_access_tokens_table','batch' => '1'),
    array('id' => '5','migration' => '2016_06_01_000003_create_oauth_refresh_tokens_table','batch' => '1'),
    array('id' => '6','migration' => '2016_06_01_000004_create_oauth_clients_table','batch' => '1'),
    array('id' => '7','migration' => '2016_06_01_000005_create_oauth_personal_access_clients_table','batch' => '1'),
    array('id' => '8','migration' => '2017_10_08_000001_create_oauth_access_token_providers_table','batch' => '1'),
    array('id' => '9','migration' => '2018_02_20_095558_create_customers_table','batch' => '1'),
    array('id' => '10','migration' => '2018_02_20_100659_create_admins_table','batch' => '1'),
    array('id' => '11','migration' => '2018_02_22_095644_create_stocks_table','batch' => '1'),
    array('id' => '12','migration' => '2018_02_22_100248_create_catagories_table','batch' => '1'),
    array('id' => '13','migration' => '2018_02_22_100350_create_ratings_table','batch' => '1'),
    array('id' => '14','migration' => '2018_02_22_100451_create_arrtibutes_table','batch' => '1'),
    array('id' => '15','migration' => '2018_02_22_100940_create_varients_table','batch' => '1'),
    array('id' => '16','migration' => '2018_02_22_180332_create_products_to_categories_table','batch' => '1'),
    array('id' => '17','migration' => '2018_02_22_182844_create_products_table','batch' => '1'),
    array('id' => '18','migration' => '2018_02_22_193655_create_product_images_table','batch' => '1'),
    array('id' => '19','migration' => '2018_02_22_203023_create_product_primary_images_table','batch' => '1'),
    array('id' => '20','migration' => '2018_03_02_065856_create_category2s_table','batch' => '1'),
    array('id' => '21','migration' => '2018_03_02_065931_create_category3s_table','batch' => '1'),
    array('id' => '22','migration' => '2018_03_02_070307_create_product_to_category3s_table','batch' => '1'),
    array('id' => '23','migration' => '2018_03_02_070316_create_product_to_category2s_table','batch' => '1'),
    array('id' => '24','migration' => '2018_03_13_212431_create_customer_cart_items_table','batch' => '1'),
    array('id' => '25','migration' => '2018_03_14_211034_create_product_atributes_table','batch' => '1'),
    array('id' => '26','migration' => '2018_03_14_211916_create_product_to_atributes_table','batch' => '1'),
    array('id' => '27','migration' => '2018_03_17_223518_create_create_orders_table','batch' => '1'),
    array('id' => '28','migration' => '2018_03_17_223554_create_order_to_products_table','batch' => '1'),
    array('id' => '29','migration' => '2018_03_21_101047_create_create_invoices_table','batch' => '1'),
    array('id' => '30','migration' => '2018_03_21_101110_create_invoice_to_products_table','batch' => '1'),
    array('id' => '31','migration' => '2018_04_08_035558_create_product_vatients_table','batch' => '1'),
    array('id' => '32','migration' => '2018_04_08_061623_create_product_vatientto_images_table','batch' => '1'),
    array('id' => '33','migration' => '2018_04_08_061903_create_product_vatientto_stocks_table','batch' => '1'),
    array('id' => '34','migration' => '2018_04_10_032119_create_variant_types_table','batch' => '1'),
    array('id' => '35','migration' => '2018_04_17_044513_create_customer_files_table','batch' => '1'),
    array('id' => '36','migration' => '2018_04_17_193848_create_store_users_table','batch' => '1'),
    array('id' => '37','migration' => '2018_04_17_194032_create_casher_users_table','batch' => '1'),
    array('id' => '38','migration' => '2018_04_19_193310_create_stock_outs_table','batch' => '1'),
    array('id' => '39','migration' => '2018_04_23_004356_create_customer_wallets_table','batch' => '1'),
    array('id' => '40','migration' => '2018_04_24_192854_create_return_invoices_table','batch' => '1'),
    array('id' => '41','migration' => '2018_04_24_192914_create_return_invoiceto_products_table','batch' => '1'),
    array('id' => '42','migration' => '2018_04_29_045946_create_mobile_sloders_table','batch' => '1'),
    array('id' => '43','migration' => '2018_05_02_082838_create_create_invoice_b_vs_table','batch' => '1'),
    array('id' => '44','migration' => '2018_05_02_082849_create_invoice_to_product_b_vs_table','batch' => '1'),
    array('id' => '45','migration' => '2018_05_05_130205_create_website_sliders_table','batch' => '1'),
    array('id' => '46','migration' => '2018_05_10_120901_create_customer_b_v_wallets_table','batch' => '1'),
    array('id' => '47','migration' => '2018_05_15_214511_create_create_b_v_orders_table','batch' => '1'),
    array('id' => '48','migration' => '2018_05_15_214527_create_create_b_v_order_to_products_table','batch' => '1'),
    array('id' => '49','migration' => '2018_06_04_005357_create_return_invoice_b_vs_table','batch' => '1'),
    array('id' => '50','migration' => '2018_06_04_005657_create_return_invoiceto_product_b_vs_table','batch' => '1'),
    array('id' => '51','migration' => '2018_06_14_012150_create_initial_create_orders_table','batch' => '1'),
    array('id' => '52','migration' => '2018_06_14_012210_create_initial_create_order_to_products_table','batch' => '1'),
    array('id' => '53','migration' => '2018_06_14_030205_create_initial_create_order_b_vs_table','batch' => '1'),
    array('id' => '54','migration' => '2018_06_14_030246_create_initial_create_order_to_product_b_vs_table','batch' => '1'),
    array('id' => '55','migration' => '2018_06_28_035126_create_order_to_invoices_table','batch' => '1'),
    array('id' => '56','migration' => '2018_06_28_035415_create_create_b_v_order_to_invoices_table','batch' => '1'),
    array('id' => '57','migration' => '2018_07_13_032417_create_customer_to_customers_table','batch' => '1'),
    array('id' => '58','migration' => '2018_08_15_175253_create_b_v_types_table','batch' => '1'),
    array('id' => '59','migration' => '2018_12_11_031243_create_temp_customers_table','batch' => '1')
);

/* `crm`.`mobile_sloders` */
$mobile_sloders = array(
);

/* `crm`.`oauth_access_tokens` */
$oauth_access_tokens = array(
);

/* `crm`.`oauth_access_token_providers` */
$oauth_access_token_providers = array(
);

/* `crm`.`oauth_auth_codes` */
$oauth_auth_codes = array(
);

/* `crm`.`oauth_clients` */
$oauth_clients = array(
);

/* `crm`.`oauth_personal_access_clients` */
$oauth_personal_access_clients = array(
);

/* `crm`.`oauth_refresh_tokens` */
$oauth_refresh_tokens = array(
);

/* `crm`.`order_to_invoices` */
$order_to_invoices = array(
);

/* `crm`.`order_to_products` */
$order_to_products = array(
);

/* `crm`.`password_resets` */
$password_resets = array(
);

/* `crm`.`products` */
$products = array(
);

/* `crm`.`products_to_categories` */
$products_to_categories = array(
);

/* `crm`.`product_atributes` */
$product_atributes = array(
);

/* `crm`.`product_images` */
$product_images = array(
);

/* `crm`.`product_primary_images` */
$product_primary_images = array(
);

/* `crm`.`product_to_atributes` */
$product_to_atributes = array(
);

/* `crm`.`product_to_category2s` */
$product_to_category2s = array(
);

/* `crm`.`product_to_category3s` */
$product_to_category3s = array(
);

/* `crm`.`product_vatients` */
$product_vatients = array(
);

/* `crm`.`product_vatientto_images` */
$product_vatientto_images = array(
);

/* `crm`.`product_vatientto_stocks` */
$product_vatientto_stocks = array(
);

/* `crm`.`ratings` */
$ratings = array(
);

/* `crm`.`return_invoices` */
$return_invoices = array(
);

/* `crm`.`return_invoiceto_products` */
$return_invoiceto_products = array(
);

/* `crm`.`return_invoiceto_product_b_vs` */
$return_invoiceto_product_b_vs = array(
);

/* `crm`.`return_invoice_b_vs` */
$return_invoice_b_vs = array(
);

/* `crm`.`stocks` */
$stocks = array(
);

/* `crm`.`stock_outs` */
$stock_outs = array(
);

/* `crm`.`store_users` */
$store_users = array(
);

/* `crm`.`temp_customers` */
$temp_customers = array(
);

/* `crm`.`users` */
$users = array(
);

/* `crm`.`variant_types` */
$variant_types = array(
);

/* `crm`.`varients` */
$varients = array(
);

/* `crm`.`website_sliders` */
$website_sliders = array(
);