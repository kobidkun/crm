<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPrimaryImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_primary_images', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->string('cdn_url')->nullable();
            $table->string('path')->nullable();
            $table->string('local_path')->nullable();
            $table->string('extinsion')->nullable();
            $table->text('images_url');
            $table->text('product_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_primary_images');
    }
}
