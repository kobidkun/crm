<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnInvoicetoProductBVsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('return_invoiceto_product_b_vs', function (Blueprint $table) {
            $table->increments('id');
            $table->text('invoice_id')->nullable();
            $table->text('create_orders_id')->nullable();
            $table->text('customer_id')->nullable();
            $table->text('create_invoices_id')->nullable();
            $table->text('product_id')->nullable();
            $table->text('parent_product_id')->nullable();
            $table->text('color')->nullable();
            $table->text('size')->nullable();
            $table->text('name')->nullable();
            $table->text('sno')->nullable();
            $table->text('unit')->nullable();
            $table->string('hsn')->nullable();
            $table->string('type')->nullable();
            $table->text('quantity')->nullable();
            $table->text('rate')->nullable();
            $table->text('discount_amount')->nullable();
            $table->text('discount_percentage')->nullable();
            $table->text('taxable_rate')->nullable();
            $table->text('taxable_amount')->nullable();
            $table->text('tax_value')->nullable();
            $table->text('cgst')->nullable();
            $table->text('sgst')->nullable();
            $table->text('igst')->nullable();
            $table->text('sgst_percentage')->nullable();
            $table->text('cgst_percentage')->nullable();
            $table->text('igst_percentage')->nullable();
            $table->text('product_total_disc')->nullable();
            $table->text('total')->nullable();
            $table->text('bv')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('return_invoiceto_product_b_vs');
    }
}
