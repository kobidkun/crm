<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('code_name')->nullable();
            $table->string('hsn')->nullable();
            $table->string('type')->nullable();
            $table->string('unit')->nullable();
            $table->longText('s_desc')->nullable();
            $table->longText('l_desc')->nullable();
            $table->longText('h_light')->nullable();
            $table->string('base_price')->nullable();
            $table->string('disc_price')->nullable();
            $table->string('offer_price')->nullable();
            $table->string('mrp_price')->nullable();
            $table->string('disc_amount')->nullable();
            $table->string('discount_price')->nullable();
            $table->string('discount_per')->nullable();
            $table->string('dis_per')->nullable();
            $table->string('taxable_value')->nullable();
            $table->string('cgst')->nullable();
            $table->string('cgst_percentage')->nullable();
            $table->string('sgst')->nullable();
            $table->string('sgst_percentage')->nullable();
            $table->string('igst')->nullable();
            $table->string('igst_percentage')->nullable();

            $table->string('taxable_rate')->nullable();
            $table->string('cess_percentage')->nullable();

            $table->string('total_tax')->nullable();
            $table->string('listing_price')->nullable();
            $table->string('slug')->nullable();
            $table->string('stock_id')->nullable();
            $table->string('stock')->nullable();
            $table->string('catagory_id')->nullable();
            $table->string('catagory2_id')->nullable();
            $table->string('catagory3_id')->nullable();
            $table->string('rating_id')->nullable();
            $table->string('arrtibute_id')->nullable();
            $table->string('varient_id')->nullable();
            $table->boolean('active')->default('0');
            $table->timestamps();

            $table->index(['active', 'id','slug']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
