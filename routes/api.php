<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/*public Routes*/


Route::get('/search/global', 'admin\globalSearch\GlobalSearch@Search');

//Products
Route::get('/products/all', 'publiccontroller\ProductListApi@ProductsApiList')->name('api.products.public');



//ProductsMobileSlider

Route::get('/products/all', 'publiccontroller\ProductListApi@ProductsApiList')->name('api.products.public');
Route::get('/products/by-attribute', 'publiccontroller\ProductListApi@ProductbyCategory')->name('api.productsattri.public');
Route::get('/products/by-category/{calslud}', 'publiccontroller\ProductListApi@GetProductByCategory')->name('api.products.bycategory.public');
Route::get('/products/by-count/{count}', 'publiccontroller\ProductListApi@GetProductByCount')->name('api.products.bycount.public');
Route::get('/products/details/{slug}', 'publiccontroller\ProductListApi@GetProductById')->name('api.products.details.public');
Route::get('/products/variant/details/{slug}', 'publiccontroller\ProductListApi@GetProductVariantById')
    ->name('api.product.variant.details.public');

//other components
Route::get('/slider/mobile', 'publiccontroller\ProductListApi@MobileSlider')->name('api.slider.mobile');
Route::get('/slider/website', 'publiccontroller\ProductListApi@WebsiteSlider')->name('api.slider.website');
/*public Routes end*/



//json-api




//json-api-end




Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => 'auth:customer-api'], function () {
    Route::get('/customer', function (Request $request) {
        return $request->user(); // Return admin
    });
});

/*customer*/
Route::post('/customer/register', 'publiccontroller\CustomerAuth@CreateCustomerSave')->name('api.customer.register.save');


Route::post('/customer/private/addtocart',
    'customer\CartController@StoreCart')
    ->name('api.customer.add.to.cart.post');


Route::get('/customer/private/getaddtocart',
    'customer\CartController@GetCartItem');


Route::get('/customer/private/cart/delete/{id}',
    'customer\CartController@DeleteCartItem');


/*order take api*/
Route::post('/customer/private/takeorder',
    'customer\CreateOrder@CreateOrderSave');


Route::post('/customer/private/v2/takeorder',
    'customer\CreateOrder@CreateOrderSave2');


/*order take api bv*/
Route::post('/customer/private/bv/takeorder',
    'customer\CreateOrderBV@CreateOrderSave');


Route::post('/customer/private/bv/v2/takeorder',
    'customer\CreateOrderBV@CreateOrderSave2');

//wallet

Route::get('/customer/wallet/balance',
    'customer\ShopController@GetWallet');


//wallet bv

Route::get('/customer/bv-wallet/balance',
    'customer\ShopController@GetWalletBV');














//customer getForApiforBvPurchaseTypes


Route::get('/master/bv/types/get',
    'Customer\CustomerData@getForApiforBvPurchaseTypes');


///// customer profile


Route::get('/customer/profile/get',
    'Customer\CustomerData@Profile');

//customer order


Route::get('/customer/order/get',
    'Customer\CustomerData@GetOrder');


Route::get('/customer/orderbv/get',
    'Customer\CustomerData@GetBVOrder');


//customer order


Route::get('/customer/invoice/get',
    'Customer\CustomerData@GetInvoice');


Route::get('/customer/invoicebv/get',
    'Customer\CustomerData@GetBVInvoice');



//customer order


Route::get('/customer/returninvoice/get',
    'Customer\CustomerData@GetReturnInvoice');


Route::get('/customer/returninvoicebv/get',
    'Customer\CustomerData@GetReturnBVInvoice');


//tree view

Route::get('/customer/treeview/{id}',
    'customer\ManageTreeView@GetAllDownloneByid')
    ->name('api.customer.treeview');