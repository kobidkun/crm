<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/admin/login');
});

Route::get('/buma', function () {
    return view('service.email.neworder');
});

Route::get('/pdf', 'customer\CreateOrder@createOrderRecipt');

/*publiv*/




//admin


Route::get('/admin/passwordchange', 'admin\ManageAdmin@View')->name('admin.password.change.view');
Route::any('/admin/passwordchange/store', 'admin\ManageAdmin@changePassword')->name('admin.password.change.store');


// end admin



Route::get('/customer/secure/invoice/download/{slug}', 'publiccontroller\PublicWebConreoller@exportPdfInvoice')
    ->name('secure.invoice.download');

Route::get('/customer/secure/invoicebv/download/{slug}', 'publiccontroller\PublicWebConreoller@exportPdfInvoicebv')
    ->name('secure.invoicebv.download');

Route::get('/customer/secure/return/invoice/download/{slug}', 'publiccontroller\PublicWebConreoller@exportPdfReturnInvoice')
    ->name('secure.return.invoice.download');

Route::get('/customer/secure/return/invoicebv/download/{slug}', 'publiccontroller\PublicWebConreoller@exportPdfReturnInvoiceBV')
    ->name('secure.return.invoicebv.download');

/*publiv*/


/*customer*/

Route::post('/customer/register', 'customer\AuthController@CustomerRegisterSave')->name('customer.register.save');
Route::get('/customer/login', 'customer\AuthController@loginPage')->name('customer.login');
Route::get('/customer/dashboard', 'customer\BaseController@dashboard')->name('customer.dashboard');
Route::post('/customer/login', 'customer\AuthController@CustomerLogin')->name('customer.login.post');


/*end customer*/

/*admin*/

//base routes
Route::post('/admin/register', 'admin\AuthController@CustomerRegisterSave')->name('admin.register.save');
Route::get('/admin/login', 'admin\AuthController@loginPage')->name('admin.login');
Route::get('/admin/dashboard', 'admin\BaseController@dashboard')->name('admin.dashboard');
Route::post('/admin/login', 'admin\AuthController@CustomerLogin')->name('admin.login.post');
//end base routes


//create user
Route::get('/admin/user/create', 'admin\customer\ManageCustomer@CreateCustomer')->name('admin.customer.create');

Route::post('/admin/user/create', 'admin\customer\ManageCustomer@CreateCustomerSave')->name('admin.customer.create.save');
Route::post('/admin/user/update/{id}', 'admin\customer\ManageCustomer@UpdateUser')->name('admin.customer.update.user');
Route::get('/admin/user/all', 'admin\customer\ManageCustomer@AllCustomer')->name('admin.customer.all');
Route::get('/admin/user/alldataajax', 'admin\customer\ManageCustomer@getCustomerDatatables')->name('admin.customer.alldatatable');
Route::get('/admin/user/details/{id}', 'admin\customer\ManageCustomer@AdminDetailsUser')->name('admin.customer.details');
Route::put('/admin/user/password/change/{id}', 'admin\customer\ManageCustomer@ChangePassword')
    ->name('admin.customer.password.change');
Route::put('/admin/user/details/{id}', 'admin\customer\ManageCustomer@UpdateUser')->name('admin.customer.details.put');
Route::get('/search/customer/apisearch', 'admin\customer\ManageCustomer@GetAllCustomerviaSearch')->name('api.get.customer.apisearch');
Route::get('/customer/order/datatable/{id}', 'admin\customer\CustomerOrders@CustomerOrder')
    ->name('admin.get.customer.order.datatable');

Route::get('/admin/user/delete/{id}', 'admin\customer\ManageCustomer@Deleteuser')->name('admin.customer.delete');
Route::any('/admin/user/changerank/{id}', 'admin\customer\ManageCustomer@ChangeRank')->name('admin.customer.change.rank');
//


//temp customer

Route::get('/admin/user/temp/alldataajax', 'admin\customer\ManageCustomer@getTempCustomerDatatables')->name('admin.customer.temp.all.datatables');
Route::get('/admin/user/temp/all', 'admin\customer\ManageCustomer@AllTempCustomer')->name('admin.customer.temp.all');
Route::get('/admin/user/temp/profile/{id}', 'admin\customer\ManageCustomer@ViewTempCustomer')
    ->name('admin.customer.temp.profile');


//customer file upload


Route::post('/admin/customer/fileupload', 'customer\CustomerFiles@savefiles')->name('admin.customer.file.post');



//end create users
//
///
///  //customer file Wallet


Route::post('/admin/customer/wallet/add-money', 'admin\customer\CustomerWallet@AddAmount')
    ->name('admin.customer.wallet.add.money');

Route::get('/admin/customer/wallet/data-table/{id}', 'admin\customer\CustomerWallet@GetDatatables')
    ->name('admin.customer.wallet.data.table');


Route::get('/admin/customer/wallet/{id}', 'admin\customer\CustomerWallet@Delete')
    ->name('admin.customer.wallet.delete');


///  //customer file BV Wallet


Route::post('/admin/customer/bvwallet/add-money', 'admin\customer\ManageCustomerBVWallet@AddAmount')
    ->name('admin.customer.bvwallet.add.money');

Route::get('/admin/customer/bvwallet/data-table/{id}', 'admin\customer\ManageCustomerBVWallet@GetDatatables')
    ->name('admin.customer.bvwallet.data.table');


Route::get('/admin/customer/bvwallet/delete/{id}', 'admin\customer\ManageCustomerBVWallet@Delete')
    ->name('admin.customer.bvwallet.delete');



//end create users
//create store user


Route::get('/admin/storeuser/create', 'admin\internalusers\storeuser@create')->name('admin.storeuser.create.view');
Route::post('/admin/storeuser/create', 'admin\internalusers\storeuser@SaveUsers')->name('admin.storeuser.create.save');
Route::get('/admin/storeuser/{id}', 'admin\internalusers\storeuser@Edit')->name('admin.storeuser.create.details');
Route::get('/admin/storeuser/delete/{id}', 'admin\internalusers\storeuser@Delete')->name('admin.storeuser.delete');
Route::get('/admin/storeuser/all', 'admin\internalusers\storeuser@View')->name('admin.storeuser.all');



//create store user end
//
///
///
///   //create store user


Route::get('/admin/cashier/create', 'admin\internalusers\cashier@create')->name('admin.cashier.create.view');
Route::post('/admin/cashier/create', 'admin\internalusers\cashier@SaveUsers')->name('admin.cashier.create.save');
Route::get('/admin/cashier/{id}', 'admin\internalusers\cashier@Edit')->name('admin.storeuser.cashier.details');
Route::get('/admin/cashier/delete/{id}', 'admin\internalusers\cashier@Delete')->name('admin.cashier.delete');
Route::get('/admin/cashier/all', 'admin\internalusers\cashier@View')->name('admin.cashier.all');



//create store user end




//product
Route::get('/admin/product/create', 'admin\product\ManageProduct@CreateProduct')->name('admin.product.create');
Route::post('/admin/product/create', 'admin\product\ManageProduct@ProductSave')->name('admin.product.create.save');
Route::get('/admin/product/alldataajax', 'admin\product\ManageProduct@getProductDatatables')->name('admin.product.alldatatable');
Route::get('/admin/product/all', 'admin\product\ManageProduct@AllProduct')->name('admin.product.all');
Route::post('/admin/product/uploadimages', 'admin\product\ManageProduct@UploadProductImages')
    ->name('admin.product.uploadimages');
Route::post('/admin/product/primaryuploadimages', 'admin\product\ManageProduct@UploadProductPrimaryImages')->name('admin.product.primaryuploadimages');
Route::get('/product/apisearch', 'admin\product\ManageProduct@GetAllProductiaSearch')
    ->name('product.apisearch');
/*admin end*/

//product crud

Route::get('/admin/product/edit/{id}', 'admin\product\ManageProduct@ViewProductDetails')->name('admin.product.edit.show');
Route::any('/admin/product/edit/save/{id}', 'admin\product\ManageProduct@UpdateProductdata')->name('admin.product.edit.save');

//end crud
//delete product
Route::get('/admin/product/delete/whole/{id}', 'admin\product\ManageProduct@DeleteProduct')
    ->name('admin.product.whole.delete');

//replace images
Route::post('/admin/product/image/primaryimage/{id}', 'admin\product\ManageProduct@UpdateProductPrimaryImage')->name('admin.product.primary.image.replace.save');
Route::get('/admin/product/image/secondary/{id}', 'admin\product\ManageProduct@DeleteProductSecondaryImages')
    ->name('admin.product.secondary.image.delete.save');


//product categories


//save product varient

Route::post('/admin/product/varient/save', 'admin\product\ProductVatient@SaveProductVarient')
    ->name('admin.product.varient.save');


Route::get('/admin/product/varient/delete/{id}', 'admin\product\ProductVatient@delete')
    ->name('admin.product.delete');


Route::get('/admin/product/varient/details/{varid}', 'admin\product\ProductVatient@details')
    ->name('admin.product.varient.details');


Route::get('/admin/product/varient/datatable/{id}', 'admin\product\ProductVatient@ProductVarianrdatatable')
    ->name('admin.product.varient.datatable');


Route::any('/admin/productvariant/edit/save/{id}', 'admin\product\ManageProduct@UpdateProductdata')
    ->name('admin.product.variant.edit.save');

//upload images

Route::post('/admin/product/variant/image/upload', 'admin\product\ProductVatient@ImageUpload')
    ->name('admin.product.variant.uploadimage');

Route::get('/admin/product/variant/image/upload/{id}', 'admin\product\ProductVatient@DeleteProductSecondaryImages')
    ->name('admin.product.variant.image.delete');




//variant stock

Route::post('/admin/product/varient/stock/add', 'admin\product\VatianttoStock@addStock')
    ->name('admin.product.varient.stock.add');

Route::get('/admin/product/varient/stock/datable/{id}', 'admin\product\VatianttoStock@ProductVarianrstockdatatable')
    ->name('admin.product.varient.stock.datatable');

Route::get('/admin/product/varient/stock/delete/{id}', 'admin\product\VatianttoStock@delete')
    ->name('admin.product.varient.stock.delete');






//primary
Route::get('/admin/category/create', 'admin\product\ManageCatagory@CreateCatagory')->name('admin.category.create');
Route::post('/admin/category/create', 'admin\product\ManageCatagory@CreateCatagorySave')->name('admin.category.create.save');
Route::get('/admin/category/alldataajax', 'admin\product\ManageCatagory@getCatagoryDatatables')->name('admin.category.alldatatable');
Route::get('/admin/category/all', 'admin\product\ManageCatagory@AllCatagory')->name('admin.category.all');
Route::get('/admin/category/delete/{id}', 'admin\product\ManageCatagory@DeleteCategoryPrimary')->name('admin.category.delete');

//secondary
Route::get('/admin/category/sec/create', 'admin\product\catagory\Secondary@CreateCatagory')->name('admin.category.sec.create');
Route::post('/admin/category/sec/create', 'admin\product\catagory\Secondary@CreateCatagorySave')->name('admin.category.create.sec.save');
Route::get('/admin/category/sec/alldataajax', 'admin\product\catagory\Secondary@getCatagoryDatatables')->name('admin.category.sec.alldatatable');
Route::get('/admin/category/sec/all', 'admin\product\catagory\Secondary@AllCatagory')->name('admin.category.sec.all');
Route::get('/admin/category/sec/delete/{id}', 'admin\product\catagory\Secondary@DeleteCategorySecondary')->name('admin.category.sec.delete');
Route::get('/admin/category/sec/searchforproducy/{cat}', 'admin\product\catagory\Secondary@getCategoriesList')
    ->name('admin.category.sec.searchforcatsec');

//turtiary
Route::get('/admin/category/tur/create', 'admin\product\catagory\Turtiary@CreateCatagory')->name('admin.category.tur.create');
Route::post('/admin/category/tur/create', 'admin\product\catagory\Turtiary@CreateCatagorySave')->name('admin.category.create.tur.save');
Route::get('/admin/category/tur/alldataajax', 'admin\product\catagory\Turtiary@getCatagoryDatatables')->name('admin.category.tur.alldatatable');
Route::get('/admin/category/tur/all', 'admin\product\catagory\Turtiary@AllCatagory')->name('admin.category.tur.all');
Route::get('/admin/category/tur/delete/{id}', 'admin\product\catagory\Turtiary@DeleteCategoryTurtiary')->name('admin.category.tur.delete');
Route::get('/admin/category/tur/searchforproducy/{cat}', 'admin\product\catagory\Turtiary@getCategoriesList')
    ->name('admin.category.tur.searchforcatsec');
//variant

Route::get('/admin/variant/create', 'admin\variant\ManageVariant@createVariantindex')->name('admin.variant.show.index');
Route::get('/admin/variant/all', 'admin\variant\ManageVariant@allVariant')->name('admin.variant.all');
Route::post('/admin/variant/create', 'admin\variant\ManageVariant@SaveVariant')
    ->name('admin.variant.save');
Route::get('/admin/variant/datatable', 'admin\variant\ManageVariant@getBasicData')->name('admin.variant.datable');
Route::get('/admin/variant/delete/{id}', 'admin\variant\ManageVariant@delete')->name('admin.variant.delete');
Route::get('/admin/variant/findoption/{name}', 'admin\variant\ManageVariant@getoptions')->name('admin.variant.findoption');
//qrcode


Route::get('/admin/qrcode/{id}', 'Product\QrcodeDownload@DownloadQRCode')->name('admin.qrcodeDownload');

//attrubutes

Route::get('/admin/attribute/create', 'admin\product\ManageAttribute@CreateAttribute')
    ->name('admin.attribute.create.view');

Route::post('/admin/attribute/create', 'admin\product\ManageAttribute@CreateAttributeStore')
    ->name('admin.attribute.create.store');


Route::get('/admin/attribute/all', 'admin\product\ManageAttribute@AllAttribute')
    ->name('admin.attribute.show.all');

Route::get('/admin/attribute/datatables', 'admin\product\ManageAttribute@getAttribuleDatatables')
    ->name('admin.attribute.datatable.get');

Route::get('/admin/attribute/{id}', 'admin\product\ManageAttribute@AttributeDetails')
    ->name('admin.attribute.details');

Route::post('/admin/product/add/attribute', 'admin\product\ManageProduct@ImagesToAttribute')
           ->name('admin.product.add.attributes');
Route::post('/att', 'admin\product\ManageProduct@CreateProductAttributeStore')->name('admin.attribute.product.store');


//attrubutes end

//Stock start

Route::post('/admin/product/stock', 'admin\stock\ManageStock@StockSave')->name('admin.stock.product.store');

Route::get('/admin/product/get/datatables/{id}', 'admin\stock\ManageStock@getBasicData')->name('admin.stock.product.datable');
Route::get('/admin/product/get/delete/{id}', 'admin\stock\ManageStock@DeleteStock')->name('admin.stock.product.delete');


//Stock start End


//sale manage
Route::get('/dashboard/sale/create', 'admin\invoice\ManageInvoice@CreateInvoiceView')
    ->name('dashboard.invoice.create.view');

/*Route::get('/dashboard/sale/view', 'admin\invoice\ManageInvoice@ViewAllinv')
    ->name('dashboard.invoice.all.view');*/

Route::post('/dashboard/invoice/save', 'admin\invoice\ManageInvoice@CreateInvoiceSave')
    ->name('dashboard.invoice.manage.save');

Route::get('/dashboard/invoice/view/{id}', 'admin\invoice\ManageInvoice@ViewCreatedInvoice')
    ->name('dashboard.invoice.view.single');

Route::get('/dashboard/invoice/datatable/all', 'admin\invoice\ManageInvoice@getInvoiceDatatables')
    ->name('dashboard.invoice.view.datatable');

Route::get('/dashboard/invoice/all', 'admin\invoice\ManageInvoice@ViewInvoiceAllDatables')
    ->name('dashboard.invoice.view.all');

Route::get('/dashboard/invoice/delete/{id}', 'admin\invoice\ManageInvoice@DeleteInvoice')
    ->name('dashboard.invoice.delete');

Route::get('/dashboard/invoice/download/{id}/pdf', 'admin\invoice\ManageInvoice@exportPdfInvoice')
    ->name('dashboard.invoice.download.pdf');

Route::get('/dashboard/invoice/download/{id}/print', 'admin\invoice\ManageInvoice@exportPrintInvoice')
    ->name('dashboard.invoice.print.pdf');

Route::get('/dashboard/invoice/html/{id}/print', 'admin\invoice\ManageInvoice@exportPrintingInvoice')
    ->name('dashboard.invoice.print.html');

//datatable in product invoices

Route::get('/dashboard/invoice/products/{id}', 'admin\invoice\ManageInvoice@getPrimaryProductDatatables')
    ->name('dashboard.invoice.product.datatable');


Route::get('/dashboard/invoice/products/productvariant/{id}', 'admin\invoice\ManageInvoice@getVariantProductDatatables')
    ->name('dashboard.invoice.productvariant.datatable');








//sale manage bvinvoice
Route::get('/dashboard/bvinvoice/create', 'admin\bvinvoice\ManageBVinvoice@CreateInvoiceView')
    ->name('dashboard.bvinvoice.create.view');

/*Route::get('/dashboard/sale/view', 'admin\invoice\ManageInvoice@ViewAllinv')
    ->name('dashboard.invoice.all.view');*/

Route::post('/dashboard/bvinvoice/save', 'admin\bvinvoice\ManageBVinvoice@CreateInvoiceSave')
    ->name('dashboard.bvinvoice.manage.save');

Route::get('/dashboard/bvinvoice/view/{id}', 'admin\bvinvoice\ManageBVinvoice@ViewCreatedInvoice')
    ->name('dashboard.bvinvoice.view.single');

Route::get('/dashboard/bvinvoice/datatable/all', 'admin\bvinvoice\ManageBVinvoice@getInvoiceDatatables')
    ->name('dashboard.bvinvoice.view.datatable');

Route::get('/dashboard/bvinvoice/all', 'admin\bvinvoice\ManageBVinvoice@ViewInvoiceAllDatables')
    ->name('dashboard.bvinvoice.view.all');


Route::get('/dashboard/bvinvoice/delete/{id}', 'admin\bvinvoice\ManageBVinvoice@DeleteInvoice')
    ->name('dashboard.bvinvoice.delete');

Route::get('/dashboard/bvinvoice/download/{id}/pdf', 'admin\bvinvoice\ManageBVinvoice@exportPdfInvoice')
    ->name('dashboard.bvinvoice.download.pdf');

Route::get('/dashboard/bvinvoice/download/{id}/print', 'admin\bvinvoice\ManageBVinvoice@exportPrintInvoice')
    ->name('dashboard.bvinvoice.print.pdf');

//datatable in product invoices

Route::get('/dashboard/bvinvoice/products/{id}', 'admin\invoice\ManageBVinvoice@getPrimaryProductDatatables')
    ->name('dashboard.bvinvoice.product.datatable');


Route::get('/dashboard/bvinvoice/products/productvariant/{id}', 'admin\invoice\ManageBVinvoice@getVariantProductDatatables')
    ->name('dashboard.bvinvoice.productvariant.datatable');




//manage Return Invoice


Route::get('/dashboard/sale/returninvoice/create', 'admin\invoice\ManageReturnInvoice@CreateInvoiceView')
    ->name('dashboard.returninvoice.create.view');



Route::post('/dashboard/returninvoice/save', 'admin\invoice\ManageReturnInvoice@CreateInvoiceSave')
    ->name('dashboard.returninvoice.manage.save');

Route::get('/dashboard/returninvoice/view/{id}', 'admin\invoice\ManageReturnInvoice@ViewCreatedInvoice')
    ->name('dashboard.returninvoice.view.single');

Route::get('/dashboard/returninvoice/datatable/all', 'admin\invoice\ManageReturnInvoice@getInvoiceDatatables')
    ->name('dashboard.returninvoice.view.datatable');

Route::get('/dashboard/returninvoice/all', 'admin\invoice\ManageReturnInvoice@ViewInvoiceAllDatables')
    ->name('dashboard.returninvoice.view.all');

Route::get('/dashboard/returninvoice/download/{id}/pdf', 'admin\invoice\ManageReturnInvoice@exportPdfInvoice')
    ->name('dashboard.returninvoice.download.pdf');

Route::get('/dashboard/returninvoice/download/{id}/print', 'admin\invoice\ManageReturnInvoice@exportPrintInvoice')
    ->name('dashboard.returninvoice.print.pdf');

//datatable in product invoices

Route::get('/dashboard/returninvoice/products/{id}', 'admin\invoice\ManageReturnInvoice@getPrimaryProductDatatables')
    ->name('dashboard.returninvoice.product.datatable');


Route::get('/dashboard/returninvoice/products/productvariant/{id}', 'admin\invoice\ManageReturnInvoice@getVariantProductDatatables')
    ->name('dashboard.returninvoice.productvariant.datatable');







//manage Return Invoice BV


Route::get('/dashboard/sale/returninvoicebv/create', 'admin\bvinvoice\ManageReturnInvoicebv@CreateInvoiceView')
    ->name('dashboard.returninvoicebv.create.view');



Route::post('/dashboard/returninvoicebv/save', 'admin\bvinvoice\ManageReturnInvoicebv@CreateInvoiceSave')
    ->name('dashboard.returninvoicebv.manage.save');

Route::get('/dashboard/returninvoicebv/view/{id}', 'admin\bvinvoice\ManageReturnInvoicebv@ViewCreatedInvoice')
    ->name('dashboard.returninvoicebv.view.single');

Route::get('/dashboard/returninvoicebv/datatable/all', 'admin\bvinvoice\ManageReturnInvoicebv@getInvoiceDatatables')
    ->name('dashboard.returninvoicebv.view.datatable');

Route::get('/dashboard/returninvoicebv/all', 'admin\bvinvoice\ManageReturnInvoicebv@ViewInvoiceAllDatables')
    ->name('dashboard.returninvoicebv.view.all');

Route::get('/dashboard/returninvoicebv/download/{id}/pdf', 'admin\bvinvoice\ManageReturnInvoicebv@exportPdfInvoice')
    ->name('dashboard.returninvoicebv.download.pdf');

Route::get('/dashboard/returninvoicebv/download/{id}/print', 'admin\bvinvoice\ManageReturnInvoicebv@exportPrintInvoice')
    ->name('dashboard.returninvoicebv.print.pdf');

//datatable in product invoices

Route::get('/dashboard/returninvoicebv/products/{id}', 'admin\bvinvoice\ManageReturnInvoicebv@getPrimaryProductDatatables')
    ->name('dashboard.returninvoicebv.product.datatable');


Route::get('/dashboard/returninvoicebv/products/productvariant/{id}', 'admin\bvinvoice\ManageReturnInvoicebv@getVariantProductDatatables')
    ->name('dashboard.returninvoicebv.productvariant.datatable');



//placed orders


Route::get('/dashboard/sale/placed/orders/', 'admin\placedorder\ManageOrder@Allorders')
    ->name('dashboard.placed.create.view.all');

Route::get('/dashboard/sale/placed/orders/{id}', 'admin\placedorder\ManageOrder@details')
    ->name('dashboard.placed.create.view');



Route::get('/dashboard/sale/placed/orders/datatable/get', 'admin\placedorder\ManageOrder@Datatable')
    ->name('dashboard.placed.create.datatable');


//export


Route::get('/dashboard/placedorder/download/{id}/pdf', 'admin\placedorder\ManageOrder@exportPdfOrder')
    ->name('dashboard.placedorder.download.pdf');

Route::get('/dashboard/placedorder/download/{id}/print', 'admin\placedorder\ManageOrder@exportPrintOrder')
    ->name('dashboard.placedorder.print.pdf');



Route::post('/dashboard/placedorder/convert/invoice/{id}', 'admin\OrderToInvoice\CreateOrderToInvoice@ConvertOrderToInvoice')
    ->name('convert.placedorder.to.invoice');

Route::get('/dashboard/placedorder/convert/invoice/{id}', 'admin\OrderToInvoice\CreateOrderToInvoice@viewinvconverson')
    ->name('convert.placedorder.to.invoice.get');


Route::post('/dashboard/placedorder/convert/bvinvoice/{id}', 'admin\OrderToInvoice\CreateOrderToInvoiceBV@ConvertOrderToInvoice')
    ->name('convert.placedorder.to.bvinvoice');

Route::get('/dashboard/placedorder/convert/bvinvoice/{id}', 'admin\OrderToInvoice\CreateOrderToInvoiceBV@viewinvconverson')
    ->name('convert.placedorder.to.bvinvoice.get');











//placed bv


Route::get('/dashboard/bvsale/placed/orders/', 'admin\placedorder\MangeBvOrder@Allorders')
    ->name('dashboard.placed.bv.create.view.all');

Route::get('/dashboard/bvsale/placed/orders/{id}', 'admin\placedorder\MangeBvOrder@details')
    ->name('dashboard.placed.bv.create.view');



Route::get('/dashboard/bvsale/placed/orders/datatable/get', 'admin\placedorder\MangeBvOrder@Datatable')
    ->name('dashboard.placed.bv.create.datatable');


//EXPORT

Route::get('/dashboard/bvplacedorder/download/{id}/pdf', 'admin\placedorder\MangeBvOrder@exportPdfOrderbv')
    ->name('dashboard.bvplacedorder.download.pdf');

Route::get('/dashboard/bvplacedorder/download/{id}/print', 'admin\placedorder\MangeBvOrder@exportPrintOrderbv')
    ->name('dashboard.bvplacedorder.print.pdf');






//return invoice export

Route::get('/dashboard/returninvoice/export/', 'admin\invoice\ManageReturnInvoice@exportCustomInvoicesview')
    ->name('dashboard.returninvoice.export.view.custom');


Route::get('/dashboard/returninvoice/export/all/excel', 'admin\invoice\ManageReturnInvoice@export')
    ->name('dashboard.returninvoice.export.all.excel');

Route::post('/dashboard/returninvoice/export/custom/excel', 'admin\invoice\ManageReturnInvoice@exportCustomInvoicesviewexcel')
    ->name('dashboard.returninvoice.export.custom.excel');




//exports

Route::get('/dashboard/invoice/export/', 'admin\invoice\ManageInvoice@exportCustomInvoicesview')
    ->name('dashboard.invoice.export.view.custom');


Route::get('/dashboard/invoice/export/all/excel', 'admin\invoice\ManageInvoice@export')
    ->name('dashboard.invoice.export.all.excel');

Route::post('/dashboard/invoice/export/custom/excel', 'admin\invoice\ManageInvoice@exportCustomInvoicesviewexcel')
    ->name('dashboard.invoice.export.custom.excel');




//product

Route::get('/dashboard/product/export/all/excel', 'export\ExportExcel@exportCustomProductsviewexcelview')
    ->name('dashboard.product.export.excel.view');

Route::post('/dashboard/product/export/custom/excel', 'export\ExportExcel@exportCustomProductsviewexcel')
    ->name('dashboard.product.export.excel');


//product

Route::get('/dashboard/stockinout/export/all/excel', 'export\ExportExcel@Stockinout')
    ->name('dashboard.stockinout.export.excel.view');

//




//stockproductvariants

Route::get('/dashboard/stockproductvariants/export/all/excel', 'export\ExportExcel@exportstockproductvariantsviewexcelview')
    ->name('dashboard.stockproductvariants.export.excel.view');

Route::post('/dashboard/stockproductvariants/export/custom/excel', 'export\ExportExcel@exportstockproductvariantsexcel')
    ->name('dashboard.stockproductvariants.export.excel');


//customer

Route::get('/dashboard/customer/export/all/excel', 'export\ExportExcel@exportCustomCustomerviewexcelview')
    ->name('dashboard.customer.export.excel.view');

Route::post('/dashboard/customer/export/custom/excel', 'export\ExportExcel@exportCustomCustomerviewexcel')
    ->name('dashboard.customer.export.excel');


//



//jsom invoice
Route::post('/dashboard/invoice/export/custom/json', 'export\InvoiceExport@InvoiceExportTojson')
    ->name('invoice.export.custom.json.download');


Route::get('/dashboard/invoice/export/custom/json', 'export\InvoiceExport@InvoiceExportTojsonview')
    ->name('invoice.export.custom.json');


//json products sale

Route::post('/dashboard/products/export/custom/json', 'export\ExportJson@ProductExportTojson')
    ->name('products.export.custom.json.download');


Route::get('/dashboard/products/export/custom/json', 'export\ExportJson@ExportProduct')
    ->name('products.export.custom.json');




//json customer sale

Route::post('/dashboard/customer/export/custom/json', 'export\ExportJson@CustomerExportTojson')
    ->name('customer.export.custom.json.download');


Route::get('/dashboard/customer/export/custom/json', 'export\ExportJson@ExportCustomer')
    ->name('customer.export.custom.json');



//json StockinoutView


Route::post('/dashboard/stockinoutview/export/custom/json', 'export\ExportJson@Stockinout')
    ->name('stockinoutview.export.custom.json.download');


Route::get('/dashboard/stockinoutview/export/custom/json', 'export\ExportJson@StockinoutView')
    ->name('stockinoutview.export.custom.json');





//exports in csv invoices

Route::post('/dashboard/invoice/export/custom/csv', 'export\ExportCsv@exportInvoice')
    ->name('invoice.export.custom.csv.download');


Route::get('/dashboard/invoice/export/custom/csv', 'export\ExportCsv@ViewInvoice')
    ->name('invoice.export.custom.csv');


//exports in csv products

Route::post('/dashboard/products/export/custom/csv', 'export\ExportCsv@exportProduct')
    ->name('products.export.custom.csv.download');


Route::get('/dashboard/products/export/custom/csv', 'export\ExportCsv@ViewProduct')
    ->name('products.export.custom.csv');



//exports in csv products

Route::post('/dashboard/customer/export/custom/csv', 'export\ExportCsv@exportCustomer')
    ->name('customer.export.custom.csv.download');


Route::get('/dashboard/customer/export/custom/csv', 'export\ExportCsv@ViewCustomer')
    ->name('customer.export.custom.csv');



//exports in csv productsvarients

Route::post('/dashboard/productsvarients/export/custom/csv', 'export\ExportCsv@ExportProductsVariendsWithStock')
    ->name('productsvarients.export.custom.csv.download');


Route::get('/dashboard/productsvarients/export/custom/csv', 'export\ExportCsv@ExportProductsVariendsWithStockview')
    ->name('productsvarients.export.custom.csv');


//csv inout stock

Route::post('/dashboard/stockinout/export/custom/csv', 'export\ExportCsv@Stockinout')
    ->name('stockinout.export.custom.csv.download');


Route::get('/dashboard/stockinout/export/custom/csv', 'export\ExportCsv@StockinoutView')
    ->name('stockinout.export.custom.csv.view');


//csv invoicebv

Route::post('/dashboard/invoicebv/export/custom/csv', 'export\ExportCsv@InvoiceBVDownload')
    ->name('invoicebv.export.custom.csv.download');


Route::get('/dashboard/invoicebv/export/custom/csv', 'export\ExportCsv@InvoiceBV')
    ->name('invoicebv.export.custom.csv.view');



//csv retujrn invoice

Route::post('/dashboard/returninvoice/export/custom/csv', 'export\ExportCsv@ReturnInvoiceDownload')
    ->name('returninvoice.export.custom.csv.download');


Route::get('/dashboard/returninvoice/export/custom/csv', 'export\ExportCsv@ReturnInvoice')
    ->name('returninvoice.export.custom.csv.view');


//csv retujrn invoice bv

Route::post('/dashboard/returninvoicebv/export/custom/csv', 'export\ExportCsv@ReturnInvoiceBVDownload')
    ->name('returninvoicebv.export.custom.csv.download');


Route::get('/dashboard/returninvoicebv/export/custom/csv', 'export\ExportCsv@ReturnInvoiceBV')
    ->name('returninvoicebv.export.custom.csv.view');









//exports in grapth invoice

Route::post('/dashboard/invoice/export/custom/graph', 'export\ExportGraph@InvoiceGraph')
    ->name('invoice.export.custom.graph.download');


Route::get('/dashboard/invoice/export/custom/graph', 'export\ExportGraph@ViewInvoice')
    ->name('invoice.export.custom.graph');









//test

Route::get('/test', 'export\ExportGraph@test');
Route::get('/realtime', 'export\ExportGraph@RealtimeOrders')->name('realtime.graph');
Route::get('/realtime/sale/quantity', 'export\ExportGraph@RealtimeQuantiy')->name('realtime.graph.quantitySold');



Route::get('/realtime/sales', 'export\ExportGraph@RealtimeReportView')->name('realtime.report.view');
Route::get('/realtime/quantity', 'export\ExportGraph@RealtimeReportViewqty')->name('realtime.report.view.qty');
Route::get('/realtime/online-order-api', 'export\ExportGraph@RealtimeOnlineOrders')
    ->name('realtime.report.online.order.api');

Route::get('/realtime/online-order', 'export\ExportGraph@OnlineorderView')
    ->name('realtime.report.online.order.view');







//sale manage end
//

//


//casher controller

Route::post('/cashier/login', 'cashier\AuthConreoller@CashierLogin')->name('cashier.auth.login.post');
Route::get('/cashier/login', 'cashier\AuthConreoller@loginview')->name('cashier.login');
Route::get('/cashier/dashboard', 'cashier\BaseConreoller@dashboard')->name('cashier.dashboard.view');
Route::post('/cashier/logout', 'cashier\AuthConreoller@logout')->name('cashier.logout');



//end cashier
//
///
///  //store  controller

Route::post('/storeuser/login/post', 'storeuser\AuthConreoller@StoreLogin')->name('storeuser.auth.login.post');
Route::get('/storeuser/login', 'storeuser\AuthConreoller@loginview')->name('storeuser.login');
Route::get('/storeuser/dashboard', 'storeuser\BaseConreoller@dashboard')->name('storeuser.dashboard.view');
Route::post('/storeuser/logout', 'storeuser\AuthConreoller@logout')->name('storeuser.logout');



//end store


//admin frontend
Route::get('/dashboard/frontend/slider/mobile', 'admin\FrontEnd\Slider\MobileSloder@View')
    ->name('mobile.slider.frontend');

Route::post('/dashboard/frontend/slider/mobile/save', 'admin\FrontEnd\Slider\MobileSloder@Upload')
    ->name('mobile.slider.frontend.uploadImage');

Route::get('/dashboard/frontend/slider/mobile/delete/{id}', 'admin\FrontEnd\Slider\MobileSloder@delete')
    ->name('mobile.slider.frontend.delete');


//admin frontend websiute
Route::get('/dashboard/frontend/slider/website', 'admin\FrontEnd\Slider\WebsiteSlider@View')
    ->name('website.slider.frontend');

Route::post('/dashboard/frontend/slider/website/save', 'admin\FrontEnd\Slider\WebsiteSlider@Upload')
    ->name('website.slider.frontend.uploadImage');

Route::get('/dashboard/frontend/slider/website/delete/{id}', 'admin\FrontEnd\Slider\WebsiteSlider@delete')
    ->name('website.slider.frontend.delete');


// Tree View Srart

Route::get('/dashboard/admin/tree-view/by-id/down-line/{id}', 'admin\TreeView\ManageTreeView@GetAllDownloneByid')
    ->name('admin.tree.view.by.id.downline');

Route::get('/tree/down-line/foradmin', 'admin\TreeView\ManageTreeView@GetAllDownlone')
    ->name('admin.tree.view.by.id.admin');

Route::get('/dashboard/admin/tree-view/', 'admin\TreeView\ManageTreeView@ViewTreeView')
    ->name('admin.tree.view');


Route::get('/dashboard/admin/tree-view/view/{id}', 'admin\TreeView\ManageTreeView@ViewTreeViewbyid')
    ->name('admin.tree.view.by.id');





// Tree View

// bv types create

Route::get('/dashboard/admin/master/managebvmaster', 'admin\master\ManageBvPurchase@ViewBvType')
    ->name('master.manage.bv.master');

Route::post('/dashboard/admin/master/managebvmaster', 'admin\master\ManageBvPurchase@ViewBvTypesave')
    ->name('master.manage.bv.master.save');

Route::get('/dashboard/admin/master/delete/{id}', 'admin\master\ManageBvPurchase@ViewBvTypedelete')
    ->name('master.manage.bv.master.delete');


// bv types create







/*admin end*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//admin stats


Route::get('admin/stats/daily/sale-amount', 'admin\AdminStats@DailyOrderStatus');